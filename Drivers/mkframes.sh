#!/bin/sh

ROOT=/NFSROOT/kerrighed64/home/alex/tmp
TABLES="migrations_session3 migrations_session2 migrations_session1 migrations_session0 migrations"

mkdir -p ${ROOT}/Videos/ ${ROOT}/VideosAcc/
for table in ${TABLES};
do
	mkdir -p ${ROOT}/Videos/${table}/1min/ && cd ${ROOT}/Videos/${table}/1min/
	perl /home/alex/PFE/trunk/Code/migration_monitor/kerrighed-migmon-migrations-videos.pl --table ${table} --frequency 1

	mkdir -p ${ROOT}/Videos/${table}/5min/ && cd ${ROOT}/Videos/${table}/5min/
	perl /home/alex/PFE/trunk/Code/migration_monitor/kerrighed-migmon-migrations-videos.pl --table ${table} --frequency 5

	mkdir -p ${ROOT}/VideosAcc/${table}/1min/ && cd ${ROOT}/VideosAcc/${table}/1min/
	perl /home/alex/PFE/trunk/Code/migration_monitor/kerrighed-migmon-migrations-videos.pl --table ${table} --frequency 1 --accumulate

	mkdir -p ${ROOT}/VideosAcc/${table}/5min/ && cd ${ROOT}/VideosAcc/${table}/5min/
	perl /home/alex/PFE/trunk/Code/migration_monitor/kerrighed-migmon-migrations-videos.pl --table ${table} --frequency 5 --accumulate
done

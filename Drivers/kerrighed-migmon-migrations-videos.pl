#!/usr/bin/perl -w

use strict;
use warnings;
use Switch;
use DBI;
use Term::ProgressBar;
use Getopt::Long;

sub usage()
{
	print STDOUT "Unknown option: @_\n" if ( @_ );
	print STDOUT "Usage: " . $0 . "
	--table TABLENAME
		Ask to use the MySQL table named 'TABLENAME' to grab data.

	--frequency FREQVALUE
		Set agregation of data to FREQVALUE minutes.

	[--accumulate]
		Ask to count migrations from start. Default is not to accumulate.

	[--server ADDRESS]
		Use MySQL server at ADDRESS for connection.

	[--user USERNAME]
		Use MySQL user USERNAME for connection.

	[--password PASSWORD]
		Use MySQL user password PASSWORD for connection.

	[--database DATABASE]
		Use MySQL database DATABASE to retrieve data.

	[--help|-?|-h]
		Print the current help.
\n";
	exit;
}

sub validargs
{
	my $table = shift;
	my $frequency = shift;
	my $accumul = shift;
	my $dbhost = shift;
	my $dbuser = shift;
	my $dbpassw = shift;
	my $dbname = shift;

	my $retval = 1 unless (
		($table eq "")
		or ($frequency < 0)
		or ($accumul ne 1 and $accumul ne 0)
		or ($dbhost eq "")
		or ($dbuser eq "")
		or ($dbpassw eq "")
		or ($dbname eq "")
	);

	print STDERR "ERROR while validating arguments.\n" if $retval ne 1;
	return $retval;
}

my ($dbhost, $dbname, $dbuser, $dbpassw) = ("localhost", "krgmigmon", "krgmigmon", "krgmigmonpass");

my $table = '';
my $frequency = 1;
my $accumul = 0;
my $help = '';

my $opts = GetOptions(
	'table=s' => \$table,
	'frequency=i' => \$frequency,
	'accumulate' => \$accumul,
	'server:s' => \$dbhost,
	'user:s'=> \$dbuser,
	'password:s' => \$dbpassw,
	'database:s' => \$dbname,
	'help|h|?' => \$help,
);
usage() if (!$opts or $help);
exit 1 if !validargs($table, $frequency, $accumul, $dbhost, $dbuser, $dbpassw, $dbname);

my $freq_sec = $frequency*60;
my $intervalles = "
SELECT DATE_SUB(endDate, INTERVAL $frequency MINUTE) as startDate, endDate
FROM $table
GROUP BY FLOOR(UNIX_TIMESTAMP(endDate)/$freq_sec)
ORDER BY endDate ASC
";

my $nodes_int = "SELECT MIN(LEAST(startNode, endNode)) AS min, MAX(GREATEST(startNode, endNode)) AS max FROM $table LIMIT 1";
my $dbh = DBI->connect("DBI:mysql:$dbname;host=$dbhost", $dbuser, $dbpassw) || die "Could not connect to database: $DBI::errstr";

my $liste_int = $dbh->prepare($intervalles);
my $rint = $liste_int->execute();
my $i = 0;
my $pcent = 0;
my $frame = 0;
$frequency = sprintf("%02d", $frequency);

my $sthnodes_int = $dbh->prepare($nodes_int);
my $resultnodes_int = $sthnodes_int->execute();
my $minnodes_int = $sthnodes_int->fetchrow_hashref();
$sthnodes_int->finish();
my %migs;
for my $sN ( $minnodes_int->{min} .. $minnodes_int->{max} ) {
	for my $eN ( $minnodes_int->{min} .. $minnodes_int->{max} ) {
		$migs{$sN}{$eN} = 0;
	}
}

my $from_dates;
my $group_by_dates;
$from_dates = "" if $accumul eq 1;
$from_dates = ", DATE_SUB(endDate, INTERVAL $frequency MINUTE) as startDate, endDate" if $accumul eq 0;
$group_by_dates = "" if $accumul eq 1;
$group_by_dates = "FLOOR(UNIX_TIMESTAMP(endDate)/$freq_sec), " if $accumul eq 0;
my $minmax = "
SELECT MIN(inters.nbmigs) AS minmigs, MAX(inters.nbmigs) AS maxmigs
FROM (SELECT COUNT(process_id) as nbmigs $from_dates
FROM $table
GROUP BY $group_by_dates startNode, endNode
ORDER BY endDate ASC) as inters
";
my $liste_minmax = $dbh->prepare($minmax);
my $rminmax = $liste_minmax->execute();
my $ref_minmax = $liste_minmax->fetchrow_hashref();
my $minmigs = $ref_minmax->{minmigs};
my $maxmigs = $ref_minmax->{maxmigs};
my $onethird = int((1/3)*($maxmigs + $minmigs));
my $twothird = int((2/3)*($maxmigs + $minmigs));
$liste_minmax->finish();

my $acc;
$acc = "Not accumulating data." if $accumul eq 0;
$acc = "Accumulating data." if $accumul eq 1;
print STDOUT "From '$table', aggregating data on $frequency minute(s). $acc\n";
my $progress = Term::ProgressBar->new({
	name => 'Generating migrations frames',
	count => $rint,
	ETA => 'linear',
	});

my $old = $i;
while(my $r = $liste_int->fetchrow_hashref()) 
{
	my $startDate = $r->{startDate};
	my $endDate = $r->{endDate};
	my $sDate;
	$sDate = "startDate >= '$startDate' AND " if $accumul eq 0;
	$sDate = "" if $accumul eq 1;

	my $migrations = "
	SELECT COUNT(id) as nbmigs, startNode, endNode
	FROM $table
	WHERE $sDate endDate <= '$endDate'
	GROUP BY startNode, endNode
	";
	my $tot = "
	SELECT COUNT(id) as totalnbmigs
	FROM $table
	WHERE $sDate endDate <= '$endDate'
	";

	my $liste_migs = $dbh->prepare($migrations);
	my $rmigs = $liste_migs->execute();
	my $liste_tot = $dbh->prepare($tot);
	my $rtot = $liste_tot->execute();
	my $ref_tot = $liste_tot->fetchrow_hashref();

	my $totalmigs = $ref_tot->{totalnbmigs};
	my $datafile = "";

	my %_migs = %migs;
	while(my $ref = $liste_migs->fetchrow_hashref()) 
	{
		$_migs{$ref->{startNode}}{$ref->{endNode}} = $ref->{nbmigs};
	}

	for my $sN ( sort keys %migs ) {
		for my $eN ( sort keys %{$migs{$sN}} ) {
			$datafile .= $migs{$sN}{$eN} . " ";
		}
		$datafile .= "\n";
	}

	my $tics = "";
	for my $i ( $minnodes_int->{min} .. $minnodes_int->{max} ) {
		$tics .= '"' . $i . '" ' . ($i - $minnodes_int->{min});
		$tics .= ', ' unless $i eq $minnodes_int->{max};
	}

	$datafile .= "e\n";
	$frame = sprintf("%08d", $i);
	my $file = $table . "_frame_" . $frame . ".png";

	open(GNUPLOT, "|gnuplot");
	print GNUPLOT <<EOFPLOT;
set terminal png size 1280,720 enhanced
set out "$file"
set title "Evolution des migrations -- Freq:$frequency -- Frame:$frame"
set xlabel "Origine"
set ylabel "Destination"
set xtics ($tics)
set ytics ($tics)
set cbrange [$minmigs:$maxmigs]
set pm3d
set view map
set palette defined ($minmigs "blue", $onethird "green", $twothird "yellow", $maxmigs "red")
splot "-" matrix notitle
$datafile
EOFPLOT
	close(GNUPLOT);

	$liste_migs->finish();
	$liste_tot->finish();
	$i++;

	$old = $progress->update($i) if $i >= $old;
}

$liste_int->finish();
$dbh->disconnect();
print STDOUT "\n";

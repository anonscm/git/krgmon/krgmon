/**
 *
 * Copyright (c) 2009 - Alexandre Lissy <alexandre.lissy@etu.univ-tours.fr>
 * Derivated from migration_probe (c) Louis Rilling.
 *
 **/

#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/time.h>
#include <linux/proc_fs.h>
#include <linux/device.h>
#include <kerrighed/action.h>

MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("Alexandre Lissy <alexandre.lissy@etu.univ-tours.fr>");
MODULE_DESCRIPTION("Kerriged Migrations Monitor");
MODULE_VERSION("3.0.1");

static struct class *migration_monitor_class;
static struct device *migration_monitor_device;

typedef enum event_type {
	KERRIGHED_MIGRATION_START	= 0x000,
	KERRIGHED_MIGRATION_END		= 0x001,
	KERRIGHED_MIGRATION_ABORTED	= 0x010,
	KERRIGHED_PROCESS_CREATION	= 0x100,
	KERRIGHED_PROCESS_DESTRUCTION	= 0x101
} event_type_t ;

typedef struct migration_event {
	event_type_t event;
	pid_t pid;
	kerrighed_node_t start_node;
	kerrighed_node_t end_node;
	struct timespec start_date;
	struct timespec end_date;
	int complete;
	struct list_head migration_event_list;
} migration_event_t;

static LIST_HEAD(migration_monitor_event_list);
DECLARE_MUTEX(migration_monitor_event_list_mutex);
static int major_allocated;

static struct notifier_block migration_end_nb;
static struct notifier_block process_life_birth_nb;
static struct notifier_block process_life_death_nb;

static ssize_t migration_monitor_read(struct file *, char __user *, size_t, loff_t *);
static void migration_monitor_mig_manage(struct epm_action*, event_type_t);
static int kmcb_migration_end(struct notifier_block *notifier, unsigned long arg, void *data);
static void process_monitor_creation(struct task_struct *tsk, event_type_t event);
static void process_monitor_destruction(pid_t tsk_pid, event_type_t event);
static int kmcb_process_life_handling(struct notifier_block *notifier, unsigned long arg, void *data);
static int __init migration_monitor_init(void);
static void __exit migration_monitor_exit(void);

static struct file_operations migration_monitor_fops = {
	.owner	= THIS_MODULE,
	.read	= migration_monitor_read
};

module_init(migration_monitor_init);
module_exit(migration_monitor_exit);

#!/usr/bin/perl -w

use strict;
use warnings;
use Switch;
use DBI;

use constant KERRIGHED_MIGRATION_START		=> 0x000;
use constant KERRIGHED_MIGRATION_END		=> 0x001;
use constant KERRIGHED_MIGRATION_ABORTED	=> 0x010;
use constant KERRIGHED_PROCESS_CREATION		=> 0x100;
use constant KERRIGHED_PROCESS_DESTRUCTION	=> 0x101;

use constant VALID_FIELD_STARTDATE	=> KERRIGHED_MIGRATION_START | KERRIGHED_PROCESS_CREATION | KERRIGHED_PROCESS_DESTRUCTION;
use constant INVALID_FIELD_STARTDATE	=> ~VALID_FIELD_STARTDATE;

use constant VALID_FIELD_ENDDATE	=> KERRIGHED_MIGRATION_END | KERRIGHED_PROCESS_CREATION | KERRIGHED_PROCESS_DESTRUCTION;
use constant INVALID_FIELD_ENDDATE	=> ~VALID_FIELD_ENDDATE;

use constant VALID_FIELD_STARTNODE	=> KERRIGHED_MIGRATION_START | KERRIGHED_PROCESS_CREATION | KERRIGHED_PROCESS_DESTRUCTION;
use constant INVALID_FIELD_STARTNODE	=> ~VALID_FIELD_STARTNODE;

use constant VALID_FIELD_ENDNODE	=> KERRIGHED_MIGRATION_END | KERRIGHED_PROCESS_CREATION | KERRIGHED_PROCESS_DESTRUCTION;
use constant INVALID_FIELD_ENDNODE	=> ~VALID_FIELD_ENDNODE;

sub is_valid_startdate
{
	my $v = shift;
	return is_valid_field($v, VALID_FIELD_STARTDATE);
}

sub is_valid_enddate
{
	my $v = shift;
	return is_valid_field($v, VALID_FIELD_ENDDATE);
}

sub is_valid_startnode
{
	my $v = shift;
	return is_valid_field($v, VALID_FIELD_STARTNODE);
}

sub is_valid_endnode
{
	my $v = shift;
	return is_valid_field($v, VALID_FIELD_ENDNODE);
}

sub is_valid_field
{
	my $value = shift;
	my $field = shift;

	return ($value & $field) eq $value;
}

my ($dbhost, $dbname, $dbuser, $dbpassw) = ("localhost", "krgmigmon", "krgmigmon", "krgmigmonpass");

my $host = shift;
my $ip = shift;

my ($arg, $arg1, $arg2, %Valeurs);

open(SYSLOG, ">>/var/log/migration_monitor.log") or die("Cannot open log");

my $head = <STDIN>;
$head = <STDIN>;
while ($arg = <STDIN>) {
	if ($arg =~ /(.*) (.*)/) {
		$arg1 = $1;
		$arg2 = $2;
		$arg2 =~ s/\"//g;
		$Valeurs{$arg1} = $arg2;
	}
}

my (
	$krgMigPid, $krgMigStartDate, $krgMigEndDate,
	$krgMigStartNode, $krgMigEndNode, $krgMigType
   );

$krgMigPid		= $Valeurs{"KERRIGHED-MONITORING-MIB::krgMigrationPid"};
$krgMigStartDate	= $Valeurs{"KERRIGHED-MONITORING-MIB::krgMigrationStartDate"};
$krgMigEndDate		= $Valeurs{"KERRIGHED-MONITORING-MIB::krgMigrationEndDate"};
$krgMigStartNode	= $Valeurs{"KERRIGHED-MONITORING-MIB::krgMigrationStartNode"};
$krgMigEndNode		= $Valeurs{"KERRIGHED-MONITORING-MIB::krgMigrationEndNode"};
$krgMigType		= $Valeurs{"KERRIGHED-MONITORING-MIB::krgMigrationType"};

# Check for each argument
die("Invalid PID.") unless $krgMigPid =~ /\d+/;
die("Invalid MigType.") unless $krgMigType =~ /\d/;
die("Invalid StartDate.") unless ($krgMigStartDate =~ /\d+/ and is_valid_startdate($krgMigType));
die("Invalid StartNode.") unless ($krgMigStartNode =~ /\d+/ and is_valid_startnode($krgMigType));
die("Invalid EndDate.") unless ($krgMigEndDate =~ /\d+/ and is_valid_enddate($krgMigType));
die("Invalid EndNode.") unless ($krgMigEndNode =~ /\d+/ and is_valid_endnode($krgMigType));

my $dbh = DBI->connect("DBI:mysql:$dbname;host=$dbhost", $dbuser, $dbpassw) || die "Could not connect to database: $DBI::errstr";

switch ($krgMigType)
{
	case KERRIGHED_MIGRATION_START
	{
		my $insertquery = "INSERT INTO tmpMigStart (id_tmpmig, pid, startDate, startNode) VALUES ('', '$krgMigPid', '$krgMigStartDate', '$krgMigStartNode')";
		print SYSLOG "$insertquery\n";
		my $sth = $dbh->prepare($insertquery);
		my $count = $sth->execute();
		print SYSLOG "Added $count row.\n";
	}

	case KERRIGHED_MIGRATION_END
	{
		# we're good !
		my $insertmig = "INSERT INTO migrations
(process_id, startDate, endDate, startNode, endNode, created_at, updated_at) VALUES
('$krgMigPid', FROM_UNIXTIME($krgMigStartDate), FROM_UNIXTIME($krgMigEndDate), '$krgMigStartNode', '$krgMigEndNode', NOW(), NOW())";
		my $sthmig = $dbh->prepare($insertmig);
		my $count_mig = $sthmig->execute();
		print SYSLOG "Added $count_mig migrations.\n";
	}

	case KERRIGHED_PROCESS_CREATION
	{
		# we're good !
		my $insertmig = "INSERT INTO migrations
(process_id, startDate, endDate, startNode, endNode, created_at, updated_at) VALUES
('$krgMigPid', FROM_UNIXTIME($krgMigStartDate), FROM_UNIXTIME($krgMigEndDate), '$krgMigStartNode', '$krgMigEndNode', NOW(), NOW())";
		my $sthmig = $dbh->prepare($insertmig);
		my $count_mig = $sthmig->execute();
		print SYSLOG "Added $count_mig migrations: new process appeared.\n";
	}

	case KERRIGHED_PROCESS_DESTRUCTION
	{
		# we're good !
		my $insertmig = "INSERT INTO migrations
(process_id, startDate, endDate, startNode, endNode, created_at, updated_at) VALUES
('$krgMigPid', FROM_UNIXTIME($krgMigStartDate), FROM_UNIXTIME($krgMigEndDate), '$krgMigStartNode', '$krgMigEndNode', NOW(), NOW())";
		my $sthmig = $dbh->prepare($insertmig);
		my $count_mig = $sthmig->execute();
		print SYSLOG "Added $count_mig migrations: process is dead.\n";
	}

	case KERRIGHED_MIGRATION_ABORTED
	{
		print SYSLOG "MIG ABORTED\n";

	}
}

$dbh->disconnect();

close(SYSLOG);

#!/usr/bin/perl -w

use strict;
use warnings;
use Switch;
use DBI;
use POSIX qw(strftime UINT_MAX);

my ($dbhost, $dbname, $dbuser, $dbpassw) = ("localhost", "krgmigmon", "krgmigmon", "krgmigmonpass");

my $table = shift;
my $file = shift;
my $out;
my $req = "
SELECT
	gr.startNode,
	gr.endNode,
	COUNT(process_id) AS nbmigs
FROM
	(SELECT startNode, endNode FROM `$table` GROUP BY startNode, endNode) AS gr,
	$table AS m
WHERE
	gr.startNode = m.startNode AND m.startNode <> 0
	AND gr.endNode = m.endNode AND m.endNode <> 0
GROUP BY
	gr.startNode, gr.endNode
";

my $max = "SELECT MIN(LEAST(startNode, endNode)) AS min, MAX(GREATEST(startNode, endNode)) AS max FROM $table WHERE startNode <> 0 and endNode <> 0 LIMIT 1";

my $dbh = DBI->connect("DBI:mysql:$dbname;host=$dbhost", $dbuser, $dbpassw) || die "Could not connect to database: $DBI::errstr";

my $sth = $dbh->prepare($req);
my $results = $sth->execute();
my $sthmax = $dbh->prepare($max);
my $resultmax = $sthmax->execute();
my $minmax = $sthmax->fetchrow_hashref();
$sthmax->finish();

my %migs;
for my $sN ( $minmax->{min} .. $minmax->{max} ) {
	for my $eN ( $minmax->{min} .. $minmax->{max} ) {
		$migs{$sN}{$eN} = 0;
	}
}

my $data = "";
my $maxmigs = 0;
my $minmigs = UINT_MAX;
my $onethird;
my $twothird;
my %_migs = %migs;

while (my $r = $sth->fetchrow_hashref()) {
	$maxmigs = $r->{nbmigs} if ($r->{nbmigs} > $maxmigs);
	$minmigs = $r->{nbmigs} if ($r->{nbmigs} < $minmigs);
	$_migs{$r->{startNode}}{$r->{endNode}} = $r->{nbmigs};
}

for my $sN ( sort keys %migs ) {
	for my $eN ( sort keys %{$migs{$sN}} ) {
		$data .= $migs{$sN}{$eN} . " ";
	}
	$data .= "\n";
}

$onethird = (1/3)*($maxmigs + $minmigs);
$twothird = (2/3)*($maxmigs + $minmigs);

$sth->finish();
$dbh->disconnect();
my $date = strftime("%A %d %B %Y, %T (UTC%z)", localtime());
my $tics = "";
for my $i ( $minmax->{min} .. $minmax->{max} ) {
	$tics .= '"' . $i . '" ' . ($i - $minmax->{min});
	$tics .= ', ' unless $i eq $minmax->{max};
}

open(GNUPLOT, "|gnuplot");
print GNUPLOT <<EOFPLOT;
set terminal png transparent size 1920,1080 enhanced
set out "$file"
set title "Statistiques de migration -- $date"
set xlabel "Origine"
set ylabel "Destination"
set xtics ($tics)
set ytics ($tics)
set cbrange [$minmigs:$maxmigs]
set grid
set pm3d
show grid
set view map
set palette defined ($minmigs "blue", $onethird "green", $twothird "yellow", $maxmigs "red")
splot "-" matrix notitle
$data
EOFPLOT
close(GNUPLOT);

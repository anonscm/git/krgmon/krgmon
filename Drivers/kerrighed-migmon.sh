#!/bin/sh
#
# kerrighed     start or stop kerrighed migration monitoring system
#
# Authors:	Alexandre Lissy <alexandre.lissy@etu.univ-tours.fr>
#
# Copyright:    INRIA-IRISA - 2006-2007
#               Kerlabs - 2008
#

### BEGIN INIT INFO
# Provides:          kerrighed-migmon
# Required-Start:    $kerrighed
# Required-Stop:     $kerrighed
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start kerrighed migration monitoring at boot time
# Description:       Load the kerrighed migration monitoring module.
### END INIT INFO

set -e

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

NAME=kerrighed-migmon
DESC="Kerrighed Migration Monitor"

KRG_MODULE=kerrighed-migration-monitor
MIGMON_BIN=/usr/bin/kerrighed-migmon-read.pl
MIGMON_PID=/var/run/krgmigmon.pid

CFG=/etc/default/$NAME
SCRIPTNAME=/etc/init.d/$NAME

# Read config file if it is present.
if [ -r $CFG ]; then
    . $CFG
fi

. /lib/lsb/init-functions

#
# Function that check if Kerrighed can be started
#
d_check_for_no_start() {
    case "x$ENABLE" in
	xtrue)
	    ;;
	xfalse)
	    log_end_msg 0
	    log_warning_msg "Kerrighed Migration Monitor disabled in $CFG; not starting Kerrighed Migration monitor"
	    exit 0
	    ;;
	*)
	    log_end_msg 1
	    log_failure_msg "Value of ENABLE in $CFG must be either 'true' or 'false';"
	    exit 1
	    ;;
    esac
}

#
# Function that starts the daemon/service.
#
d_start() {
    /sbin/modprobe -q $KRG_MODULE $ARGS

    if [ -d $MIGMON_DEV ]; then
	start-stop-daemon --background --pidfile $MIGMON_PID --make-pidfile --nicelevel 0 --start $NAME --exec $MIGMON_BIN
    else
	log_failure_msg "Migration monitor device not present."
    fi
}

#
# Function that stops the daemon/service.
#
d_stop() {
    start-stop-daemon --stop $NAME --pidfile $MIGMON_PID
    /sbin/rmmod $KRG_MODULE
    true
}

case "$1" in
    start)
	log_begin_msg "Starting $DESC: "
	d_start || log_end_msg 1
	log_end_msg 0
	;;
    stop)
	log_begin_msg "Stopping $DESC: "
	d_stop || log_end_msg 1
	log_end_msg 0
	;;
    restart)
	log_begin_msg "Restart $DESC: "
	d_stop || log_end_msg 1
	d_check_for_no_start
	d_start || log_end_msg 1
	log_end_msg 0
	;;	
    *)
	log_success_msg "Usage: $SCRIPTNAME {start|stop|restart}"
	exit 1
	;;
esac

exit 0

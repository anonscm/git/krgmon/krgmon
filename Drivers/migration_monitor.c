#include "migration_monitor.h"
#include <kerrighed/krginit.h>
#include <kerrighed/migration.h>
#include <kerrighed/action.h>
#include <kerrighed/pid.h>
#include <kerrighed/scheduler/hooks.h>
#include <asm/uaccess.h>

static ssize_t migration_monitor_read(struct file *f, char __user *user_buff, size_t count, loff_t *offset)
{
	char *buf;
	ssize_t len = 0;
	unsigned long ret;
	migration_event_t *pos = NULL, *pos_safe = NULL;

	buf = kmalloc(sizeof(char)*PAGE_SIZE, GFP_KERNEL);
	if (!buf) {
		len = -ENOMEM;
		goto out_mem;
	}

	down(&migration_monitor_event_list_mutex);
	if (!list_empty(&migration_monitor_event_list)) {
		list_for_each_entry_safe(pos, pos_safe, &migration_monitor_event_list, migration_event_list)
		{
			len += sprintf(buf + len, "%d:%d:%d:%lld:%d:%lld\n", pos->event, pos->pid,
				pos->start_node, timespec_to_ns(&pos->start_date),
				pos->end_node, timespec_to_ns(&pos->end_date));
			list_del(&pos->migration_event_list);
			kfree(pos);
		}
	}
	up(&migration_monitor_event_list_mutex);

	ret = copy_to_user(user_buff, buf, len);
	if (ret > 0) {
		printk(KERN_ERR "[%s]: Something wrong: copy_to_user() returned %lu!\n", __PRETTY_FUNCTION__, ret);
	}

	kfree(buf);
out_mem:
	
	return len;
}

static void migration_monitor_mig_manage(struct epm_action *action, event_type_t event)
{
	migration_event_t *ev; 
	ev = kmalloc(sizeof(migration_event_t), GFP_ATOMIC);

	if (action && event == KERRIGHED_MIGRATION_END) {
		printk(KERN_ERR "migration_monitor: On node %d, process #%d migration event: %d ...\n", kerrighed_node_id, action->migrate.pid, event); 
		ev->event = event;
		ev->pid = action->migrate.pid;
		ev->start_node = action->migrate.source;
		ev->end_node = action->migrate.target;
		ev->start_date = action->migrate.start_date;
		ev->end_date = action->migrate.end_date;
		down(&migration_monitor_event_list_mutex);
		list_add_tail(&ev->migration_event_list, &migration_monitor_event_list);
		up(&migration_monitor_event_list_mutex);
	}
}

static int kmcb_migration_end(struct notifier_block *notifier, unsigned long arg, void *data)
{
	migration_monitor_mig_manage(data, KERRIGHED_MIGRATION_END);
	return NOTIFY_DONE;
}

static void process_monitor_creation(struct task_struct *tsk, event_type_t event)
{
	migration_event_t *ev; 
	ev = kmalloc(sizeof(migration_event_t), GFP_ATOMIC);

	if (tsk && event == KERRIGHED_PROCESS_CREATION) {
		printk(KERN_ERR "migration_monitor: On node %d, process #%d creation event: %d ...\n", kerrighed_node_id, task_pid_knr(tsk), event); 
		ev->event = event;
		ev->pid = task_pid_knr(tsk);
		ev->start_node = 0;
		ev->end_node = kerrighed_node_id;
		ev->start_date = current_kernel_time();
		ev->start_date.tv_sec -= 1;
		ev->end_date = current_kernel_time();
		down(&migration_monitor_event_list_mutex);
		list_add_tail(&ev->migration_event_list, &migration_monitor_event_list);
		up(&migration_monitor_event_list_mutex);
	}
}

static void process_monitor_destruction(pid_t tsk_pid, event_type_t event)
{
	migration_event_t *ev; 
	ev = kmalloc(sizeof(migration_event_t), GFP_ATOMIC);

	if (event == KERRIGHED_PROCESS_DESTRUCTION) {
		printk(KERN_ERR "migration_monitor: On node %d, process #%d death event: %d ...\n", kerrighed_node_id, tsk_pid, event); 
		ev->event = event;
		ev->pid = tsk_pid;
		ev->start_node = kerrighed_node_id;
		ev->end_node = 0;
		ev->start_date = current_kernel_time();
		ev->start_date.tv_sec -= 1;
		ev->end_date = current_kernel_time();
		down(&migration_monitor_event_list_mutex);
		list_add_tail(&ev->migration_event_list, &migration_monitor_event_list);
		up(&migration_monitor_event_list_mutex);
	}
}

static int kmcb_process_life_handling(struct notifier_block *notifier, unsigned long arg, void *data)
{
	if (!data) {
		process_monitor_destruction((pid_t)arg, KERRIGHED_PROCESS_DESTRUCTION);
	} else {
		process_monitor_creation(data, KERRIGHED_PROCESS_CREATION);
	}
	return NOTIFY_DONE;
}

static int __init migration_monitor_init(void)
{
	int err;

	migration_end_nb.notifier_call = kmcb_migration_end;
	err = atomic_notifier_chain_register(&kmh_migration_recv_end, &migration_end_nb);
	if (err)
		printk(KERN_ERR "[%s]: Cannot register notifier kmh_migration_recv_end.\n", __PRETTY_FUNCTION__);

	process_life_birth_nb.notifier_call = kmcb_process_life_handling;
	err = atomic_notifier_chain_register(&kmh_process_birth, &process_life_birth_nb);
	if (err)
		printk(KERN_ERR "[%s]: Cannot register notifier kmh_process_birth.\n", __PRETTY_FUNCTION__);

	process_life_death_nb.notifier_call = kmcb_process_life_handling;
	err = atomic_notifier_chain_register(&kmh_process_death, &process_life_death_nb);
	if (err)
		printk(KERN_ERR "[%s]: Cannot register notifier kmh_process_death.\n", __PRETTY_FUNCTION__);

	major_allocated = register_chrdev(0, "migration_monitor", &migration_monitor_fops);

	migration_monitor_class = class_create(THIS_MODULE, "migration_monitor");
	migration_monitor_device = device_create(migration_monitor_class, NULL, MKDEV(major_allocated, 0), NULL, "migration_monitor");

	return 0;
}

static void __exit migration_monitor_exit(void)
{
	device_destroy(migration_monitor_class, MKDEV(major_allocated, 0));
	class_destroy(migration_monitor_class);
	unregister_chrdev(major_allocated, "migration_monitor");
	atomic_notifier_chain_unregister(&kmh_migration_recv_end, &migration_end_nb);
	atomic_notifier_chain_unregister(&kmh_process_birth, &process_life_birth_nb);
	atomic_notifier_chain_unregister(&kmh_process_death, &process_life_death_nb);
}

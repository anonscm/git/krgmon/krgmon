#!/bin/sh

perl /usr/sbin/kerrighed-migmon-insert-mysql.pl host ip \
	KERRIGHED-MONITORING-MIB::krgMigrationPid 1232 \
	KERRIGHED-MONITORING-MIB::krgMigrationStartDate "1246368852.72501" \
	KERRIGHED-MONITORING-MIB::krgMigrationEndDate "" \
	KERRIGHED-MONITORING-MIB::krgMigrationStartNode 10 \
	KERRIGHED-MONITORING-MIB::krgMigrationEndNode 0 \
	KERRIGHED-MONITORING-MIB::krgMigrationType 0 \
	SNMPv2-MIB::snmpTrapOID.0 KERRIGHED-MONITORING-MIB::krgMigrationNotif

perl /usr/sbin/kerrighed-migmon-insert-mysql.pl host ip \
	KERRIGHED-MONITORING-MIB::krgMigrationPid 1232 \
	KERRIGHED-MONITORING-MIB::krgMigrationStartDate "" \
	KERRIGHED-MONITORING-MIB::krgMigrationEndDate "1246368854.72501" \
	KERRIGHED-MONITORING-MIB::krgMigrationStartNode 0 \
	KERRIGHED-MONITORING-MIB::krgMigrationEndNode 11 \
	KERRIGHED-MONITORING-MIB::krgMigrationType 1 \
	SNMPv2-MIB::snmpTrapOID.0 KERRIGHED-MONITORING-MIB::krgMigrationNotif

#!/usr/bin/perl -w

use strict;
use warnings;

use Net::SNMP;

my $snmp_host = "192.168.5.1";
my $snmp_comm = "public";

my ($session, $errors) = Net::SNMP->session(
		-hostname	=> $snmp_host,
		-community	=> $snmp_comm,
		-port		=> 162,
		-version	=> 'snmpv2c',
) or die ("Cannot instantiate a SNMP session.");

my $result_start= $session->snmpv2_trap(
	-varbindlist	=> [
		# 'KERRIGHED-MONITORING-MIB::krgMigrationPid',
		'.1.3.6.1.4.1.8192.1.1.1.1.1',
			INTEGER32,
			'1232',
		# 'KERRIGHED-MONITORING-MIB::krgMigrationStartDate',
		'.1.3.6.1.4.1.8192.1.1.1.1.2',
			OCTET_STRING,
			'1246282400000000000',
		# 'KERRIGHED-MONITORING-MIB::krgMigrationEndDate',
		'.1.3.6.1.4.1.8192.1.1.1.1.3',
			OCTET_STRING,
			'0',
		# 'KERRIGHED-MONITORING-MIB::krgMigrationStartNode',
		'.1.3.6.1.4.1.8192.1.1.1.1.4',
			INTEGER32,
			'10',
		# 'KERRIGHED-MONITORING-MIB::krgMigrationEndNode',
		'.1.3.6.1.4.1.8192.1.1.1.1.5',
			INTEGER32,
			'0',
		# 'KERRIGHED-MONITORING-MIB::krgMigrationType',
		'.1.3.6.1.4.1.8192.1.1.1.1.6',
			INTEGER32,
			'0',
		# 'SNMPv2-MIB::snmpTrapOID.0',
		'1.3.6.1.6.3.1.1.4.1.0',
			OBJECT_IDENTIFIER,
			'.1.3.6.1.4.1.8192.1.1.1.2'
			# 'KERRIGHED-MONITORING-MIB::krgMigrationNotif'
	]
);

my $result_stop= $session->snmpv2_trap(
	-varbindlist	=> [
		# 'KERRIGHED-MONITORING-MIB::krgMigrationPid',
		'.1.3.6.1.4.1.8192.1.1.1.1.1', 
			INTEGER32,
			'1232',
		# 'KERRIGHED-MONITORING-MIB::krgMigrationStartDate',
		'.1.3.6.1.4.1.8192.1.1.1.1.2', 
			OCTET_STRING,
			'',
		# 'KERRIGHED-MONITORING-MIB::krgMigrationEndDate',
		'.1.3.6.1.4.1.8192.1.1.1.1.3', 
			OCTET_STRING,
			'1246284400000000000',
		# 'KERRIGHED-MONITORING-MIB::krgMigrationStartNode',
		'.1.3.6.1.4.1.8192.1.1.1.1.4', 
			INTEGER32,
			'0',
		# 'KERRIGHED-MONITORING-MIB::krgMigrationEndNode',
		'.1.3.6.1.4.1.8192.1.1.1.1.5', 
			INTEGER32,
			'11',
		# 'KERRIGHED-MONITORING-MIB::krgMigrationType',
		'.1.3.6.1.4.1.8192.1.1.1.1.6', 
			INTEGER32,
			'1',
		# 'SNMPv2-MIB::snmpTrapOID.0',
		'1.3.6.1.6.3.1.1.4.1.0',
			OBJECT_IDENTIFIER,
			'.1.3.6.1.4.1.8192.1.1.1.2'
			# 'KERRIGHED-MONITORING-MIB::krgMigrationNotif'
	]
);

if (!defined($result_start)) {
	printf("KRGSTART:ERROR: %s.\n", $session->error());
} else {
	printf("SNMPv2-Trap-PDU sent : start\n");
}

if (!defined($result_stop)) {
	printf("KRGSTOP:ERROR: %s.\n", $session->error());
} else {
	printf("SNMPv2-Trap-PDU sent : stop\n");
}

$session->close();

exit 0;

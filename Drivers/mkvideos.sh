#!/bin/sh

# Ran inside CHROOT
ROOT=/home/alex/tmp
TABLES="migrations_session3 migrations_session2 migrations_session1 migrations_session0 migrations"

mkdir -p ${ROOT}/Videos/ ${ROOT}/VideosAcc/
for table in ${TABLES};
do
	cd ${ROOT}/Videos/${table}/
	ffmpeg -i 1min/${table}_frame_%08d.png -y -an -vcodec libx264 -vpre hq -b $((1024*1024)) -threads 0 ${table}_1min.mp4
	ffmpeg -i 5min/${table}_frame_%08d.png -y -an -vcodec libx264 -vpre hq -b $((1024*1024)) -threads 0 ${table}_5min.mp4

	cd ${ROOT}/VideosAcc/${table}/
	ffmpeg -i 1min/${table}_frame_%08d.png -y -an -vcodec libx264 -vpre hq -b $((1024*1024)) -threads 0 ${table}_1min_acc.mp4
	ffmpeg -i 5min/${table}_frame_%08d.png -y -an -vcodec libx264 -vpre hq -b $((1024*1024)) -threads 0 ${table}_5min_acc.mp4
done

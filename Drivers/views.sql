CREATE VIEW migrations_stats_min AS SELECT COUNT(process_id) as pidCount, DATE_FORMAT(endDate, '%Y-%m-%d %H:%i') as endDateMin FROM `migrations` GROUP BY endDateMin

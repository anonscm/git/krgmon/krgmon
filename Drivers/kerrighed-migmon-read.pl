#!/usr/bin/perl -w

use strict;
use warnings;

use Time::HiRes qw(usleep);
use Time::Zone;
use DateTime;
use Net::SNMP;
use Sys::Syslog;

use constant KERRIGHED_MIGRATION_START		=> 0x000;
use constant KERRIGHED_MIGRATION_END		=> 0x001;
use constant KERRIGHED_MIGRATION_ABORTED	=> 0x010;
use constant KERRIGHED_PROCESS_CREATION		=> 0x100;
use constant KERRIGHED_PROCESS_DESTRUCTION	=> 0x101;

my $snmp_host = "192.168.5.1";
my $snmp_comm = "public";

my ($session, $errors) = Net::SNMP->session(
		-hostname	=> $snmp_host,
		-community	=> $snmp_comm,
		-port		=> 162,
		-version	=> 'snmpv2c',
) or die ("Cannot instantiate a SNMP session.");

sub envoyer_trap {
	my $action = shift;
	my $pid = shift;
	my $snode = shift;
	my $sdate = shift;
	my $enode = shift;
	my $edate = shift;
	my ($start_date, $end_date, $start_node, $end_node);

	if ( ($action eq KERRIGHED_MIGRATION_START)
	or   ($action eq KERRIGHED_MIGRATION_END)
	or   ($action eq KERRIGHED_MIGRATION_ABORTED)
	or   ($action eq KERRIGHED_PROCESS_CREATION)
	or   ($action eq KERRIGHED_PROCESS_DESTRUCTION)
	   ) {
		$start_date = $sdate;
		$start_node = $snode;
		$end_date = $edate;
		$end_node = $enode;
	} 

	my $result_start= $session->snmpv2_trap(
		-varbindlist	=> [
		# 'KERRIGHED-MONITORING-MIB::krgMigrationPid',
		'.1.3.6.1.4.1.8192.1.1.1.1.1',
			INTEGER32,
			$pid,
		# 'KERRIGHED-MONITORING-MIB::krgMigrationStartDate',
		'.1.3.6.1.4.1.8192.1.1.1.1.2',
			OCTET_STRING,
			"$start_date",
		# 'KERRIGHED-MONITORING-MIB::krgMigrationEndDate',
		'.1.3.6.1.4.1.8192.1.1.1.1.3',
			OCTET_STRING,
			"$end_date",
		# 'KERRIGHED-MONITORING-MIB::krgMigrationStartNode',
		'.1.3.6.1.4.1.8192.1.1.1.1.4',
			INTEGER32,
			$start_node,
		# 'KERRIGHED-MONITORING-MIB::krgMigrationEndNode',
		'.1.3.6.1.4.1.8192.1.1.1.1.5',
			INTEGER32,
			$end_node,
		# 'KERRIGHED-MONITORING-MIB::krgMigrationType',
		'.1.3.6.1.4.1.8192.1.1.1.1.6',
			INTEGER32,
			$action,
		# 'SNMPv2-MIB::snmpTrapOID.0',
		'1.3.6.1.6.3.1.1.4.1.0',
			OBJECT_IDENTIFIER,
			# 'KERRIGHED-MONITORING-MIB::krgMigrationNotif'
			'.1.3.6.1.4.1.8192.1.1.1.2'
		]
	);
}

sub traiter_donnees {
	my $data = shift;
	my @lines = split(/\n/, $data);
	print "Received ".  @lines . " lines: \n";
	foreach my $line (@lines) {
		traiter_ligne($line);
	}
}

sub traiter_ligne {
	my $ligne = shift;
	my ($action, $pid, $startnode, $startdate, $endnode, $enddate) = split(/:/, $ligne);

	my $startepoch = $startdate/1000000000;
	my $endepoch = $enddate/1000000000;
	my $dtstart = DateTime->from_epoch(epoch => $startepoch, time_zone => 'UTC');
	my $dtend = DateTime->from_epoch(epoch => $endepoch, time_zone => 'UTC');
	$startepoch = $dtstart->strftime("%s") - tz_local_offset();
	$endepoch = $dtend->strftime("%s") - tz_local_offset();
	syslog("info", "Migration Event: $action on node $endnode from node $startnode, targetting process $pid, starting at $startepoch ending at $endepoch.");
	envoyer_trap ($action, $pid, $startnode, $startepoch, $endnode, $endepoch);
}

open(MIGMON, "/dev/migration_monitor") or die "Can't open Kerrighed Migration Monitor file : $!";
openlog("KerrighedMigMon", "ndelay,pid", "info");

my ($data, $result);
while (1) {
	$result = sysread(MIGMON, $data, 4096);
	if (not(defined($result))) {
		last;
	}
	if ($result gt 0) {
		traiter_donnees($data);
	}
	usleep(500000);
}

closelog();
close(MIGMON);

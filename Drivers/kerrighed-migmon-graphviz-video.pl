#!/usr/bin/perl -w

use strict;
use warnings;
use Switch;
use DBI;
use GraphViz;
use Image::Magick;

my ($dbhost, $dbname, $dbuser, $dbpassw) = ("localhost", "krgmigmon", "krgmigmon", "krgmigmonpass");

my $table = shift;
my $intervalles = "
SELECT DATE_SUB(endDate, INTERVAL 5 MINUTE) as startDate, endDate
FROM $table
GROUP BY FLOOR(UNIX_TIMESTAMP(endDate)/300)
ORDER BY endDate ASC
";
my $dbh = DBI->connect("DBI:mysql:$dbname;host=$dbhost", $dbuser, $dbpassw) || die "Could not connect to database: $DBI::errstr";

my $liste_int = $dbh->prepare($intervalles);
my $rint = $liste_int->execute();
my $i = 0;
my $pcent = 0;
my $frame = 0;

while(my $r = $liste_int->fetchrow_hashref()) 
{
	my $startDate = $r->{startDate};
	my $endDate = $r->{endDate};

	my $migrations = "
	SELECT COUNT(id) as nbmigs, startNode, endNode
	FROM $table
	WHERE startDate >= '$startDate' AND endDate <= '$endDate'
	GROUP BY startNode, endNode
	";
	my $tot = "
	SELECT COUNT(id) as totalnbmigs
	FROM $table
	WHERE startDate >= '$startDate' AND endDate <= '$endDate'
	";
	my $minmax = "
	SELECT MIN(liste.nbmigs) AS min, MAX(liste.nbmigs) AS max FROM (SELECT COUNT(id) as nbmigs
	FROM $table
	WHERE startDate >= '$startDate' AND endDate <= '$endDate'
	GROUP BY startNode, endNode) AS liste
	";

	my $liste_migs = $dbh->prepare($migrations);
	my $rmigs = $liste_migs->execute();
	my $liste_tot = $dbh->prepare($tot);
	my $rtot = $liste_tot->execute();
	my $ref_tot = $liste_tot->fetchrow_hashref();
	my $liste_minmax = $dbh->prepare($minmax);
	my $rminmax = $liste_minmax->execute();
	my $ref_minmax = $liste_minmax->fetchrow_hashref();

	my $totalmigs = $ref_tot->{totalnbmigs};
	my $minmigs = $ref_minmax->{min};
	my $maxmigs = $ref_minmax->{min};

	my $g = GraphViz->new(
		directed => 1,
		layout => 'dot',
		splines => 'true',
	);

	while(my $ref = $liste_migs->fetchrow_hashref()) 
	{
		my $w = 1 + (log($ref->{nbmigs} / $totalmigs) - log($minmigs / $totalmigs));
		
		$g->add_node(
			"N" . $ref->{startNode},
			style => 'filled',
			color => 'lightblue3',
		);

		$g->add_node(
			"N" . $ref->{endNode},
			style => 'filled',
			color => 'lightblue3',
		);

		$g->add_edge(
			"N" . $ref->{startNode} => "N" . $ref->{endNode},
			label => $ref->{nbmigs},
			dir => 'forward',
			weight => $w,
			style => 'setlinewidth(' . $w . ')',
			color => random_color(),
		);
	}

	$frame = sprintf("%08d", $i);
	my $file = "migrations_frame_$frame";
	if ($g->as_svg("$file.svg")) {
		my $image = Image::Magick->new();
		
		if (!$image->Read('svg:' . $file . '.svg')) {
			$image->Resize(geometry => '1280x800!');
			if (!$image->Write('png:' . $file . '.png')) {
				unlink("$file.svg");
				$pcent = sprintf("%0.2f", ($i / $rint) * 100);
				print STDERR "\rFrame #$i/$rint ($pcent%)";
			} else {
				die ("Problem while writing $file.png: $!");
			}
		} else {
			die ("Problem while reading $file.svg: $!");
		}
	}

	$liste_migs->finish();
	$liste_tot->finish();
	$liste_minmax->finish();
	$i++;
}

$dbh->disconnect();
print STDERR "\n";

sub random_color {
	my ($r, $g, $b) = (int(rand(256)), int(rand(256)), int(rand(256)));
	my $color2 = sprintf("#%02x%02x%02x", ($r + 128) % 256, ($g + 128) % 256, ($b + 128) % 256);
	return $color2;
}


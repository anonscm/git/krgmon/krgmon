#!/usr/bin/perl -w

use strict;
use warnings;
use Switch;
use DBI;
use GraphViz;
use Digest::MD5 qw(md5 md5_hex md5_base64);

my ($dbhost, $dbname, $dbuser, $dbpassw) = ("localhost", "krgmigmon", "krgmigmon", "krgmigmonpass");

my $migrations = "
SELECT COUNT(id) as nbmigs, startNode, endNode
FROM migrations
GROUP BY startNode, endNode
";
my $tot = "
SELECT COUNT(id) as totalnbmigs
FROM migrations
";
my $minmax = "
SELECT MIN(liste.nbmigs) AS min, MAX(liste.nbmigs) AS max FROM (SELECT COUNT(id) as nbmigs
FROM migrations
GROUP BY startNode, endNode) AS liste
";
my $dbh = DBI->connect("DBI:mysql:$dbname;host=$dbhost", $dbuser, $dbpassw) || die "Could not connect to database: $DBI::errstr";

my $liste_migs = $dbh->prepare($migrations);
my $rmigs = $liste_migs->execute();
my $liste_tot = $dbh->prepare($tot);
my $rtot = $liste_tot->execute();
my $ref_tot = $liste_tot->fetchrow_hashref();
my $liste_minmax = $dbh->prepare($minmax);
my $rminmax = $liste_minmax->execute();
my $ref_minmax = $liste_minmax->fetchrow_hashref();

my $totalmigs = $ref_tot->{totalnbmigs};
my $minmigs = $ref_minmax->{min};
my $maxmigs = $ref_minmax->{min};

my $g = GraphViz->new(
	directed => 1,
	layout => 'dot',
	splines => 'true',
);

$g->add_node(`date -R`, shape => 'plaintext');
while(my $ref = $liste_migs->fetchrow_hashref()) 
{
	my $w = 1 + (log($ref->{nbmigs} / $totalmigs) - log($minmigs / $totalmigs));
	
	$g->add_node(
		"N" . $ref->{startNode},
		style => 'filled',
		color => 'lightblue3',
	);

	$g->add_node(
		"N" . $ref->{endNode},
		style => 'filled',
		color => 'lightblue3',
	);

	$g->add_edge(
		"N" . $ref->{startNode} => "N" . $ref->{endNode},
		label => $ref->{nbmigs},
		dir => 'forward',
		weight => $w,
		style => 'setlinewidth(' . $w . ')',
		color => random_color("N" . $ref->{startNode}, "N" . $ref->{endNode}),
	);
}

$liste_migs->finish();
$liste_tot->finish();
$liste_minmax->finish();
$dbh->disconnect();

print STDOUT $g->as_png();

sub random_color {
	my $src = shift;
	my $dest= shift;
	my $lbl = $src . $dest;
	my @chrs = ();

	foreach my $c (split(//, $lbl)) {
		push(@chrs, $c);
	}

	my @md5 = split(//, md5_hex(join("", @chrs)));
	my $color = "#" . $md5[6] . $md5[11] . $md5[16] . $md5[21] . $md5[26] . $md5[31];
	
	return $color;
}


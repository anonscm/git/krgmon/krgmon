#!/usr/bin/perl -w

use strict;
use warnings;
use Switch;
use DBI;
use DateTime::Format::Strptime;
use Time::Zone;

my ($dbhost, $dbname, $dbuser, $dbpassw) = ("localhost", "krgmigmon", "krgmigmon", "krgmigmonpass");

my $table = shift;
my $file = shift;
my $out;
my $req = "SELECT COUNT(process_id) AS pidCount,
DATE_FORMAT(endDate, '%Y-%m-%d %H:%i:00') AS endDateMin
FROM $table
GROUP BY DATE_FORMAT(endDate, '%Y-%m-%d %H:%i')";

my $dbh = DBI->connect("DBI:mysql:$dbname;host=$dbhost", $dbuser, $dbpassw) || die "Could not connect to database: $DBI::errstr";
my $parser =
    DateTime::Format::Strptime->new(
		pattern => '%Y-%m-%d %H:%M',
		time_zone => 'UTC'
	);

if(defined($file)) {
	print STDERR "Using $file as output.\n";
	open(GNUPLOT, ">$file") or die "Cannot open file $file";
} else {
	die("No output file specified.");
}

my $sth = $dbh->prepare($req);
my $results = $sth->execute();

print GNUPLOT "# endDateMin pidCount\n";
while (my $r = $sth->fetchrow_hashref()) {
	my $dt = $parser->parse_datetime($r->{endDateMin});
	$dt->set_time_zone('Europe/Paris');
	my $endDateMin = $dt->strftime("%Y-%m-%d %H:%M");
	print GNUPLOT "$endDateMin $r->{pidCount}\n";
}

$sth->finish();
$dbh->disconnect();
close(GNUPLOT);

#!/bin/sh

DATA=$1

if [ ! -f ${DATA} ]; then
	echo "Invalid data file: ${DATA}"
	exit 1
fi;

DATE=$(date -R)
GNUPLOT=${DATA}.gnuplot

cat >${GNUPLOT} <<EOF
set terminal png transparent size 1920,1080 enhanced
set out "${DATA}.png"
set title "Statistiques de migration -- ${DATE}"
set xlabel "Origine"
set ylabel "Destination"
set grid
set view map
show grid
set palette rgbformulae 22,13,-31
splot "${DATA}" with pm3d palette notitle
EOF

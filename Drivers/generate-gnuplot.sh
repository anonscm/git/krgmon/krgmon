#!/bin/sh

DATA=$1

if [ ! -f ${DATA} ]; then
	echo "Invalid data file: ${DATA}"
	exit 1
fi;

DATE=$(date -R)
GNUPLOT=${DATA}.gnuplot

cat >${GNUPLOT} <<EOF
set terminal png transparent size 1920,1080 enhanced
set out "${DATA}.png"
set title "Statistiques de migration -- ${DATE}"
set xlabel "Date"
set xdata time
set timefmt "%Y-%m-%d %H:%M"
set format x "%Y-%m-%d %H:%M"
set xtics nomirror rotate
set ylabel "Nombre de migrations"
set grid
show grid
plot	"${DATA}" using 1:3 with impulses notitle
EOF

class CreateMigrations < ActiveRecord::Migration
  def self.up
    create_table :migrations do |t|
      t.integer :process_id, :null => false, :default => 0
      t.timestamp :startDate
      t.timestamp :endDate
      t.integer :startNode
      t.integer :endNode
      t.timestamps
    end
  end

  def self.down
    drop_table :migrations
  end
end

/**
CLUSTER Cluster vizualisation for kerrighed
Copyright (c) 2010, Université François Rabelais - Tours - France
*/

/**
* @class Draw a line between the node and the process
* @param {Integer} Node id
* @param {Cluster.Process} Instance of the process
* @constructor
*/
Cluster.Mark = function(startNode, process){
	

    /**
     * @constant
     * @description The duration of the mark
     */
	this.duration = 50000;
	/**
	 * Identifier of the mark
	 */
	this.id = null;
	/**
	 * The node 
	 */
	this.startNode = null;
	/**
	 * The process 
	 */
	this.process = null;
	/**
	 * Drawable that will be displayed in the scene
	 */
	this.objectGL = null;
	/**
	 * The time when the transition has begun
	 */
	this.startTime = null;
	
	/**
	 * Size of the line
	 */
	this.scale = 0.1;
	/**
	 * Initial color
	 */
	this.colorBegin = null;	
	/**
	 * End color
	 */
	this.colorEnd = null;	
	/**
	 * The material containing the color
	 */
	this.material = null;
	/**
	 * The speed on red component
	 */
	this.speedRed = null;
	/**
	 * The speed on green component
	 */
	this.speedGreen = null;
	/**
	 * The speed on blue component
	 */
	this.speedBlue = null;
	
	this.initialize(startNode, process);
};

/**
 * Initialize the object
* @param {Cluster.Node} Node
* @param {Cluster.Process} Instance of the process
 */
Cluster.Mark.prototype.initialize = function(startNode, process){
	
    this.setId(Cluster.getMarksNb());
    this.setStartNode(startNode);
    this.setProcess(process);
    
    this.setStartTime(parseInt(new Date().getTime()));

    this.setObjectGL(new GLGE.Object());
    this.getObjectGL().setScale(this.getScale(), this.getScale(), this.getScale());

    
    var sphereMesh = new GLGE.Mesh();
    sphereMesh = Cluster.getDocumentScene().getElement("cube");
    
    this.getObjectGL().setMesh(sphereMesh, 0);

    // We create a new material to set on the node.
    this.setMaterial(new GLGE.Material());
    // and we set his color to processColor
    this.getMaterial().setColor(this.getStartNode().getColor());
    //material.setAlpha(0);
    this.getObjectGL().setMaterial(this.getMaterial());


    this.setColorEnd(GLGE.colorParse("#777"));
    this.setColorBegin(GLGE.colorParse(this.getStartNode().getColor()));

    this.setSpeedRed((this.getColorEnd().r - this.getColorBegin().r) / this.getDuration());
    this.setSpeedGreen((this.getColorEnd().g - this.getColorBegin().g) / this.getDuration());
    this.setSpeedBlue((this.getColorEnd().b - this.getColorBegin().b) / this.getDuration());

    
    Cluster.getAllMarks()[this.getId()] = this;
    Cluster.incMarksNb();
    
    ClusterManager.getSingleton().getObjectScene().addObject(this.getObjectGL());
    
}


/**
 * Destroy the object
 */
Cluster.Mark.prototype.destroy = function()
{
    delete Cluster.getAllMarks()[this.getId()];

    ClusterManager.getSingleton().getObjectScene().removeChild(this.getObjectGL());
    
    delete this.getObjectGL().getMaterial(0);
    delete this.getObjectGL().getMesh(0);
    delete this.getObjectGL();

    Cluster.decMarksNb();
    
    delete this;
}


Cluster.Mark.prototype.hide = function(){
	this.getObjectGL().setScale(0, 0, 0);
	this.destroy();
}

Cluster.Mark.prototype.render = function(){
    var elapsedTime = parseInt(new Date().getTime()) - this.getStartTime();
    if(elapsedTime > this.getDuration()){
        this.hide();
    }
    else{
        var r = this.getColorBegin().r + this.getSpeedRed() * elapsedTime;
        var g = this.getColorBegin().g + this.getSpeedGreen() * elapsedTime;
        var b = this.getColorBegin().b + this.getSpeedBlue() * elapsedTime;

        this.getMaterial().setColorR(r);
        this.getMaterial().setColorG(g);
        this.getMaterial().setColorB(b);

        var startX = this.getStartNode().getObjectGL().getLocX();
        var startY = this.getStartNode().getObjectGL().getLocY();
        var startZ = this.getStartNode().getObjectGL().getLocZ();

        var endX = this.getProcess().getObjectGL().getLocX();
        var endY = this.getProcess().getObjectGL().getLocY();
        var endZ = this.getProcess().getObjectGL().getLocZ();

        var distance = Math.sqrt(Math.pow(endX-startX, 2) + Math.pow(endY-startY, 2) + Math.pow(endZ-startZ, 2));

        // Here, we set the size of the node to NODE_SIZE
        this.getObjectGL().setScaleX(distance / 2);

        this.getObjectGL().setLocX((endX + startX) / 2);
        this.getObjectGL().setLocY((endY + startY) / 2);
        this.getObjectGL().setLocZ((endZ + startZ) / 2);



        var coeff = 0;
        if(endX <= startX && endZ <= startZ){
            coeff = -1;
        }
        if(endX > startX && endZ <= startZ){
            coeff =  1;
        }
        if(endX > startX && endZ > startZ){
            coeff =  -1;
        }
        if(endX <= startX && endZ > startZ){
            coeff =  1;
        }

        this.getObjectGL().setRotY(
            coeff * Math.acos( Math.sqrt( Math.pow(endX - startX, 2) ) / distance )
        );
    }
}



// Getter - Setter


/**
 * Getter for the attribute "id"
 * @return {Integer} Id value
 */
Cluster.Mark.prototype.getId = function(){
	return this.id;
};
/**
 * Getter for the attribute "id"
 * @param {Integer} Id value
 */
Cluster.Mark.prototype.setId = function(id){
	this.id = id;
};

/**
 * Getter for the attribute "duration"
 * @return {Integer} Duration value
 */
Cluster.Mark.prototype.getDuration = function(){
	return this.duration;
};
/**
 * Getter for the attribute "duration"
 * @param {Integer} Duration value
 */
Cluster.Mark.prototype.setDuration = function(duration){
	this.duration = duration;
};

/**
 * Getter for the attribute "startNode"
 * @return {Cluster.Node} StartNode value
 */
Cluster.Mark.prototype.getStartNode = function(){
	return this.startNode
};
/**
 * Getter for the attribute "startNode"
 * @param {Cluster.Node} StartNode value
 */
Cluster.Mark.prototype.setStartNode = function(startNode){
	this.startNode = startNode;
};

/**
 * Getter for the attribute "process"
 * @return {Cluster.Process} Process value
 */
Cluster.Mark.prototype.getProcess = function(){
	return this.process;
};
/**
 * Setter for the attribute "process"
 * @param {Cluster.Process} Process value
 */
Cluster.Mark.prototype.setProcess = function(process){
	this.process = process;
};

/**
 * Getter for the attribute "objectGL"
 * @return {GLGE.Object} ObjectGL value
 */
Cluster.Mark.prototype.getObjectGL = function(){
	return this.objectGL;
};
/**
 * Setter for the attribute "objectGL"
 * @param {GLGE.Object} ObjectGL value
 */
Cluster.Mark.prototype.setObjectGL = function(objectGL){
	this.objectGL = objectGL;
};

/**
 * Getter for the attribute "startTime"
 * @return {Time} StartTime value
 */
Cluster.Mark.prototype.getStartTime = function(){
	return this.startTime;
};
/**
 * Setter for the attribute "startTime"
 * @param {Time} StartTime value
 */
Cluster.Mark.prototype.setStartTime = function(startTime){
	this.startTime = startTime;
};

/**
 * Getter for the attribute "scale"
 * @return {Float} Scale value
 */
Cluster.Mark.prototype.getScale = function(){
	return this.scale;
};
/**
 * Getter for the attribute "scale"
 * @param {Float} Scale value
 */
Cluster.Mark.prototype.setScale = function(scale){
	this.scale = scale;
};

/**
 * Getter for the attribute "colorBegin"
 * @return {String} ColorBegin value
 */
Cluster.Mark.prototype.getColorBegin = function(){
	return this.colorBegin;
};
/**
 * Getter for the attribute "colorBegin"
 * @param {String} ColorBegin value
 */
Cluster.Mark.prototype.setColorBegin = function(colorBegin){
	this.colorBegin = colorBegin;
};

/**
 * Getter for the attribute "colorEnd"
 * @return {String} ColorEnd value
 */
Cluster.Mark.prototype.getColorEnd = function(){
	return this.colorEnd;
};
/**
 * Getter for the attribute "colorEnd"
 * @param {String} ColorEnd value
 */
Cluster.Mark.prototype.setColorEnd = function(colorEnd){
	this.colorEnd = colorEnd;
};

/**
 * Getter for the attribute "material"
 * @return {GLGE.Material} Material value
 */
Cluster.Mark.prototype.getMaterial = function(){
	return this.material;
};
/**
 * Getter for the attribute "material"
 * @param {GLGE.Material} Material value
 */
Cluster.Mark.prototype.setMaterial = function(material){
	this.material = material;
};

/**
 * Getter for the attribute "speedRed"
 * @return {Float} SpeedRed value
 */
Cluster.Mark.prototype.getSpeedRed = function(){
	return this.speedRed;
};
/**
 * Getter for the attribute "speedRed"
 * @param {Float} SpeedRed value
 */
Cluster.Mark.prototype.setSpeedRed = function(speedRed){
	this.speedRed = speedRed;
};

/**
 * Getter for the attribute "speedGreen"
 * @return {Float} SpeedGreen value
 */
Cluster.Mark.prototype.getSpeedGreen = function(){
	return this.speedGreen;
};
/**
 * Getter for the attribute "speedGreen"
 * @param {Float} SpeedGreen value
 */
Cluster.Mark.prototype.setSpeedGreen = function(speedGreen){
	this.speedGreen = speedGreen;
};

/**
 * Getter for the attribute "speedBlue"
 * @return {Float} SpeedBlue value
 */
Cluster.Mark.prototype.getSpeedBlue = function(){
	return this.speedBlue;
};
/**
 * Getter for the attribute "speedBlue"
 * @param {Float} SpeedBlue value
 */
Cluster.Mark.prototype.setSpeedBlue = function(speedBlue){
	this.speedBlue = speedBlue;
};

/**
CLUSTER Cluster vizualisation for kerrighed
Copyright (c) 2010, Université François Rabelais - Tours - France
*/

var _cacheSpherePosition = new Array()

/**
* @namespace Holds the functionality of the library
*/
Cluster.SpherePosition = function(){};

/**
 * Calculate the position of N points on a sphere to have a uniform distribution
 * @param {Integer} N Number of points
 * @param {Float} length Ray of the sphere
 * @return {Array} List of position for the N points on the sphere
 */
Cluster.SpherePosition.getSphereDistribution = function(N, radius) {
	var list;
	
	if(Cluster.SpherePosition.checkCache(N) == true)
	{
		list = _cacheSpherePosition[N];		
	}
	else
	{
		list = Cluster.SpherePosition.generateSphereDistribution(N);
		_cacheSpherePosition[N] = list;	
	}
	
	list = Cluster.SpherePosition._scaleList(list, N, radius);
	
	return list;
}


/**
 * Check if there positions were already generate
 * @param {Integer} N Number of points
 * @return {Booelean} True if the list of positions is in cache
 */
Cluster.SpherePosition.checkCache = function(N) {
	if(_cacheSpherePosition[N] == undefined)
		return false;
	return true;
}


/**
 * Calculate the position of N points on a sphere to have a uniform distribution
 * @param {Integer} N Number of points
 * @param {Float} length Ray of the sphere
 * @return {Array} List of position for the N points on the sphere
 */
Cluster.SpherePosition.generateSphereDistribution = function(N) {
					
	if(N > 0)
	{
		var i, k;
		var step = 0.01;
		var minimal_step = Math.pow(10, -10);
		var p0 = new Array();
		var p1 = new Array();
		var f;

		var Nstep = 150;
		
		if(N != 1)
		{
			var list;
			if(Cluster.SpherePosition.checkCache(N-1) == true)
			{
				list = _cacheSpherePosition[N-1];		
			}
			else
			{
				list = Cluster.SpherePosition.generateSphereDistribution(N-1);
				_cacheSpherePosition[N-1] = list;	
			}
			
			for(k = 0 ; k < N-1 ; k++) {
				p0[k] = list[k];
			}				
		}
		
		p0[N-1] = GLGE.Vec3(2*Cluster.SpherePosition.frand(), 2*Cluster.SpherePosition.frand(), 2*Cluster.SpherePosition.frand());

		for(i = 0 ; i < N ; i++) {
			p1[i] = GLGE.Vec3(0.0, 0.0, 0.0);	
			var l = GLGE.lengthVec3(p0[i]);
			if(l != 0.0) {
				p0[i] = GLGE.scaleVec3(p0[i], 1.0/l);
			}
			else
				i--;
		}
			
		
		var e0 = Cluster.SpherePosition._getCoulombEnergy(N, p0);
		for(k = 0 ; k < Nstep ; k++) {
			
			f = Cluster.SpherePosition._getForces(N, p0);
			
			for(i = 0 ; i < N ; i++) {
				var d = GLGE.dotVec3(f[i], p0[i]);
				
				f[i][0]  -= p0[i][0] * d;
				f[i][1]  -= p0[i][1] * d;
				f[i][2]  -= p0[i][2] * d;
				
				p1[i][0] = p0[i][0] + f[i][0] * step;
				p1[i][1] = p0[i][1] + f[i][1] * step;
				p1[i][2] = p0[i][2] + f[i][2] * step;
					
				var l = GLGE.lengthVec3(p1[i]);
				p1[i] = GLGE.scaleVec3(p1[i], 1.0/l);
			}
			
			var e = Cluster.SpherePosition._getCoulombEnergy(N, p1);
			if(e >= e0) {  // not successfull step
				step /= 2;
				if(step < minimal_step)
					break;
				continue;
			}
			else {// successfull step
				var t = p0; 
				p0 = p1;
				p1 = t; 
				     
				e0 = e;
				step *= 2;
			}   
		}

		return p0;
	}
	
	return null;
};



/**
 * Scale the list of positions
 * @return {Array} list List of positions
 * @param {Integer} N Number of points
 * @param {Float} radius Scale size
 * @return {Array} List of positions scaled
 */
Cluster.SpherePosition._scaleList = function(list, N, radius)
{
	var i;
	var result = new Array();
		
	for(i = 0 ; i < N ; i++) {
		result[i] = GLGE.scaleVec3(list[i], radius);
	}
	
	return result;
};

/**
 * Calculate the coulomb energy of the N points
 * @param {Integer} N Number of points
 * @param {Array} p List of the positions
 * @return {Integer} Coulomb energy
 */
Cluster.SpherePosition._getCoulombEnergy = function(N, p)
{
	var i, j;
	var e = 0.0;
	
	for(i = 0 ; i < N ; i++) {  
		for(j = i+1 ; j < N ; j++) {
			e += 1.0 / GLGE.distanceVec3(p[i], p[j]);
		}
	}
	
	return e;
};

/**
 * Generate an appropriate random number
 * @return {Integer} Coulomb energy
 */
Cluster.SpherePosition.frand = function()
{
	return Math.random() - .5;
};

/**
 * Calculate the forces between the N points
 * @param {Integer} N Number of points
 * @param {Array} p List of the positions
 * @return {Integer} Forces
 */
Cluster.SpherePosition._getForces = function(N, p)
{
	var i, j;
	var f = new Array();
	
	for(i = 0 ; i < N ; i++) {  
		f[i] = GLGE.Vec3(0.0, 0.0, 0.0);
	}
	
	for(i = 0 ; i < N ; i++) {  
		for(j = i+1 ; j < N ; j++) {
			
			var r = GLGE.subVec3(p[i], p[j]);
			
			var l = GLGE.lengthVec3(r);
			l = 1.0/(l*l*l);
			
			var ff;			
			ff = l*r[0];
			f[i][0] += ff;
			f[j][0] -= ff;
			
			ff = l*r[1];
			f[i][1] += ff;
			f[j][1] -= ff;
			
			ff = l*r[2];
			f[i][2] += ff;
			f[j][2] -= ff;
		}
	}
	
	return f;
};


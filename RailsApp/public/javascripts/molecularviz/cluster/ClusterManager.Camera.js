/**
CLUSTER Cluster vizualisation for kerrighed
Copyright (c) 2010, Université François Rabelais - Tours - France
*/

/**
* @class ClusterManager.Camera Class managing the camera scene
* @param {String} name Name of the manager
* @param {String} canvas Name of the canvas element
* @constructor
*/
ClusterManager.Camera = function(name, canvas)
{
	/**
	 * Name of the camera
	 */
	this.name = null;
	/**
	 * The element containing the scene 
	 */
	this.canvas = null;
	/**
	 * Object GL for the camera
	 */
	this.cameraGL = null;
	/**
	 * Current target position
	 */
	this.currentTargetPosition = [0.0, 0.0, 0.0];
	/**
	 * Target position
	 */
	this.targetPosition = [0.0, 0.0, 0.0];
	/**
	 * Object Targeted
	 */
	this.targetObject = null;
	/**
	 * Type of the Object Targeted
	 */
	this.targetObjectTypeId = 0;
	/**
	 * Distance between the camera position and the camera target
	 */
	this.dist = 100;
	/**
	 * Angle of rotation on the axis X
	 */
	this.angleX = Math.PI / 2.0;
	/**
	 * Angle of rotation on the axis Y
	 */
	this.angleY = Math.PI / 2.0;
	
	this.initialize(name, canvas);
}



/**
 * Init the camera
* @param {String} name Name of the manager
* @param {String} canvas Name of the canvas element
 */
ClusterManager.Camera.prototype.initialize = function(name, canvas)
{
	this.setName(name);
	this.setCanvas(canvas);
	
	this.setCameraGL(new GLGE.Camera(this.getName()));
	this.getCameraGL().setRotOrder(GLGE.ROT_XZY);
	
	this.setAspect();
	
	this.reset();
}

/**
 * Destroy the camera
 */
ClusterManager.Camera.prototype.destroy = function()
{
	delete this.getCameraGL();
}

/**
 * Set the aspect of the camera
 */
ClusterManager.Camera.prototype.setAspect = function()
{
	var height = document.getElementById(this.getCanvas()).offsetHeight;
	var width = document.getElementById(this.getCanvas()).offsetWidth;
	
	if(height != 0 && width != 0)
	{
		this.getCameraGL().setAspect(width*1.0/height);
	}
}

/**
 * Reset the camera
 */
ClusterManager.Camera.prototype.reset = function()
{
	this.setDist(100);
	this.setAngleX(Math.PI / 2.0);
	this.setAngleY(Math.PI / 2.0);
	this.setTargetObject(null);
	this.setTargetObjectTypeId(Cluster.TYPE_NULL);
	this.setTargetPosition(GLGE.Vec3(0.0, 0.0, 0.0));
	this.setCurrentTargetPosition(GLGE.Vec3(0.0, 0.0, 0.0));
	
	this.setPosition(0.0, this.getDist(), 0.0);
	this.getCameraGL().Lookat(this.getTargetPosition());
}

/**
 * Returns the camera GL
 * @return {GLGE.Camera} The camera GL
 */
ClusterManager.Camera.prototype.getCameraGL = function()
{
	return this.getCameraGL();
}

/**
 * Returns the camera position
 * @return {GLGE.Vec3} The camera position
 */
ClusterManager.Camera.prototype.getPosition = function()
{
	return GLGE.Vec3(this.getCameraGL().getLocX(), this.getCameraGL().getLocY(), this.getCameraGL().getLocZ());
}

/**
 * Define the camera position
 * @param {double} x The camera position on axis X
 * @param {double} y The camera position on axis Y
 * @param {double} z The camera position on axis Z
 */
ClusterManager.Camera.prototype.setPosition = function(x, y, z)
{
	this.getCameraGL().setLoc(x, y, z);
}

/**
 * Get target position
 * @return {Position} quickly Define if it's necessary or not to move/update the camera target. Set true to skip this step
 */
ClusterManager.Camera.prototype.getGlobalTargetPosition = function()
{
	var result = Cluster.getObjectFromIdObject(this.getTargetObjectTypeId(), this.getTargetObject());
	if(result != null)
	{
		return GLGE.Vec3(result.getObjectGL().getLocX(), result.getObjectGL().getLocY(), result.getObjectGL().getLocZ());
	}
	
	return this.getTargetPosition();
}

/**
 * Define the camera position
 * @param {double} x The camera position on axis X
 * @param {double} y The camera position on axis Y
 * @param {double} z The camera position on axis Z
 */
ClusterManager.Camera.prototype.setGlobalTargetPosition = function(x, y, z)
{
	this.setTargetPosition(GLGE.Vec3(x, y, z));
}

/**
 * Define the camera target
 * @param {Integer} idType The type of object
 * @param {Integer} idObject The id of the object
 */
ClusterManager.Camera.prototype.defineTargetObject = function(idType, idObject)
{
	this.setTargetObjectTypeId(idType);
	this.setTargetObject(idObject);
}

/**
 * Rotate the camera with a mouse mouvement
 * @param {Object} mouseMove Object containing the mouvement of the mouse (x/y)
 */
ClusterManager.Camera.prototype.dragMouse = function(mouseMove)
{
	var rotX = -mouseMove.x * 0.005;
	var rotY = -mouseMove.y * 0.005;
	
	if(Math.abs(mouseMove.x) < 4)
		rotX = 0;
	if(Math.abs(mouseMove.y) < 4)
		rotY = 0;
		
	this.rotate(rotX, rotY);
}

/**
 * Rotate the camera
 * @param {double} rotX Rotation on axis X
 * @param {double} rotY Rotation on axis Y
 */
ClusterManager.Camera.prototype.rotate = function(rotX, rotY)
{
	if(rotX != 0 || rotY != 0)
	{
		this.setAngleX((this.getAngleX() + rotX)%(Math.PI * 2));
		this.setAngleY(this.getAngleY() + rotY);
							
		if(this.getAngleY() > (Math.PI - 0.15))
			this.setAngleY(Math.PI - 0.15);
		else if(this.getAngleY() < 0.15)
			this.setAngleY(0.15);
		
		this.update(true);
	}
}

/**
 * Zoom in or out the camera
 * @param {double} rotX Value of the zoom
 */
ClusterManager.Camera.prototype.zoom = function(zoomDist)
{
	this.setDist(this.getDist() + zoomDist);
	if(this.getDist() < 10)
		this.setDist(10);
		
	this.update(true);
}

/**
 * Update the camera
 * @return The position of the camera target
 */
ClusterManager.Camera.prototype.update = function(quickly)
{
	var speed = 1.0;
	var seuil = 0.5;
			
	var cameraTargetPosition = this.getGlobalTargetPosition();
	var cameraTargetCurrentPosition = this.getCurrentTargetPosition();
	
	var locX = cameraTargetPosition[0] + Math.sin(this.getAngleY()) * Math.cos(this.getAngleX()) * this.getDist();
	var locY = cameraTargetPosition[1] + Math.sin(this.getAngleY()) * Math.sin(this.getAngleX()) * this.getDist();
	var locZ = cameraTargetPosition[2] + Math.cos(this.getAngleY()) * this.getDist();
	
	var cameraPosition = GLGE.Vec3(locX, locY, locZ);
	var cameraCurrentPosition = this.getPosition();
	
	if(quickly == undefined || quickly == false)
	{
		var vectorCamera = GLGE.subVec3(cameraPosition, cameraCurrentPosition);
		var vectorCameraNormalize = GLGE.toUnitVec3(vectorCamera);		
		var distCamera = GLGE.lengthVec3(vectorCamera);
		
		
		if(distCamera > seuil)
		{
			cameraCurrentPosition[0] += vectorCameraNormalize[0] * speed;
			cameraCurrentPosition[1] += vectorCameraNormalize[1] * speed;
			cameraCurrentPosition[2] += vectorCameraNormalize[2] * speed;
		}
		
		var vectorTargetCamera = GLGE.subVec3(cameraTargetPosition, cameraTargetCurrentPosition);
		var vectorTargetCameraNormalize = GLGE.toUnitVec3(vectorTargetCamera);
		var distTargetCamera = GLGE.lengthVec3(vectorTargetCamera);
		
		
		if(distTargetCamera > seuil)
		{
			cameraTargetCurrentPosition[0] += vectorTargetCameraNormalize[0] * speed;
			cameraTargetCurrentPosition[1] += vectorTargetCameraNormalize[1] * speed;
			cameraTargetCurrentPosition[2] += vectorTargetCameraNormalize[2] * speed;
		}
	}
	else
	{
		cameraTargetCurrentPosition = cameraTargetPosition;
		cameraCurrentPosition = cameraPosition;
	}
	
	this.setPosition(cameraCurrentPosition[0], cameraCurrentPosition[1], cameraCurrentPosition[2]);
	this.getCameraGL().Lookat(cameraTargetCurrentPosition);
	
	this.setAspect();
}




// Getter - Setter



/**
 * Getter for the attribute "name"
 * @return {String} Name value
 */
ClusterManager.Camera.prototype.getName = function(){
	return this.name;
};
/**
 * Setter for the attribute "name"
 * @param {String} Name value
 */
ClusterManager.Camera.prototype.setName = function(name){
	this.name = name;
};

/**
 * Getter for the attribute "canvas"
 * @return {String} Canvas value
 */
ClusterManager.Camera.prototype.getCanvas = function(){
	return this.canvas;
};
/**
 * Setter for the attribute "canvas"
 * @param {String} Canvas value
 */
ClusterManager.Camera.prototype.setCanvas = function(canvas){
	this.canvas = canvas;
};

/**
 * Getter for the attribute "cameraGL"
 * @return {GLGE.Camera} CameraGL value
 */
ClusterManager.Camera.prototype.getCameraGL = function(){
	return this.cameraGL;
};
/**
 * Setter for the attribute "cameraGL"
 * @param {GLGE.Camera} CameraGL value
 */
ClusterManager.Camera.prototype.setCameraGL = function(cameraGL){
	this.cameraGL = cameraGL;
};

/**
 * Getter for the attribute "targetPosition"
 * @return {Array} TargetPosition value
 */
ClusterManager.Camera.prototype.getTargetPosition = function(){
	return this.targetPosition;
};
/**
 * Setter for the attribute "targetPosition"
 * @param {Array} TargetPosition value
 */
ClusterManager.Camera.prototype.setTargetPosition = function(targetPosition){
	this.targetPosition = targetPosition;
};

/**
 * Getter for the attribute "currentTargetPosition"
 * @return {Array} CurrentTargetPosition value
 */
ClusterManager.Camera.prototype.getCurrentTargetPosition = function(){
	return this.currentTargetPosition;
};
/**
 * Setter for the attribute "currentTargetPosition"
 * @param {Array} CurrentTargetPosition value
 */
ClusterManager.Camera.prototype.setCurrentTargetPosition = function(currentTargetPosition){
	this.currentTargetPosition = currentTargetPosition;
};

/**
 * Getter for the attribute "targetObject"
 * @return {Object} TargetObject value
 */
ClusterManager.Camera.prototype.getTargetObject = function(){
	return this.targetObject;
};
/**
 * Setter for the attribute "targetObject"
 * @param {Object} TargetObject value
 */
ClusterManager.Camera.prototype.setTargetObject = function(targetObject){
	this.targetObject = targetObject;
};

/**
 * Getter for the attribute "targetObjectTypeId"
 * @return {Integer} TargetObjectTypeId value
 */
ClusterManager.Camera.prototype.getTargetObjectTypeId = function(){
	return this.targetObjectTypeId;
};
/**
 * Setter for the attribute "targetObjectTypeId"
 * @param {Integer} TargetObjectTypeId value
 */
ClusterManager.Camera.prototype.setTargetObjectTypeId = function(targetObjectTypeId){
	this.targetObjectTypeId = targetObjectTypeId;
};

/**
 * Getter for the attribute "dist"
 * @return {Float} Dist value
 */
ClusterManager.Camera.prototype.getDist = function(){
	return this.dist;
};
/**
 * Setter for the attribute "dist"
 * @param {Float} Dist value
 */
ClusterManager.Camera.prototype.setDist = function(dist){
	this.dist = dist;
};

/**
 * Getter for the attribute "angleX"
 * @return {Float} AngleX value
 */
ClusterManager.Camera.prototype.getAngleX = function(){
	return this.angleX;
};
/**
 * Setter for the attribute "angleX"
 * @param {Float} AngleX value
 */
ClusterManager.Camera.prototype.setAngleX = function(angleX){
	this.angleX = angleX;
};

/**
 * Getter for the attribute "angleY"
 * @return {Float} AngleY value
 */
ClusterManager.Camera.prototype.getAngleY = function(){
	return this.angleY;
};
/**
 * Setter for the attribute "angleY"
 * @param {Float} AngleY value
 */
ClusterManager.Camera.prototype.setAngleY = function(angleY){
	this.angleY = angleY;
};

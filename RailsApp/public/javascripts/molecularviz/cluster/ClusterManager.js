/**
CLUSTER Cluster vizualisation for kerrighed
Copyright (c) 2010, Université François Rabelais - Tours - France
*/

/**
 * Instance of the singleton
 */
var instanceClusterManager = null;


/**
* @class ClusterManager Class managing the GL scene
* @param {String} name Name of the manager
* @param {String} canvas Name of the canvas element
* @param {GLGE.Document} documentScene Contains the default scene
* @param {String} debugMode Name of the debug element
* @constructor
*/
ClusterManager = function(name, canvas, documentScene, debugMode)
{
	/**
	 * Name of the manager
	 */
	this.name = null;
	/**
	 * The element containing the scene 
	 */
	this.canvas = null;
	/**
	 * The element containing the default scene 
	 */
	this.documentScene = null;
	/**
	 * The object for the renderer 
	 */
	this.objectRenderer = null;
	/**
	 * The object for the scene 
	 */
	this.objectScene = null;
	/**
	 * The object for the mouse input 
	 */
	this.objectMouse = null;
	/**
	 * The object for the keyboard input 
	 */
	this.objectKeyboard = null;
	/**
	 * Interval called to destroy the process
	 */
	this.camera = null;
	/**
	 * The start time
	 */
	this.frameTimeStart = null;
	/**
	 * the current time
	 */
	this.frameTimeNow = null;
	/**
	 * The last time
	 */
	this.frameTimeLast = null;
	/**
	 * The frame rate buffer
	 */
	this.frameRateBuffer = 60;
	/**
	 * Define if the bug mode is on
	 */
	this.debugMode = null;
	/**
	 * Id of the last migration
	 */
	this.lastMigId = -1;
	/**
	 * Interval called to render the frame
	 */
	this.interval = null;
	/**
	 * Define if the mouse is on the canvas
	 */
	this.mouseActive = false;
	/**
	 * Define if the mouse button is pressed
	 */
	this.mouseButtonPressed = false;
	/**
	 * The mouse position
	 */
	this.mousePosition = null;
	/**
	 * The last object gotten with the ray picking
	 */
	this.rayPickingLastObject = null;
}

/**
* Create singleton for ClusterManager Class
* @param {String} name Name of the manager
* @param {String} canvas Name of the canvas element
* @param {GLGE.Document} documentScene Contains the default scene
* @param {String} debugMode Name of the debug element
*/
ClusterManager.createSingleton = function(name, canvas, documentScene, debugMode)
{
	if(instanceClusterManager == null)
	{
		instanceClusterManager = new ClusterManager(name, canvas, documentScene, debugMode);
		instanceClusterManager.initialize(name, canvas, documentScene, debugMode);
	}
}

/**
* Return the instance of the class
* @return {ClusterManager} The singleton
*/
ClusterManager.getSingleton = function()
{
	return instanceClusterManager;
}

/**
* Destroy the singleton
*/
ClusterManager.destroy = function()
{
	if(instanceClusterManager != null)
	{
		instanceClusterManager.destroy();
		delete instanceClusterManager;
		instanceClusterManager = null;
	}
}


/**
 * Function to init the WebGL
 * @param {String} name Name of the manager
 * @param {String} canvas Name of the canvas element
* @param {GLGE.Document} documentScene Contains the default scene
 * @param {String} debugMode Name of the debug element
 */
ClusterManager.prototype.initialize = function(name, canvas, documentScene, debugMode)
{
	this.setName(name);
	this.setCanvas(canvas);
	this.setDocumentScene(documentScene);
	this.setDebugMode(debugMode);
	
    this.setObjectRenderer(new GLGE.Renderer(document.getElementById(this.getCanvas())));
    
    this.setObjectScene(this.getDocumentScene().getElement("mainscene"));
    
    this.getObjectRenderer().setScene(this.getObjectScene());

	this.setCamera(new ClusterManager.Camera("mainCamera", this.getCanvas()));
	this.getObjectScene().setCamera(this.getCamera().getCameraGL());	
	
    this.setObjectMouse(new GLGE.MouseInput(document.getElementById(this.getCanvas())));
    this.setObjectKeyboard(new GLGE.KeyInput());
    
    document.getElementById(this.getCanvas()).onmouseover = function(e){ ClusterManager.getSingleton().setMouseActive(true); }
    document.getElementById(this.getCanvas()).onmousemove = function(e){ ClusterManager.getSingleton().setMouseActive(true); }
    document.getElementById(this.getCanvas()).onmouseout = function(e){ ClusterManager.getSingleton().setMouseActive(false); }
	
    Cluster.initialize(this.getDocumentScene());
    
    this.setFrameTimeStart(parseInt(new Date().getTime()));
    this.setFrameTimeLast(0);
    this.setFrameTimeNow(0);
    
    this.setInterval(setInterval("ClusterManager.getSingleton().render();", 1));
}

/**
 * Function to destroy the scene
 */
ClusterManager.prototype.destroy = function()
{
    clearInterval(this.getInterval());
    
    Cluster.destroy();
    
    this.getCamera().destroy();
    delete this.getCamera();
    
    delete this.getObjectMouse();
    delete this.getObjectKeyboard();
    delete this.getObjectScene();
    delete this.getObjectRenderer();
    
    delete this;
}

/**
 * Function to do a migration
 * @param {Integer} mid Id of the migration
 * @param {Integer} process Id of the process
 * @param {Integer} from Id of the start node
 * @param {Integer} to Id of the end node
 */
ClusterManager.prototype.migrate = function(mid, process, from, to)
{
	if(mid > this.getLastMigId())
	{
		this.setLastMigId(mid);
		Cluster.displayMigrations(process, from, to);
	}
}




/**
 * Render a frame
 */
ClusterManager.prototype.render = function()
{
    this.setFrameTimeNow(parseInt(new Date().getTime()));
	
	if (this.getDebugMode() != null)
	{
		this.setFrameRateBuffer(Math.round(((this.frameRateBuffer*9)+1000/(this.getFrameTimeNow()-this.getFrameTimeLast()))/10));
		document.getElementById(this.getDebugMode()).innerHTML="Frame Rate:"+this.getFrameRateBuffer()+" - Nb node : "+Cluster.nodeNb+" Nb process : "+Cluster.processNb;
	}
	
	this.lookMouse();
	this.lookKeyboard();
	
	Cluster.render(this.getFrameTimeStart(), this.getFrameTimeNow());
	
	this.getCamera().update();
	
	this.getObjectRenderer().render();

	this.setFrameTimeLast(this.getFrameTimeNow());
}

/**
 * Get the mouse evenement
 */
ClusterManager.prototype.lookMouse = function()
{
	// Si la souris survole la scene
	if(this.getMouseActive())
	{
		// On récupére la position de la position en x et en y
		var mousePos = this.getObjectMouse().getMousePosition();
		mousePos.x = mousePos.x - document.getElementById(this.getCanvas()).offsetLeft + window.pageXOffset;
		mousePos.y = mousePos.y - document.getElementById(this.getCanvas()).offsetTop + window.pageYOffset;

		if(mousePos.x && mousePos.y)
		{
			var obj = this.getObjectScene().pick(mousePos.x, mousePos.y);
			if(obj != null)
			{
				var info = Cluster.getObjectFromIdGLObject(obj.object.getId());
				
				if(info != null)
				{
					Cluster.getDescriptionFromId(info.object);
					
					if(this.getObjectMouse().isButtonDown(0) && this.getMouseButtonPressed() == false && obj != this.getRayPickingLastObject())
					{
						this.getCamera().defineTargetObject(info.idType, info.object.getId());
						
						if(info.idType == Cluster.TYPE_NODE)
						{
							Cluster.PrintInfoTrack("Node", "Node Id : "+info.object.getId());
						}
						else if(info.idType == Cluster.TYPE_PROCESS)
						{
							Cluster.PrintInfoTrack("Process", "Process Id : "+info.object.getId());
						}
									
						this.setRayPickingLastObject(obj);
					}
				}
            }
			else
			{
				Cluster.PrintInfoPick("", "");
			}
            
            if(this.getObjectMouse().isButtonDown(0))
            {
				if(this.getMouseButtonPressed() == false)
				{
					this.setMouseButtonPressed(true);
				}
				else if(this.getMousePosition().x != mousePos.x && this.getMousePosition().y != mousePos.y)
				{				
					var mouseMove = {x:mousePos.x-this.getMousePosition().x, y:mousePos.y-this.getMousePosition().y};
					
					this.getCamera().dragMouse(mouseMove);
				}
				
				this.setMousePosition(mousePos);
            }
            else
				this.setMouseButtonPressed(false);     
        }
		else
			this.setMouseButtonPressed(false);
	}
}

/**
 * Get the keyboard evenement
 */
ClusterManager.prototype.lookKeyboard = function()
{
	if(this.getObjectKeyboard().isKeyPressed(GLGE.KI_C))
	{
		this.getCamera().reset();
	}
	else if(this.getObjectKeyboard().isKeyPressed(GLGE.KI_UP_ARROW) || this.getObjectKeyboard().isKeyPressed(GLGE.KI_P))
	{
		this.getCamera().zoom(-10);

		if(this.getObjectKeyboard().isKeyPressed(GLGE.KI_UP_ARROW))
			window.scrollBy(0, 1);
	}
	else if(this.getObjectKeyboard().isKeyPressed(GLGE.KI_DOWN_ARROW) || this.getObjectKeyboard().isKeyPressed(GLGE.KI_M))
	{
		this.getCamera().zoom(10);

		if(this.getObjectKeyboard().isKeyPressed(GLGE.KI_DOWN_ARROW))
			window.scrollBy(0, -1);
	}
}



// Getter - Setter



/**
 * Getter for the attribute "name"
 * @return {String} Name value
 */
ClusterManager.prototype.getName = function(){
	return this.name;
};
/**
 * Setter for the attribute "name"
 * @param {String} Name value
 */
ClusterManager.prototype.setName = function(name){
	this.name = name;
};

/**
 * Getter for the attribute "canvas"
 * @return {String} Canvas value
 */
ClusterManager.prototype.getCanvas = function(){
	return this.canvas;
};
/**
 * Setter for the attribute "canvas"
 * @param {String} Canvas value
 */
ClusterManager.prototype.setCanvas = function(canvas){
	this.canvas = canvas;
};

/**
 * Getter for the attribute "objectRenderer"
 * @return {GLGE.ObjectRenderer} ObjectRenderer value
 */
ClusterManager.prototype.getObjectRenderer = function(){
	return this.objectRenderer;
};
/**
 * Setter for the attribute "objectRenderer"
 * @param {GLGE.ObjectRenderer} ObjectRenderer value
 */
ClusterManager.prototype.setObjectRenderer = function(objectRenderer){
	this.objectRenderer = objectRenderer;
};

/**
 * Getter for the attribute "documentScene"
 * @return {GLGE.Document} DocumentScene value
 */
ClusterManager.prototype.getDocumentScene = function(){
	return this.documentScene;
};
/**
 * Setter for the attribute "documentScene"
 * @param {GLGE.Document} DocumentScene value
 */
ClusterManager.prototype.setDocumentScene = function(documentScene){
	this.documentScene = documentScene;
};

/**
 * Getter for the attribute "objectScene"
 * @return {GLGE.Document} ObjectScene value
 */
ClusterManager.prototype.getObjectScene = function(){
	return this.objectScene;
};
/**
 * Setter for the attribute "objectScene"
 * @param {GLGE.Document} ObjectScene value
 */
ClusterManager.prototype.setObjectScene = function(objectScene){
	this.objectScene = objectScene;
};

/**
 * Getter for the attribute "objectMouse"
 * @return {GLGE.MouseInput} ObjectMouse value
 */
ClusterManager.prototype.getObjectMouse = function(){
	return this.objectMouse;
};
/**
 * Setter for the attribute "objectMouse"
 * @param {GLGE.MouseInput} ObjectMouse value
 */
ClusterManager.prototype.setObjectMouse = function(objectMouse){
	this.objectMouse = objectMouse;
};

/**
 * Getter for the attribute "objectKeyboard"
 * @return {GLGE.KeyInput} ObjectKeyboard value
 */
ClusterManager.prototype.getObjectKeyboard = function(){
	return this.objectKeyboard;
};
/**
 * Setter for the attribute "objectKeyboard"
 * @param {GLGE.KeyInput} ObjectKeyboard value
 */
ClusterManager.prototype.setObjectKeyboard = function(objectKeyboard){
	this.objectKeyboard = objectKeyboard;
};

/**
 * Getter for the attribute "camera"
 * @return {ClusterManager.Camera} Camera value
 */
ClusterManager.prototype.getCamera = function(){
	return this.camera;
};
/**
 * Setter for the attribute "camera"
 * @param {ClusterManager.Camera} Camera value
 */
ClusterManager.prototype.setCamera = function(camera){
	this.camera = camera;
};

/**
 * Getter for the attribute "frameTimeStart"
 * @return {Time} FrameTimeStart value
 */
ClusterManager.prototype.getFrameTimeStart = function(){
	return this.frameTimeStart;
};
/**
 * Setter for the attribute "frameTimeStart"
 * @param {Time} FrameTimeStart value
 */
ClusterManager.prototype.setFrameTimeStart = function(frameTimeStart){
	this.frameTimeStart = frameTimeStart;
};

/**
 * Getter for the attribute "frameTimeNow"
 * @return {Time} FrameTimeNow value
 */
ClusterManager.prototype.getFrameTimeNow = function(){
	return this.frameTimeNow;
};
/**
 * Setter for the attribute "frameTimeNow"
 * @param {Time} FrameTimeNow value
 */
ClusterManager.prototype.setFrameTimeNow = function(frameTimeNow){
	this.frameTimeNow = frameTimeNow;
};

/**
 * Getter for the attribute "frameTimeLast"
 * @return {Time} FrameTimeLast value
 */
ClusterManager.prototype.getFrameTimeLast = function(){
	return this.frameTimeLast;
};
/**
 * Setter for the attribute "frameTimeLast"
 * @param {Time} FrameTimeLast value
 */
ClusterManager.prototype.setFrameTimeLast = function(frameTimeLast){
	this.frameTimeLast = frameTimeLast;
};

/**
 * Getter for the attribute "frameRateBuffer"
 * @return {Integer} FrameRateBuffer value
 */
ClusterManager.prototype.getFrameRateBuffer = function(){
	return this.frameRateBuffer;
};
/**
 * Setter for the attribute "frameRateBuffer"
 * @param {Integer} FrameRateBuffer value
 */
ClusterManager.prototype.setFrameRateBuffer = function(frameRateBuffer){
	this.frameRateBuffer = frameRateBuffer;
};

/**
 * Getter for the attribute "debugMode"
 * @return {String} DebugMode value
 */
ClusterManager.prototype.getDebugMode = function(){
	return this.debugMode;
};
/**
 * Setter for the attribute "debugMode"
 * @param {String} DebugMode value
 */
ClusterManager.prototype.setDebugMode = function(debugMode){
	this.debugMode = debugMode;
};

/**
 * Getter for the attribute "lastMigId"
 * @return {Integer} LastMigId value
 */
ClusterManager.prototype.getLastMigId = function(){
	return this.lastMigId;
};
/**
 * Setter for the attribute "lastMigId"
 * @param {Integer} LastMigId value
 */
ClusterManager.prototype.setLastMigId = function(lastMigId){
	this.lastMigId = lastMigId;
};

/**
 * Getter for the attribute "interval"
 * @return {Integer} Interval value
 */
ClusterManager.prototype.getInterval = function(){
	return this.interval;
};
/**
 * Setter for the attribute "interval"
 * @param {Integer} Interval value
 */
ClusterManager.prototype.setInterval = function(interval){
	this.interval = interval;
};

/**
 * Getter for the attribute "mouseActive"
 * @return {Boolean} MouseActive value
 */
ClusterManager.prototype.getMouseActive = function(){
	return this.mouseActive;
};
/**
 * Setter for the attribute "mouseActive"
 * @param {Boolean} MouseActive value
 */
ClusterManager.prototype.setMouseActive = function(mouseActive){
	this.mouseActive = mouseActive;
};

/**
 * Getter for the attribute "mouseButtonPressed"
 * @return {Boolean} MouseButtonPressed value
 */
ClusterManager.prototype.getMouseButtonPressed = function(){
	return this.mouseButtonPressed;
};
/**
 * Setter for the attribute "mouseButtonPressed"
 * @param {Boolean} MouseButtonPressed value
 */
ClusterManager.prototype.setMouseButtonPressed = function(mouseButtonPressed){
	this.mouseButtonPressed = mouseButtonPressed;
};

/**
 * Getter for the attribute "mousePosition"
 * @return {Object} MousePosition value
 */
ClusterManager.prototype.getMousePosition = function(){
	return this.mousePosition;
};
/**
 * Setter for the attribute "mousePosition"
 * @param {Object} MousePosition value
 */
ClusterManager.prototype.setMousePosition = function(mousePosition){
	this.mousePosition = mousePosition;
};

/**
 * Getter for the attribute "rayPickingLastObject"
 * @return {Cluster.Object} RayPickingLastObject value
 */
ClusterManager.prototype.getRayPickingLastObject = function(){
	return this.rayPickingLastObject;
};
/**
 * Setter for the attribute "rayPickingLastObject"
 * @param {Cluster.Object} RayPickingLastObject value
 */
ClusterManager.prototype.setRayPickingLastObject = function(rayPickingLastObject){
	this.rayPickingLastObject = rayPickingLastObject;
};

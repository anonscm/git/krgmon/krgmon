/**
CLUSTER Cluster vizualisation for kerrighed
Copyright (c) 2010, Université François Rabelais - Tours - France
*/

/**
* @class Process class that add a new process in the scene
* @param {String} id Identifier of the process
* @constructor
*/
Cluster.Process = function(id){

	/**
	 * @constant
	 * Define the object for a process
	 */
	this.idType = Cluster.TYPE_PROCESS;
	/**
	 * Identifier of the process
	 */
	this.id = null;
	/**
	 * Host on which the process is running
	 */
	this.host = null;
	/**
	 * Drawable that will be displayed in the scene
	 */
	this.objectGL = null;
	/**
	 * The CPU consumption of the process (between 0 and 1), it will be displayed
	 * as close to the node if the CPU consumption is high, and on the PROCESS_RADIUS
	 * if the consumption is low
	 */
	this.cpuConsumption = 0.5;
	/**
	 * Interval called to destroy the process
	 */
	this.interval = null;
	/**
	 * The time when the transition has begun
	 */
	this.startTime = null;
	/**
	 * The x component of the beginrent position
	 */
	this.beginX = null;
	/**
	 * The y component of the beginrent position
	 */
	this.beginY = null;
	/**
	 * The z component of the beginrent position
	 */
	this.beginZ = null;
	/**
	 * The x component at the begining of the movement
	 */
	this.endX = null;
	/**
	 * The y component at the end of the movement
	 */
	this.endY = null;
	/**
	 * The z component at the end of the movement
	 */
	this.endZ = null;
	/**
	 * The speed on x component
	 */
	this.speedX = null;
	/**
	 * The speed on y component
	 */
	this.speedY = null;
	/**
	 * The speed on z component
	 */
	this.speedZ = null;
	
	this.initialize(id);	
};

/**
 * Initialize the object
 * @param {String} id Identifier of the process
 */
Cluster.Process.prototype.initialize = function(id)
{
    // we set the id for this
    this.setId(id);
    // we set also the cpu consumption of the process, beginrently random
    this.setCpuConsumption(Math.random());

    // We create a new object and assign it to process, the attribute that is
    // draw in the scene
    this.setObjectGL(new GLGE.Object(this.getIdType()+"_"+id));
    // we set the id of this object (not done by giving in argument
    // id to object !)
    this.getObjectGL().setId(this.getIdType()+"_"+id);
    // Here, we set the size of the node to NODE_SIZE
    this.getObjectGL().setScale(Cluster.PROCESS_SIZE, Cluster.PROCESS_SIZE, Cluster.PROCESS_SIZE);
    // we set the position of the process (object display in the scene)

    // Now, we create a new mesh, already defined in the scene.xml file
    // that the renderer will use to draw the sphere
    this.getObjectGL().setMesh(Cluster.sphereMesh, 0);

    // We create a new material to set on the node.
    var material = new GLGE.Material();
    // and we set his color to processColor
    //material.setColor(Cluster.processColor);
    this.getObjectGL().setMaterial(material, 0);

    // finally, we add the object to the scene
    ClusterManager.getSingleton().getObjectScene().addObject(this.getObjectGL());
    // We add this object to allProcesses that contains all the processes
    // that have been migrated in the cluster
    Cluster.getAllProcesses()[id] = this;

    Cluster.incProcessNb();
};


/**
 * Destroy the object
 */
Cluster.Process.prototype.destroy = function()
{
	if(this.getInterval() != null)
		clearInterval(this.getInterval());
	
	if(this.getHost() != null)
	{	
		delete this.getHost().getHisProcesses()[this.getId()];
		this.getHost().deleteProcess(this);
	}
	
    ClusterManager.getSingleton().getObjectScene().removeChild(this.getObjectGL());
    delete this.getObjectGL().getMaterial(0);
    delete this.getObjectGL().getMesh(0);
    delete this.getObjectGL();

    delete Cluster.getAllProcesses()[this.getId];
    
    Cluster.decProcessNb();
        
    delete this;
}

/**
 * Function to transform the CPU consumption of the process in a parameter
 * to adjust the orbit on which the process is gravitating
 * @return {Integer} The transform paramter
 * @private
 */
Cluster.Process.prototype.classify = function(){
    return 1 - this.getCpuConsumption() + 0.2;
};

/**
 * Function to migrate a process from one node to another
 * @param {Cluster.Node} node The node on which the process will execute
 */
Cluster.Process.prototype.migrateTo = function(node){
    new Cluster.Mark(this.getHost(), this);

    this.getHost().deleteProcess(this);
    node.addProcess(this, false);
    
    if(node.getId() == Cluster.ROOT_NODE && this.getInterval() == null)
		this.setInterval(setInterval("Cluster.allProcesses["+this.getId()+"].destroy();", 3000));
};


/**
* function to set the new position of the drawable
* @param {Integer} x The x components of the new position
* @param {Integer} y The y components of the new position
* @param {Integer} z The z components of the new position
*/
Cluster.Process.prototype.setPosition = function(x,y,z){
    this.goTo(x,y,z);
};


/**
 * PARTIE POSITIONNEMENT
 */


/**
 * function that set the start time and end value to null, because the
 * movement is finish, i.e. the object has reached his final point
 * @private
 */
Cluster.Process.prototype.reached = function(){
    this.setStartTime(null);
	this.setBeginX(null);
	this.setBeginY(null);
	this.setBeginZ(null);
	this.setEndX(null);
	this.setEndY(null);
	this.setEndZ(null);
};

/**
 * function that tell we have already reached or not the end point
 * @return {Boolean} true if the object is not moving or if it has reached
 * the end point
 */

Cluster.Process.prototype.isReached = function(){
    var isReached = false;
    if(this.getStartTime() == null){
        isReached = true;
        //Cluster.Log("Beginrent pos reached : "+this.node.getLocZ());
    }
    return isReached;
};

/**
 * function that specify that a movement is asked
 * @param {Integer} x The x components of the end position
 * @param {Integer} y The y components of the end position
 * @param {Integer} z The z components of the end position
 */
Cluster.Process.prototype.goTo = function(x, y, z){
    if(this.getHost() == null || this.getHost().getStartTime() == null){
        this.setStartTime(parseInt(new Date().getTime()));
    }
    else{
        this.setStartTime(this.getHost().getStartTime());
    }
	this.setBeginX(this.getObjectGL().getLocX());
	this.setBeginY(this.getObjectGL().getLocY());
	this.setBeginZ(this.getObjectGL().getLocZ());
	this.setEndX(x);
	this.setEndY(y);
	this.setEndZ(z);
    //this.synchronize();
    this.setSpeedX((this.getEndX() - this.getBeginX()) / Cluster.DURATION);
    this.setSpeedY((this.getEndY() - this.getBeginY()) / Cluster.DURATION);
    this.setSpeedZ((this.getEndZ() - this.getBeginZ()) / Cluster.DURATION);
    //Cluster.Log(this.getBeginZ()+" "+this.getEndZ()+" "+this.getSpeedZ());
    //Cluster.Log("Process on host "+this.getHost().getId()+" : "+this.getSpeedX()+" "+this.getSpeedY()+" "+this.getSpeedZ());
};

/**
 * function that set the position of the object in the scene, calculated from
 * the time that has been past, the duration of the movement and the last
 * position of the object
 */
Cluster.Process.prototype.setProcessesPosition = function(){
    //this.synchronize();
    if(!this.isReached()){
        // We compute the time that have past from the last call
        var time = parseInt(new Date().getTime()) - this.getStartTime();
        // we calcul the path that the object will follow
        if(time > Cluster.DURATION){
            this.getObjectGL().setLocX(this.getEndX());
            this.getObjectGL().setLocY(this.getEndY());
            this.getObjectGL().setLocZ(this.getEndZ());
            this.reached();
        }
        else{
            this.getObjectGL().setLocX(this.getBeginX() + this.getSpeedX()*time);
            this.getObjectGL().setLocY(this.getBeginY() + this.getSpeedY()*time);
            this.getObjectGL().setLocZ(this.getBeginZ() + this.getSpeedZ()*time);
        }   
    }
};




// Getter - Setter


/**
 * Getter for the attribute "idType"
 * @return {Integer} Id Type value
 */
Cluster.Process.prototype.getIdType = function(){
	return this.idType;
};

/**
 * Getter for the attribute "id"
 * @return {Integer} Id value
 */
Cluster.Process.prototype.getId = function(){
	return this.id;
};
/**
 * Getter for the attribute "id"
 * @param {Integer} Id value
 */
Cluster.Process.prototype.setId = function(id){
	this.id = id;
};

/**
 * Getter for the attribute "host"
 * @return {String} Host value
 */
Cluster.Process.prototype.getHost = function(){
	return this.host;
};
/**
 * Setter for the attribute "host"
 * @param {String} Host value
 */
Cluster.Process.prototype.setHost = function(host){
	this.host = host;
};

/**
 * Getter for the attribute "objectGL"
 * @return {GLGE.Object} ObjectGL value
 */
Cluster.Process.prototype.getObjectGL = function(){
	return this.objectGL;
};
/**
 * Setter for the attribute "objectGL"
 * @param {GLGE.Object} ObjectGL value
 */
Cluster.Process.prototype.setObjectGL = function(objectGL){
	this.objectGL = objectGL;
};

/**
 * Getter for the attribute "cpuConsumption"
 * @return {Float} CpuConsumption value
 */
Cluster.Process.prototype.getCpuConsumption = function(){
	return this.cpuConsumption;
};
/**
 * Setter for the attribute "cpuConsumption"
 * @param {Float} CpuConsumption value
 */
Cluster.Process.prototype.setCpuConsumption = function(cpuConsumption){
	this.cpuConsumption = cpuConsumption;
};

/**
 * Getter for the attribute "interval"
 * @return {Integer} Interval value
 */
Cluster.Process.prototype.getInterval = function(){
	return this.interval;
};
/**
 * Setter for the attribute "interval"
 * @param {Integer} Interval value
 */
Cluster.Process.prototype.setInterval = function(interval){
	this.interval = interval;
};

/**
 * Getter for the attribute "startTime"
 * @return {Time} StartTime value
 */
Cluster.Process.prototype.getStartTime = function(){
	return this.startTime;
};
/**
 * Setter for the attribute "startTime"
 * @param {Time} StartTime value
 */
Cluster.Process.prototype.setStartTime = function(startTime){
	this.startTime = startTime;
};

/**
 * Getter for the attribute "beginX"
 * @return {Float} BeginX value
 */
Cluster.Process.prototype.getBeginX = function(){
	return this.beginX;
};
/**
 * Setter for the attribute "beginX"
 * @param {Float} BeginX value
 */
Cluster.Process.prototype.setBeginX = function(beginX){
	this.beginX = beginX;
};

/**
 * Getter for the attribute "beginY"
 * @return {Float} BeginY value
 */
Cluster.Process.prototype.getBeginY = function(){
	return this.beginY;
};
/**
 * Setter for the attribute "beginY"
 * @param {Float} BeginY value
 */
Cluster.Process.prototype.setBeginY = function(beginY){
	this.beginY = beginY;
};

/**
 * Getter for the attribute "beginZ"
 * @return {Float} BeginZ value
 */
Cluster.Process.prototype.getBeginZ = function(){
	return this.beginZ;
};
/**
 * Setter for the attribute "beginZ"
 * @param {Float} BeginZ value
 */
Cluster.Process.prototype.setBeginZ = function(beginZ){
	this.beginZ = beginZ;
};

/**
 * Getter for the attribute "endX"
 * @return {Float} EndX value
 */
Cluster.Process.prototype.getEndX = function(){
	return this.endX;
};
/**
 * Setter for the attribute "endX"
 * @param {Float} EndX value
 */
Cluster.Process.prototype.setEndX = function(endX){
	this.endX = endX;
};

/**
 * Getter for the attribute "endY"
 * @return {Float} EndY value
 */
Cluster.Process.prototype.getEndY = function(){
	return this.endY;
};
/**
 * Setter for the attribute "endY"
 * @param {Float} EndY value
 */
Cluster.Process.prototype.setEndY = function(endY){
	this.endY = endY;
};

/**
 * Getter for the attribute "endZ"
 * @return {Float} EndZ value
 */
Cluster.Process.prototype.getEndZ = function(){
	return this.endZ;
};
/**
 * Setter for the attribute "endZ"
 * @param {Float} EndZ value
 */
Cluster.Process.prototype.setEndZ = function(endZ){
	this.endZ = endZ;
};

/**
 * Getter for the attribute "speedX"
 * @return {Float} SpeedX value
 */
Cluster.Process.prototype.getSpeedX = function(){
	return this.speedX;
};
/**
 * Setter for the attribute "speedX"
 * @param {Float} SpeedX value
 */
Cluster.Process.prototype.setSpeedX = function(speedX){
	this.speedX = speedX;
};

/**
 * Getter for the attribute "speedY"
 * @return {Float} SpeedY value
 */
Cluster.Process.prototype.getSpeedY = function(){
	return this.speedY;
};
/**
 * Setter for the attribute "speedY"
 * @param {Float} SpeedY value
 */
Cluster.Process.prototype.setSpeedY = function(speedY){
	this.speedY = speedY;
};

/**
 * Getter for the attribute "speedZ"
 * @return {Float} SpeedZ value
 */
Cluster.Process.prototype.getSpeedZ = function(){
	return this.speedZ;
};
/**
 * Setter for the attribute "speedZ"
 * @param {Float} SpeedZ value
 */
Cluster.Process.prototype.setSpeedZ = function(speedZ){
	this.speedZ = speedZ;
};

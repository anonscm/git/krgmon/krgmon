/**
CLUSTER Cluster vizualisation for kerrighed
Copyright (c) 2010, Université François Rabelais - Tours - France
*/

nodeList = function() { };
processList = function() { };
markList = function() { };
axisSphereList = function() { };

/**
 * @fileOverview
 * @name Cluster.js
 * @author julien.gomes@gmail.com
 * @author serge.mbittom@gmail.com
 * @author guillaume.smaha@gmail.com
 */
if(!Cluster){
    /**
    * @namespace Holds the functionality of the library
    */
    var Cluster={};
}

(function(Cluster){
    /**
     * @constant
     * @description System version
     */
    Cluster.version = "0.5"

    /**
     * @constant
     * @description Define the type of none
     */
    Cluster.TYPE_NULL = 0;
    /**
     * @constant
     * @description Define the type of a node
     */
    Cluster.TYPE_NODE = 1;
    /**
     * @constant
     * @description Define the type of a process
     */
    Cluster.TYPE_PROCESS = 2;
    /**
     * @constant
     * @description Define the type of a sphere axis
     */
    Cluster.TYPE_SPHERE_AXIS = 3;

    /**
     * @constant
     * @description Id of the root node (kill and fork process)
     */
    Cluster.ROOT_NODE = 0;

    /**
     * @constant
     * @description Name of the mesh for the sphere form (used to represent the
     * nodes and the processes) defined in the file scene.xml
     */
    Cluster.MESH_SPHERE_NAME = "sphere";
    /**
     * @constant
     * @description Name of the mesh for the circle form
     * defined in the file scene.xml
     */
    Cluster.MESH_CIRCLE_NAME = "circle";
    
    /**
     * @constant
     * @description Draw or not the mark between node and process
     */
    Cluster.DISPLAY_MARKS = false;
    /**
     * @constant
     * @description Radius of the maximum sphere on which the processes will
     * move around the nodes (depends on theCPU consumption of the process)
     */
    Cluster.PROCESS_RADIUS = 3;
    /**
     * @constant
     * @description Radius minimum of the sphere on which the nodes will be positionate.
     * Center of the node is (0,0,0)
     */
    Cluster.NODE_RADIUS_MIN = 12;
    /**
     * @constant
     * @description Radius factor of the sphere on which the nodes will be positionate.
     * Center of the node is (0,0,0)
     */
    Cluster.NODE_RADIUS_FACTOR = 0.25;
    /**
     * @constant
     * @description Size of a node sphere (scale = 1)
     */
    Cluster.NODE_SIZE = 1;
    /**
     * @constant
     * @description Size of a process sphere (scale = 0.1)
     */
    Cluster.PROCESS_SIZE = 0.2;

    /**
     * @constant
     * @description The duration of an animation (for example during a migration
     * when you see the process moving from one node to another) it's in milliseconds
     */
    Cluster.DURATION = 1000;

    /**
     * @constant
     * @description The frequency on which the database will be called
     */
    //~ Cluster.DATABASE_CALL_FREQUENCY = 1000;
    
    /**
     * @description The element containing the default scene 
     */
    Cluster.documentScene = null;
    /**
     * @type Array
     * @description An array that contains all the nodes
     */
    Cluster.allNodes = null ;
    /**
     * @type Array
     * @description An array that contains all the processes
     */
    Cluster.allProcesses = null;
    /**
     * @type Array
     * @description An array that contains all the marks
     */
    Cluster.allMarks = null;
    /**
     * @type Array
     * @description An array that contains all the axis circle
     */
    Cluster.allAxisSphere = null;

    /**
     * @description The mesh for the sphere
     */
    Cluster.sphereMesh = null;
    /**
     * @description The mesh for the sphere
     */
    Cluster.circleMesh = null;
    /**
     * @description Number of nodes present in the cluster
     */
    Cluster.nodeNb = 0;
    /**
     * @description Number of process present in the cluster
     */
    Cluster.processNb = 0;
    /**
     * @description Number of marks present in the cluster
     */
    Cluster.marksNb = 0;
    /**
     * @type Integer
     * @description The current number of database call. It's used to get the
     * next records in the database.
     */
    //~ Cluster.databaseCall = 0;
    /**
     * @description Identifier return by the setInterval function, used for the play/pause feature
     */
    //~ Cluster.intervalId = null;

	/**
	 * Initialize the object
	 */
    Cluster.initialize = function(documentScene)
    {
		Cluster.setDocumentScene(documentScene);
        
        Cluster.setAllNodes(new nodeList());
        Cluster.setAllProcesses(new processList());
        Cluster.setAllMarks(new markList());
        Cluster.setAllAxisSphere(new axisSphereList());
        
        Cluster.setNodeNb(0);
        Cluster.setProcessNb(0);
        Cluster.setMarksNb(0);
        
        Cluster.createMesh();
        
		new Cluster.AxisSphere("center");
    }


	/**
	 * Destroy the object
	 */
	Cluster.destroy = function()
	{	
		for(var id in this.allNodes)
		{
			if(this.getAllNodes()[id] != null)
			{
				this.getAllNodes()[id].destroy();
			}
		}
		
		//For the processes residual
		for(var id in this.getAllProcesses())
		{
			if(this.getAllProcesses()[id] != null)
			{
				this.getAllProcesses()[id].destroy();
			}
		}
		
		//For the marks residual
		for(var id in this.getAllMarks())
		{
			if(this.getAllMarks()[id] != null)
			{
				this.getAllMarks()[id].destroy();
			}
		}
		
		//For the axis
		for(var id in this.getAllAxisSphere())
		{
			if(this.getAllAxisSphere()[id] != null)
			{
				this.getAllAxisSphere()[id].destroy();
			}
		}
		
		delete this;
	}


    /**
    * Start the animation
    */
    Cluster.play = function(){
        //~ Cluster.getMigration();
        //~ Cluster.intervalId = setInterval(Cluster.getMigration, Cluster.DATABASE_CALL_FREQUENCY);
    }

    /**
    * Stop the animation and wait the play command
    */
    Cluster.pause = function(){
        //~ clearInterval(Cluster.intervalId);
        //~ Cluster.intervalId = null;
    }


    /**
    * function to create a unique mesh for the sphere
    * @private
    */
    Cluster.createMesh = function(){
        Cluster.setSphereMesh(Cluster.getDocumentScene().getElement(Cluster.MESH_SPHERE_NAME));
        Cluster.setCircleMesh(Cluster.getDocumentScene().getElement(Cluster.MESH_CIRCLE_NAME));
    };

    /**
    * function to get call the server via ajax to get an xml response
    * @private
    */
/*
    Cluster.getMigration = function (){
        // We call the file db.php to get the xml
        $.ajax({
            type: "GET",
            url: "db.php",
            dataType: "xml",
            data : {
                // We say that we want the records from the
                // nth record
                begin:Cluster.databaseCall
            },
            complete : function(data, status) {
                // On complete, we call the parseXML method
                Cluster.parseXML(data.responseXML);
            }
        });
        // As we have called the database, we increment databaseCall
        Cluster.databaseCall++;
    };
*/

    /**
    * function to parse the xml retrieve from the database
    * @param {XMLDocument} xmlDoc The xml document to parse
    * @private
    */
    Cluster.parseXML = function(xmlDoc){
        // For each migrations retrieved
        $(xmlDoc).find('migration').each(function(){
            var processId = $(this).find('processId').text();
            var startNode = $(this).find('startNode').text();
            var endNode = $(this).find('endNode').text();
            // We create a new migration
            Cluster.displayMigrations(processId,startNode,endNode);
        });
    };

    /**
    * function to display migration in the current scene
    * @param {Integer} processId Identifier (supposed to be unique) of the process
    * @param {Integer} startNode Identifier (supposed to be unique) of the start node
    * @param {Integer} endNode Identifier (supposed to be unique) of the end node
    * @private
    */
    Cluster.displayMigrations = function(processId,startNode,endNode){
		
        // If the start node doesn't exists, we create it
        if(!Cluster.getAllNodes()[startNode]) new Cluster.Node(startNode);
        
        // If the end node doesn't exists, we create it
        if(!Cluster.getAllNodes()[endNode]) new Cluster.Node(endNode);
        
        // If the process doesn't exists, we create it
        if(!Cluster.getAllProcesses()[processId]){
            new Cluster.Process(processId);
            
            // and we add it to the start node
            Cluster.getAllNodes()[startNode].addProcess(Cluster.getAllProcesses()[processId], true);
        }
        
        // We log the migration
        Cluster.Log("Process "+processId+" migrates from node "+startNode+" to node "+endNode);
        
        // And we proceed the migration from startNode to endNode
        Cluster.getAllProcesses()[processId].migrateTo(Cluster.getAllNodes()[endNode]);
    };
 
	/**
	* function to get the node radius
	*/
	Cluster.getNodeRadius = function(){
		var nbNode = Cluster.nodeNb;

		for(var id in Cluster.allNodes){
			if(id == Cluster.ROOT_NODE)
			{
				nbNode--;
				break;
			}
		}

		return Cluster.NODE_RADIUS_MIN + nbNode * Cluster.NODE_RADIUS_FACTOR;
	}

    /**
    * function to set the position of all the nodes of the cluster
    * @private
    */
    Cluster.setNodesPositions = function(){
        var k = 0;
        var nbNode = Cluster.getNodeNb();
        var theta = new Array();
        var phi = new Array();
        
        
        for(var id in Cluster.getAllNodes()){
			if(id == Cluster.ROOT_NODE)
			{
				nbNode--;
				break;
			}
		}
	
		var radius = Cluster.getNodeRadius();
		var list = Cluster.SpherePosition.getSphereDistribution(nbNode, radius);
    
        // For all the nodes present in the cluster
        for(var id in Cluster.getAllNodes()){
			if(id != Cluster.ROOT_NODE)
			{
				var pos = list[k];
								
				// and we assign to the node his new position
				this.getAllNodes()[id].setPosition(pos[0], pos[1], pos[2]);
				
				k++;
			}
			else
				this.getAllNodes()[id].setPosition(0.0, 0.0, 0.0);
        }
    };

    /**
    * function to get the color of the node, rely on the number of nodes
    * TODO generate automatically a new color, to avoid the redundancy of the color
    * @private
    */
    Cluster.generateNodeColor = function(){
        var color = Cluster.getNodeNb() % 8;
        switch(color){
            case 0 :
                return "#E00000";
                break;
            case 1 :
                return "#00940d";
                break;
            case 2 :
                return "#1900f0";
                break;
            case 3 :
                return "#8900c2";
                break;
            case 4 :
                return "#ea7406";
                break;
            case 5 :
                return "#ffde05";
                break;
            case 6 :
                return "#ff709f";
                break;
            case 7 :
                return "#92d600";
                break;
        }
    };
    

    

    /**
     * function that will returns the type and object of the GL object
     * @param {String} object The id of GL object
     * @return {Object} Returns the type and the object of the GL object or null
     */
    Cluster.getObjectFromIdGLObject = function(idGLObject)
    {
		var result = null;
		var chaine = new String(idGLObject);
		
		var match = chaine.strip().match(/([0-9]{1})\_([0-9]+)$/);
		
		if(match != null)
		{
			if(match[1] == Cluster.TYPE_PROCESS)
			{
				result = {idType: Cluster.TYPE_PROCESS, object:Cluster.getAllProcesses()[match[2]]};
			}
			else if(match[1] == Cluster.TYPE_NODE)
			{
				result = {idType: Cluster.TYPE_NODE, object:Cluster.getAllNodes()[match[2]]};
			}
		}
        
        return result;
    };
    
    /**
     * function that will returns the type and id of the object
     * @param {Integer} idType The type of object
     * @param {Integer} idObject The id of the object
     * @return {Object} Returns the object or null
     */
    Cluster.getObjectFromIdObject = function(idType, idObject)
    {
		var result = null;
		
		if(idType != Cluster.TYPE_NULL && idObject != 0)
		{
			if(idType == Cluster.TYPE_PROCESS)
			{
				result = Cluster.getAllProcesses()[idObject];
			}
			else if(idType == Cluster.TYPE_NODE)
			{
				result = Cluster.getAllNodes()[idObject];
			}
		}
        
        return result;
    };

    

    /**
     * function that will print the description of a node or a process
     * @param {Object} object The object node/process
     */
    Cluster.getDescriptionFromId = function(object)
    {
        if(object.idType == Cluster.TYPE_PROCESS)
        {
            Cluster.PrintInfoPick("Process : "+object.getId(), "Host id : "+object.getHost().getId());
        }
        else if(object.idType == Cluster.TYPE_NODE)
        {
            Cluster.PrintInfoPick("Node : "+object.getId(), "Nb de processus : "+object.getNbProcesses());
        }
    };

    /**
     * function that will print to screen a title and a text, usually used to
     * to display the caracteristics of a node
     * @param {String} title The tile to be displayed
     * @param {String} text The text to be displayed
     */
    Cluster.PrintInfoPick = function(title, text)
    {
        if(title != "")
        {
            document.getElementById("info-pick-title").innerHTML = title;
            document.getElementById("info-pick-display").innerHTML = text;
        }
        else
        {
            document.getElementById("info-pick-title").innerHTML = "Pick a node";
            document.getElementById("info-pick-display").innerHTML = "<br/>";
        }
    };

    /**
     * function that will print to screen a ttile and a text, usually used to
     * to display the object tracked
     * @param {String} title The tile to be displayed
     * @param {String} text The text to be displayed
     */
    Cluster.PrintInfoTrack = function(title, text)
    {
        if(title != "")
        {
            document.getElementById("info-track-title").innerHTML = "Tracked : "+title;
            document.getElementById("info-track-display").innerHTML = text;
        }
        else
        {
            document.getElementById("info-track-title").innerHTML = "Tracked : None";
            document.getElementById("info-track-display").innerHTML = "<br/>";
        }
    };

    /**
     * function that log things you want to log
     * @param {String} text text to be displayed in the console
     */
    Cluster.Log = function(text){
		if(document.getElementById("console") != null)
			document.getElementById("console").innerHTML = text + "\n" + document.getElementById("console").innerHTML;
    };


    /**
     * function called each time that the scene is redraw
     * @param {Date} start the begining of the animation
     * @param {Date} now the date when this function is called
     */
    Cluster.render = function(start,now){
	    // For all the processes and all the nodes, if they have not reached their
	    // final position, we update their position.
	    for(var id in Cluster.getAllNodes()){
		    Cluster.getAllNodes()[id].setNodePosition();
	    }

	    for(var id in Cluster.getAllProcesses()){
		    Cluster.getAllProcesses()[id].setProcessesPosition();
	    }

	    for(var id in Cluster.getAllMarks()){
		    if(Cluster.DISPLAY_MARKS){
			    Cluster.getAllMarks()[id].render();
		    }
		    else{
			    Cluster.getAllMarks()[id].hide();
		    }
	    }

	    for(var id in Cluster.getAllAxisSphere()){
		    Cluster.getAllAxisSphere()[id].render();
	    }
    };
    
    
    
//Getter - Setter
    



/**
 * Getter for the attribute "documentScene"
 * @return {GLGE.Document} DocumentScene value
 */
Cluster.getDocumentScene = function(){
	return Cluster.documentScene;
};
/**
 * Setter for the attribute "documentScene"
 * @param {GLGE.Document} DocumentScene value
 */
Cluster.setDocumentScene = function(documentScene){
	Cluster.documentScene = documentScene;
};
/**
 * Getter for the attribute "allProcesses"
 * @return {processList} AllProcesses value
 */
Cluster.getAllProcesses = function(){
	return Cluster.allProcesses;
};
/**
 * Setter for the attribute "allProcesses"
 * @param {processList} AllProcesses value
 */
Cluster.setAllProcesses = function(allProcesses){
	Cluster.allProcesses = allProcesses;
};

/**
 * Getter for the attribute "allNodes"
 * @return {nodeList} AllNodes value
 */
Cluster.getAllNodes = function(){
	return Cluster.allNodes;
};
/**
 * Setter for the attribute "allNodes"
 * @param {nodeList} AllNodes value
 */
Cluster.setAllNodes = function(allNodes){
	Cluster.allNodes = allNodes;
};

/**
 * Getter for the attribute "allMarks"
 * @return {markList} AllMarks value
 */
Cluster.getAllMarks = function(){
	return Cluster.allMarks;
};
/**
 * Setter for the attribute "allMarks"
 * @param {markList} AllMarks value
 */
Cluster.setAllMarks = function(allMarks){
	Cluster.allMarks = allMarks;
};

/**
 * Getter for the attribute "allAxisSphere"
 * @return {axisSphereList} AllAxisSphere value
 */
Cluster.getAllAxisSphere = function(){
	return Cluster.allAxisSphere;
};
/**
 * Setter for the attribute "allAxisSphere"
 * @param {axisSphereList} AllAxisSphere value
 */
Cluster.setAllAxisSphere = function(allAxisSphere){
	Cluster.allAxisSphere = allAxisSphere;
};

/**
 * Getter for the attribute "sphereMesh"
 * @return {GLGE.Mesh} SphereMesh value
 */
Cluster.getSphereMesh = function(){
	return Cluster.sphereMesh;
};
/**
 * Setter for the attribute "sphereMesh"
 * @param {GLGE.Mesh} SphereMesh value
 */
Cluster.setSphereMesh = function(sphereMesh){
	Cluster.sphereMesh = sphereMesh;
};

/**
 * Getter for the attribute "circleMesh"
 * @return {GLGE.Mesh} CircleMesh value
 */
Cluster.getCircleMesh = function(){
	return Cluster.circleMesh;
};
/**
 * Setter for the attribute "circleMesh"
 * @param {GLGE.Mesh} CircleMesh value
 */
Cluster.setCircleMesh = function(circleMesh){
	Cluster.circleMesh = circleMesh;
};

/**
 * Increment the value of the attribute "nodeNb"
 */
Cluster.incNodeNb = function(){
	Cluster.setNodeNb(this.getNodeNb() + 1);
};
/**
 * Decrement the value of the attribute "nodeNb"
 */
Cluster.decNodeNb = function(){
	Cluster.setNodeNb(this.getNodeNb() - 1);
};
/**
 * Getter for the attribute "nodeNb"
 * @return {Integer} NodeNb value
 */
Cluster.getNodeNb = function(){
	return Cluster.nodeNb;
};
/**
 * Setter for the attribute "nodeNb"
 * @param {Integer} NodeNb value
 */
Cluster.setNodeNb = function(nodeNb){
	Cluster.nodeNb = nodeNb;
};

/**
 * Increment the value of the attribute "processNb"
 */
Cluster.incProcessNb = function(){
	Cluster.setProcessNb(this.getProcessNb() + 1);
};
/**
 * Decrement the value of the attribute "processNb"
 */
Cluster.decProcessNb = function(){
	Cluster.setProcessNb(this.getProcessNb() - 1);
};
/**
 * Getter for the attribute "processNb"
 * @return {Integer} ProcessNb value
 */
Cluster.getProcessNb = function(){
	return Cluster.processNb;
};
/**
 * Setter for the attribute "processNb"
 * @param {Integer} ProcessNb value
 */
Cluster.setProcessNb = function(processNb){
	Cluster.processNb = processNb;
};

/**
 * Increment the value of the attribute "marksNb"
 */
Cluster.incMarksNb = function(){
	Cluster.setMarksNb(this.getMarksNb() + 1);
};
/**
 * Decrement the value of the attribute "marksNb"
 */
Cluster.decMarksNb = function(){
	Cluster.setMarksNb(this.getMarksNb() - 1);
};
/**
 * Getter for the attribute "marksNb"
 * @return {Integer} ProcessNb value
 */
Cluster.getMarksNb = function(){
	return Cluster.marksNb;
};
/**
 * Setter for the attribute "marksNb"
 * @param {Integer} MarksNb value
 */
Cluster.setMarksNb = function(marksNb){
	Cluster.marksNb = marksNb;
};
    
})(Cluster);

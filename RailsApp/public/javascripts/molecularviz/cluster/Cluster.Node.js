/**
CLUSTER Cluster vizualisation for kerrighed
Copyright (c) 2010, Université François Rabelais - Tours - France
*/

/**
* @class Node class that add a new node in the scene
* @param {String} id Identifier of the node
* @constructor
*/
Cluster.Node = function(id){
	
	/**
	 * @constant
	 * Define the object for a node
	 */
	this.idType = Cluster.TYPE_NODE;
	/**
	 * Identifier of the node
	 */
	this.id = null;
	/**
	 * Color of the node
	 */
	this.color = null;
	/**
	 * The drawable that will be displayed in the scene
	 */
	this.objectGL = null;
	/**
	 * An array that contains all the processes attached to the node
	 */
	this.hisProcesses = null;
	/**
	 * The number of processes currently running on the node
	 */
	this.nbProcesses = 0;
	/**
	 * The time when the transition has begun
	 */
	this.startTime = null;
	/**
	 * The x component at the begining of the movement
	 */
	this.beginX = null;
	/**
	 * The y component at the begining of the movement
	 */
	this.beginY = null;
	/**
	 * The z component at the begining of the movement
	 */
	this.beginZ = null;
	/**
	 * The x component at the begining of the movement
	 */
	this.endX = null;
	/**
	 * The y component at the end of the movement
	 */
	this.endY = null;
	/**
	 * The z component at the end of the movement
	 */
	this.endZ = null;
	/**
	 * The speed on x component
	 */
	this.speedX = null;
	/**
	 * The speed on y component
	 */
	this.speedY = null;
	/**
	 * The speed on z component
	 */
	this.speedZ = null;
	
	this.initialize(id);
};



/**
 * Initialize the object
 * @param {String} id Identifier of the node
 */
Cluster.Node.prototype.initialize = function(id)
{	
    // we set the id for this
    this.setId(id);
    // we create an array that will contains all the processes attached
    // to the node
    this.setHisProcesses(new processList());
    //~ this.setHisProcesses(new processList());

    // We create a new object and assign it to node, the attribute that is
    // draw in the scene
    this.setObjectGL(new GLGE.Object(this.getIdType()+"_"+id));
    // we set the id of this object (not done by giving in argument
    // id to object !)
    this.getObjectGL().setId(this.getIdType()+"_"+id);
    // Here, we set the size of the node to NODE_SIZE
    this.getObjectGL().setScale(Cluster.NODE_SIZE, Cluster.NODE_SIZE, Cluster.NODE_SIZE);
    // we set the position of the node (object display in the scene)

    // Now, we create a new mesh, already defined in the scene.xml file
    // that the renderer will use to draw the sphere
    this.getObjectGL().setMesh(Cluster.getSphereMesh());

    // We create a new material to set on the node.
    var material = new GLGE.Material();
    // We used the generate color to apply on the node
    if(this.getId() == Cluster.ROOT_NODE)
		this.setColor("#000000");
	else
		this.setColor(Cluster.generateNodeColor());
		
    material.setColor(this.getColor());
    this.getObjectGL().setMaterial(material);

    // finally, we add the object to the scene
    ClusterManager.getSingleton().getObjectScene().addObject(this.objectGL);

    // We add this object to allNodes that contains all the object
    Cluster.getAllNodes()[id] = this;
    // And we increment the total number of nodes
    Cluster.incNodeNb();

    // And finally we set the position of all the nodes (because when one node
    // is added, all the positions of the nodes are updated)
    Cluster.setNodesPositions();
}

/**
 * Destroy the object
 */
Cluster.Node.prototype.destroy = function()
{	
    for(var id in this.getHisProcesses())
    {
		if(this.getHisProcesses()[id] != null)
		{
			this.getHisProcesses()[id].destroy();
		}
	}
	
    delete Cluster.getAllNodes()[this.getId()];

    ClusterManager.getSingleton().getObjectScene().removeChild(this.getObjectGL());
    
    delete this.getObjectGL().getMaterial(0);
    delete this.getObjectGL().getMesh(0);
    delete this.getObjectGL();

    Cluster.decNodeNb();
    
    delete this;
}

/**
* function to set the new position of the drawable
* @param {Integer} x The x components of the new position
* @param {Integer} y The y components of the new position
* @param {Integer} z The z components of the new position
*/
Cluster.Node.prototype.setPosition = function(x,y,z){
    // We specify to the position that it has to be update
    this.goTo(x,y,z);
    //this.setProcessesPositions();
    // We set also the position of the processes attached to this node
};

/**
* function to add a new process to a node
* @param {Cluster.Process} processToAdd The process to add to the node
* @param {Boolean} firstCreated True if the process if newly created
*/
Cluster.Node.prototype.addProcess = function(processToAdd, firstCreated){
    // We specify to the process that it is attached to this node now
    processToAdd.setHost(this);
    // Wa add the process in the array of all the processes that this node
    // has
    this.getHisProcesses()[processToAdd.getId()] = processToAdd;
    // We increment the number of process he got
    this.incNbProcesses();

    if(firstCreated){
        processToAdd.getObjectGL().setLocX(this.getObjectGL().getLocX());
        processToAdd.getObjectGL().setLocY(this.getObjectGL().getLocY());
        processToAdd.getObjectGL().setLocZ(this.getObjectGL().getLocZ());
    }

    // Finally, we update the position of all the processes owned by this
    // node
    this.setProcessesPositions();
};

/**
* function to delete a process from a node
* @param {Cluster.Process} processToRemove The process to remove from the node
*/
Cluster.Node.prototype.deleteProcess = function(processToRemove){
    // We specify to the process that it's doesn't have node attached anymore
    processToRemove.setHost(null);
    // We delete the entry in the array of all the processes of this node
    delete this.getHisProcesses()[processToRemove.getId()];
    // We decrement the number of processes owned by this node
    this.decNbProcesses();
    // And we set the position of all the other process owned y this node
    this.setProcessesPositions();
};

/**
* function to set the position of all the processes owned by a node
* @private
* TODO : Set the position on a sphere, no more on a circle
*/
Cluster.Node.prototype.setProcessesPositions = function(){
    var nodeX = null;
        var nodeY = null;
        var nodeZ = null;
        // If the node is currently moving, i.e. his end position is not null,
        // then the node position is given to the process as the end position
        // else, if the node is not moving, it's given as the current position
        if(this.getEndX() != null && this.getEndY() != null && this.getEndZ() != null){
            nodeX = this.getEndX();
            nodeY = this.getEndY();
            nodeZ = this.getEndZ();
        }else{
            nodeX = this.getObjectGL().getLocX();
            nodeY = this.getObjectGL().getLocY();
            nodeZ = this.getObjectGL().getLocZ();
        }
        
    var i = 0;
    // For all the processes of the node
    
    var list = Cluster.SpherePosition.getSphereDistribution(this.getNbProcesses(), Cluster.PROCESS_RADIUS);
    
    for(var id in this.getHisProcesses()){
        // We compute the x, y and z offset
        var pos = list[i];
        
        pos = GLGE.scaleVec3(pos, this.getHisProcesses()[id].classify());
        
        var x = pos[0];
        var y = pos[1];
        var z = pos[2];
        
        // And we set the new position
        this.getHisProcesses()[id].setPosition(nodeX+x, nodeY+y, nodeZ+z);
        i++;
    }
};

/**
 * PARTIE POSITIONNEMENT
 */




/**
 * function that set the start time and end value to null, because the
 * movement is finish, i.e. the object has reached his final point
 * @private
 */
Cluster.Node.prototype.reached = function(){
    //var isReached = false;
        //Cluster.Log("Current pos reached : "+this.node.getLocZ());
    //if(this.curX == this.endX && this.curY == this.endY && this.curZ == this.endZ){
	this.getObjectGL().setLocX(this.getEndX());
	this.getObjectGL().setLocY(this.getEndY());
	this.getObjectGL().setLocZ(this.getEndZ());
    this.setStartTime(null);
	this.setBeginX(null);
	this.setBeginY(null);
	this.setBeginZ(null);
	this.setEndX(null);
	this.setEndY(null);
	this.setEndZ(null);
        //isReached = true;
    //}
    //return isReached;
};

/**
 * function that tell we have already reached or not the end point
 * @return {Boolean} true if the object is not moving or if it has reached
 * the end point
 */
Cluster.Node.prototype.isReached = function(){
    var isReached = false;
    if(this.getStartTime() == null){
        isReached = true;
        //Cluster.Log("Current pos reached : "+this.node.getLocZ());
    }
    return isReached;
};

/**
 * function that specify that a movement is asked
 * @param {Integer} x The x components of the end position
 * @param {Integer} y The y components of the end position
 * @param {Integer} z The z components of the end position
 */
Cluster.Node.prototype.goTo = function(x,y,z){
    this.setStartTime(parseInt(new Date().getTime()));
	this.setBeginX(this.getObjectGL().getLocX());
	this.setBeginY(this.getObjectGL().getLocY());
	this.setBeginZ(this.getObjectGL().getLocZ());
	this.setEndX(x);
	this.setEndY(y);
	this.setEndZ(z);
    //this.synchronize();
    //var uneCte = 63472620700;
    this.setSpeedX((this.getEndX() - this.getBeginX()) / Cluster.DURATION);
    this.setSpeedY((this.getEndY() - this.getBeginY()) / Cluster.DURATION);
    this.setSpeedZ((this.getEndZ() - this.getBeginZ()) / Cluster.DURATION);
    //Cluster.Log("Node  "+this.id+" : "+this.speedX+" "+this.speedY+" "+this.speedZ);
    this.setProcessesPositions();
};

/**
 * function that set the position of the object in the scene, calculated from
 * the time that has been past, the duration of the movement and the last
 * position of the object
 */
Cluster.Node.prototype.setNodePosition = function(){
    //this.synchronize();
    if(!this.isReached()){
        // We compute the time that have past from the last call
        var time = parseInt(new Date().getTime()) - this.getStartTime();
        // we calcul the path that the object will follow
        if(time > Cluster.DURATION){
            this.reached();
        }else{
            this.getObjectGL().setLocX(this.getBeginX() + this.getSpeedX() * time);
            this.getObjectGL().setLocY(this.getBeginY() + this.getSpeedY() * time);
            this.getObjectGL().setLocZ(this.getBeginZ() + this.getSpeedZ() * time);
        }

        //this.setProcessesPositions();
    }
};


// Getter - Setter

/**
 * Getter for the attribute "idType"
 * @return {Integer} Id Type value
 */
Cluster.Node.prototype.getIdType = function(){
	return this.idType;
};

/**
 * Getter for the attribute "id"
 * @return {Integer} Id value
 */
Cluster.Node.prototype.getId = function(){
	return this.id;
};
/**
 * Getter for the attribute "id"
 * @param {Integer} Id value
 */
Cluster.Node.prototype.setId = function(id){
	this.id = id;
};

/**
 * Getter for the attribute "color"
 * @return {String} Color value
 */
Cluster.Node.prototype.getColor = function(){
	return this.color;
};
/**
 * Setter for the attribute "color"
 * @param {String} Color value
 */
Cluster.Node.prototype.setColor = function(color){
	this.color = color;
};

/**
 * Getter for the attribute "hisProcesses"
 * @return {processList} HisProcesses value
 */
Cluster.Node.prototype.getHisProcesses = function(){
	return this.hisProcesses;
};
/**
 * Setter for the attribute "hisProcesses"
 * @param {processList} HisProcesses value
 */
Cluster.Node.prototype.setHisProcesses = function(hisProcesses){
	this.hisProcesses = hisProcesses;
};

/**
 * Getter for the attribute "objectGL"
 * @return {GLGE.Object} ObjectGL value
 */
Cluster.Node.prototype.getObjectGL = function(){
	return this.objectGL;
};
/**
 * Setter for the attribute "objectGL"
 * @param {GLGE.Object} ObjectGL value
 */
Cluster.Node.prototype.setObjectGL = function(objectGL){
	this.objectGL = objectGL;
};

/**
 * Increment the value of the attribute "nbProcesses"
 */
Cluster.Node.prototype.incNbProcesses = function(){
	this.setNbProcesses(this.getNbProcesses() + 1);
};
/**
 * Decrement the value of the attribute "nbProcesses"
 */
Cluster.Node.prototype.decNbProcesses = function(){
	this.setNbProcesses(this.getNbProcesses() - 1);
};
/**
 * Getter for the attribute "nbProcesses"
 * @return {Integer} NbProcesses value
 */
Cluster.Node.prototype.getNbProcesses = function(){
	return this.nbProcesses;
};
/**
 * Setter for the attribute "nbProcesses"
 * @param {Integer} NbProcesses value
 */
Cluster.Node.prototype.setNbProcesses = function(nbProcesses){
	this.nbProcesses = nbProcesses;
};

/**
 * Getter for the attribute "startTime"
 * @return {Time} StartTime value
 */
Cluster.Node.prototype.getStartTime = function(){
	return this.startTime;
};
/**
 * Setter for the attribute "startTime"
 * @param {Time} StartTime value
 */
Cluster.Node.prototype.setStartTime = function(startTime){
	this.startTime = startTime;
};

/**
 * Getter for the attribute "beginX"
 * @return {Float} BeginX value
 */
Cluster.Node.prototype.getBeginX = function(){
	return this.beginX;
};
/**
 * Setter for the attribute "beginX"
 * @param {Float} BeginX value
 */
Cluster.Node.prototype.setBeginX = function(beginX){
	this.beginX = beginX;
};

/**
 * Getter for the attribute "beginY"
 * @return {Float} BeginY value
 */
Cluster.Node.prototype.getBeginY = function(){
	return this.beginY;
};
/**
 * Setter for the attribute "beginY"
 * @param {Float} BeginY value
 */
Cluster.Node.prototype.setBeginY = function(beginY){
	this.beginY = beginY;
};

/**
 * Getter for the attribute "beginZ"
 * @return {Float} BeginZ value
 */
Cluster.Node.prototype.getBeginZ = function(){
	return this.beginZ;
};
/**
 * Setter for the attribute "beginZ"
 * @param {Float} BeginZ value
 */
Cluster.Node.prototype.setBeginZ = function(beginZ){
	this.beginZ = beginZ;
};

/**
 * Getter for the attribute "endX"
 * @return {Float} EndX value
 */
Cluster.Node.prototype.getEndX = function(){
	return this.endX;
};
/**
 * Setter for the attribute "endX"
 * @param {Float} EndX value
 */
Cluster.Node.prototype.setEndX = function(endX){
	this.endX = endX;
};

/**
 * Getter for the attribute "endY"
 * @return {Float} EndY value
 */
Cluster.Node.prototype.getEndY = function(){
	return this.endY;
};
/**
 * Setter for the attribute "endY"
 * @param {Float} EndY value
 */
Cluster.Node.prototype.setEndY = function(endY){
	this.endY = endY;
};

/**
 * Getter for the attribute "endZ"
 * @return {Float} EndZ value
 */
Cluster.Node.prototype.getEndZ = function(){
	return this.endZ;
};
/**
 * Setter for the attribute "endZ"
 * @param {Float} EndZ value
 */
Cluster.Node.prototype.setEndZ = function(endZ){
	this.endZ = endZ;
};

/**
 * Getter for the attribute "speedX"
 * @return {Float} SpeedX value
 */
Cluster.Node.prototype.getSpeedX = function(){
	return this.speedX;
};
/**
 * Setter for the attribute "speedX"
 * @param {Float} SpeedX value
 */
Cluster.Node.prototype.setSpeedX = function(speedX){
	this.speedX = speedX;
};

/**
 * Getter for the attribute "speedY"
 * @return {Float} SpeedY value
 */
Cluster.Node.prototype.getSpeedY = function(){
	return this.speedY;
};
/**
 * Setter for the attribute "speedY"
 * @param {Float} SpeedY value
 */
Cluster.Node.prototype.setSpeedY = function(speedY){
	this.speedY = speedY;
};

/**
 * Getter for the attribute "speedZ"
 * @return {Float} SpeedZ value
 */
Cluster.Node.prototype.getSpeedZ = function(){
	return this.speedZ;
};
/**
 * Setter for the attribute "speedZ"
 * @param {Float} SpeedZ value
 */
Cluster.Node.prototype.setSpeedZ = function(speedZ){
	this.speedZ = speedZ;
};

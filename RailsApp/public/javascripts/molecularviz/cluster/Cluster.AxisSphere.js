/**
CLUSTER Cluster vizualisation for kerrighed
Copyright (c) 2010, Université François Rabelais - Tours - France
*/

/**
* @class Avis class that add a new axis in the scene
* @param {String} id Identifier of the axis
* @constructor
*/
Cluster.AxisSphere = function(id){
	/**
	 * @constant
	 * Define the object for a sphere axis
	 */
	this.idType = Cluster.TYPE_SPHERE_AXIS;
	/**
	 * Identifier of the node
	 */
	this.id = null;
	/**
	 * Identifier of the node
	 */
	this.position = null;
	/**
	 * Color of the circle X
	 */
	this.colorX = null;
	/**
	 * Color of the circle Y
	 */
	this.colorY = null;
	/**
	 * Color of the circle Z
	 */
	this.colorZ = null;
	/**
	 * The drawable that will be displayed in the scene
	 */
	this.objectGLX = null;
	/**
	 * The drawable that will be displayed in the scene
	 */
	this.objectGLY = null;
	/**
	 * The drawable that will be displayed in the scene
	 */
	this.objectGLZ = null;

	this.initialize(id);
};

/**
 * Initialize the object
 * @param {String} id Identifier of the axis sphere
 */
Cluster.AxisSphere.prototype.initialize = function(id)
{
	this.setId(id);

	this.setPosition(GLGE.Vec3(0.0, 0.0, 0.0));

	this.setColorX("#ff0000");
	this.setColorY("#00ff00");
	this.setColorZ("#0000ff");

    this.setObjectGLX(new GLGE.Object("axis_"+id+"_x"));
    this.setObjectGLY(new GLGE.Object("axis_"+id+"_y"));
    this.setObjectGLZ(new GLGE.Object("axis_"+id+"_z"));

    this.getObjectGLX().setId(this.getIdType()+"_"+id+"_x");
    this.getObjectGLY().setId(this.getIdType()+"_"+id+"_y");
    this.getObjectGLZ().setId(this.getIdType()+"_"+id+"_z");

    this.getObjectGLX().setMesh(Cluster.getCircleMesh());
    this.getObjectGLY().setMesh(Cluster.getCircleMesh());
    this.getObjectGLZ().setMesh(Cluster.getCircleMesh());

    this.getObjectGLX().setDrawType(GLGE.DRAW_LINELOOPS);
    this.getObjectGLY().setDrawType(GLGE.DRAW_LINELOOPS);
    this.getObjectGLZ().setDrawType(GLGE.DRAW_LINELOOPS);

    this.setAxisX();
    this.setAxisY();
    this.setAxisZ();

    var materialX = new GLGE.Material();
    materialX.setColor(this.getColorX());
    this.getObjectGLX().setMaterial(materialX);

    var materialY = new GLGE.Material();
    materialY.setColor(this.getColorY());
    this.getObjectGLY().setMaterial(materialY);

    var materialZ = new GLGE.Material();
    materialZ.setColor(this.getColorZ());
    this.getObjectGLZ().setMaterial(materialZ);

    // finally, we add the object to the scene
    ClusterManager.getSingleton().getObjectScene().addObject(this.getObjectGLX());
    ClusterManager.getSingleton().getObjectScene().addObject(this.getObjectGLY());
    ClusterManager.getSingleton().getObjectScene().addObject(this.getObjectGLZ());

    // We add this object to allNodes that contains all the object
    Cluster.getAllAxisSphere()[id] = this;
    // And finally we set the position of all the nodes (because when one node
    // is added, all the positions of the nodes are updated)
    this.render();
};



/**
 * Destroy the object
 */
Cluster.AxisSphere.prototype.destroy = function(){
    delete Cluster.getAllAxisSphere()[this.getId()];

    ClusterManager.getSingleton().getObjectScene().removeChild(this.getObjectGLX());
    ClusterManager.getSingleton().getObjectScene().removeChild(this.getObjectGLY());
    ClusterManager.getSingleton().getObjectScene().removeChild(this.getObjectGLZ());

    delete this.getObjectGLX().getMaterial(0);
    delete this.getObjectGLX().getMesh(0);
    delete this.getObjectGLX();

    delete this.getObjectGLY().getMaterial(0);
    delete this.getObjectGLY().getMesh(0);
    delete this.getObjectGLY();

    delete this.getObjectGLZ().getMaterial(0);
    delete this.getObjectGLZ().getMesh(0);
    delete this.getObjectGLZ();

    delete this;
}

/**
 * Define the axis like axis X
 */
Cluster.AxisSphere.prototype.setAxisX = function(){
	this.getObjectGLX().setDRot(0.0, 0.0, 0.0);
	this.getObjectGLX().setRot(Math.PI/2, 0.0, 0.0);
}

/**
 * Define the axis like axis Y
 */
Cluster.AxisSphere.prototype.setAxisY = function(){
	this.getObjectGLY().setDRot(0.0, 0.0, 0.0);
	this.getObjectGLY().setRot(0.0, 0.0, 0.0);
}

/**
 * Define the axis like axis Z
 */
Cluster.AxisSphere.prototype.setAxisZ = function(){
	this.getObjectGLZ().setDRot(0.0, 0.0, 0.0);
	this.getObjectGLZ().setRot(0.0, 0.0, Math.PI/2);
}


/**
 * Function that set the position of the object in the scene, calculated from
 * the time that has been past, the duration of the movement and the last
 * position of the object
 */
Cluster.AxisSphere.prototype.render = function(){
	var radius = Cluster.getNodeRadius() / 2;

	this.getObjectGLX().setScale(radius, radius, radius);
	this.getObjectGLY().setScale(radius, radius, radius);
	this.getObjectGLZ().setScale(radius, radius, radius);

	var position = this.getPosition();
	this.getObjectGLX().setLoc(position[0], position[1], position[2]);
	this.getObjectGLY().setLoc(position[0], position[1], position[2]);
	this.getObjectGLZ().setLoc(position[0], position[1], position[2]);
};



// Getter - Setter

/**
 * Getter for the attribute "idType"
 * @return {Integer} Id Type value
 */
Cluster.AxisSphere.prototype.getIdType = function(){
	return this.idType;
};

/**
 * Getter for the attribute "id"
 * @return {Integer} Id value
 */
Cluster.AxisSphere.prototype.getId = function(){
	return this.id;
}
/**
 * Getter for the attribute "id"
 * @param {Integer} Id value
 */
Cluster.AxisSphere.prototype.setId = function(id){
	this.id = id;
}

/**
 * Getter for the attribute "position"
 * @return {Array} Position value
 */
Cluster.AxisSphere.prototype.getPosition = function(){
	return this.position;
}
/**
 * Getter for the attribute "position"
 * @param {Array} Position value
 */
Cluster.AxisSphere.prototype.setPosition = function(position){
	this.position = position;
}

/**
 * Getter for the attribute "colorX"
 * @return {String} ColorX value
 */
Cluster.AxisSphere.prototype.getColorX = function(){
	return this.colorX;
}
/**
 * Setter for the attribute "colorX"
 * @param {String} ColorX value
 */
Cluster.AxisSphere.prototype.setColorX = function(colorX){
	this.colorX = colorX;
}

/**
 * Getter for the attribute "colorY"
 * @return {String} ColorY value
 */
Cluster.AxisSphere.prototype.getColorY = function(){
	return this.colorY;
}
/**
 * Setter for the attribute "colorY"
 * @param {String} ColorY value
 */
Cluster.AxisSphere.prototype.setColorY = function(colorY){
	this.colorY = colorY;
}

/**
 * Getter for the attribute "colorZ"
 * @return {String} ColorZ value
 */
Cluster.AxisSphere.prototype.getColorZ = function(){
	return this.colorZ;
}
/**
 * Setter for the attribute "colorZ"
 * @param {String} ColorZ value
 */
Cluster.AxisSphere.prototype.setColorZ = function(colorZ){
	this.colorZ = colorZ;
}

/**
 * Getter for the attribute "objectGLX"
 * @return {GLGE.Object} ObjectGLX value
 */
Cluster.AxisSphere.prototype.getObjectGLX = function(){
	return this.objectGLX;
}
/**
 * Setter for the attribute "objectGLX"
 * @param {GLGE.Object} ObjectGLX value
 */
Cluster.AxisSphere.prototype.setObjectGLX = function(objectGLX){
	this.objectGLX = objectGLX;
}

/**
 * Getter for the attribute "objectGLY"
 * @return {GLGE.Object} ObjectGLY value
 */
Cluster.AxisSphere.prototype.getObjectGLY = function(){
	return this.objectGLY;
}
/**
 * Setter for the attribute "objectGLY"
 * @param {GLGE.Object} ObjectGLY value
 */
Cluster.AxisSphere.prototype.setObjectGLY = function(objectGLY){
	this.objectGLY = objectGLY;
}

/**
 * Getter for the attribute "objectGLZ"
 * @return {GLGE.Object} ObjectGLZ value
 */
Cluster.AxisSphere.prototype.getObjectGLZ = function(){
	return this.objectGLZ;
}
/**
 * Setter for the attribute "objectGLZ"
 * @param {GLGE.Object} ObjectGLZ value
 */
Cluster.AxisSphere.prototype.setObjectGLZ = function(objectGLZ){
	this.objectGLZ = objectGLZ;
}

var nbNodes = 0;
var nbProcesses = 0;
var nodes = new nodeList();
var processes = new processList();

function addNodeDebug(){
	var id = "Node"+nbNodes;
	nodes[id] = new Cluster.Node(id);
	document.getElementById("nodeSelector").appendChild(createOption(id));
	document.getElementById("from").appendChild(createOption(id));
	document.getElementById("to").appendChild(createOption(id));
	nbNodes++;
}

function addProcessDebug(){
	var nodeId = document.getElementById("nodeSelector").value;
	if(nodeId != ""){
		var id = "Process"+nbProcesses;
		processes[id] = new Cluster.Process(id);
		nodes[nodeId].addProcess(processes[id]);
		nbProcesses++;
	}
}

function migrateDebug(){
	var from = document.getElementById("from").value;
	var to = document.getElementById("to").value;
	var process = document.getElementById("process").value;
	if(from != "" && to != "" && process != ""){
		processes[process].migrateTo(nodes[to]);
	}
}

function createOption(value){
	var option = document.createElement("option");
	option.setAttribute("value",value);
	option.innerHTML = value;
	return option;

}

function populateProcesses(){
	var from = document.getElementById("from").value;
	if(from != ""){
		document.getElementById("process").innerHTML = "";
		for(var id in nodes[from].hisProcesses){
			document.getElementById("process").appendChild(createOption(id));
		}
	}
}

function mark(){
	var mark = new Cluster.Mark(node1,proc);
	mark.render();
	/*
	   new Cluster.Mark(node3,node1);
	   new Cluster.Mark(node4,node2);
	   new Cluster.Mark(node3,node0);
	   new Cluster.Mark(node4,node1);
	 */
}

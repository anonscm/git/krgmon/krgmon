function drawFilledPolygon(ctx, shape, style) {
    ctx.beginPath();
    ctx.fillStyle = style;
    ctx.moveTo(shape[0][0], shape[0][1]);

    for(shapeElement in shape) {
        if (shapeElement > 0) {
		ctx.lineTo(shape[shapeElement][0], shape[shapeElement][1]);
	}
    }

    ctx.lineTo(shape[0][0], shape[0][1]);
    ctx.fill();
};

function translateShape(shape, x, y) {
    var rv = [];
    for(var i = 0; i < shape.length; i++) {
	var r = [ shape[i][0] + x, shape[i][1] + y ];
        rv.push(r);
    }
    return rv;
};

function rotateShape(shape, ang) {
    var rv = [];
    for(var i = 0; i < shape.length; i++) {
	var r = rotatePoint(ang, shape[i][0], shape[i][1]);
        rv.push(r);
    }
    return rv;
};

function rotatePoint(ang, x, y) {
    return [
        (x * Math.cos(ang)) - (y * Math.sin(ang)),
        (x * Math.sin(ang)) + (y * Math.cos(ang))
    ];
};

function drawArrow(ctx, x1, y1, x2, y2, style, width, nodeWidth) {
    var ang = Math.atan2(y2-y1,x2-x1);
    var arrowLength = 4;
    var arrowStart = width-nodeWidth;
    var arrowWidth = 2;
    var arrow = [
	    [ arrowStart, 0 ],
	    [ arrowStart - width*arrowLength, width*(-arrowWidth) ],
	    [ arrowStart - width*arrowLength, width*(arrowWidth) ]
	];

    drawFilledPolygon(ctx, translateShape(rotateShape(arrow, ang), x2, y2), style);
}

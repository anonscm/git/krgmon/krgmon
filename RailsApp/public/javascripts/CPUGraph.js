function CPUGraph(name, retention)
{
	this.name = name;
	this.rb = new RingBuffer("CPUGraph", retention);
	this.init();
}

CPUGraph.prototype.init = function()
{
	this.pos = 0;
}

CPUGraph.prototype.push_new_value = function(value)
{
	var Datas = new Array();

	for (var i = 0; i < value.cpustats.length; i++) {
		var cpust = value.cpustats[i];
		var key = cpust.cpuid;
		var val = cpust.prcent;

		this.rb.push(key, val);
		
		var o = {
			data: this.rb.toIndexedArray(key),
			label: "CPU " + cpust.cpuid.replace("cpu", ""),
			lines: { show: true, fill: false }
		};

		Datas.push(o);
	}

	this.chart = new Proto.Chart($(this.name), Datas,
	{
		//since line chart is the default charting view
		//we do not need to pass any specific options for it.
		xaxis: {
			min: 0,
			max: this.rb.getRetention(),
			tickSize: 5
		},
		lines: {
			show: true,
			fill: false
		},
		legend: {
			show: true
		},
		grid: {
			coloredAreas: [{
				x1: (this.pos % this.rb.getRetention()) - 0.1,
				x2: (this.pos % this.rb.getRetention()) + 0.1
			}],
			coloredAreasColor: "#ff0000"
		},
	});

	this.pos++;
}

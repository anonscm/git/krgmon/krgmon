function Process(pid, migrationGraph)
{
	// Processus identifier
	this.pid = pid;
	// Local identifier
	this.migraph = migrationGraph;
	this.localId = migrationGraph.processCounter++;

	// The node on which this process is currently running
	this.currentNode = undefined;
	// The date when we start to run on the current node
	this.currentRun_start = undefined;
	// The identifier of the current run
	this.svgCurrentRun_domId = undefined;

	this.svgTitle_domId = migrationGraph.name + '_process' + this.pid + 'Title';
	this.svgTitleBG_domId = migrationGraph.name + '_process' + this.pid + 'TitleBG';
	this.svgLine_domId = migrationGraph.name + '_process' + this.pid + 'Line';

	// Name of the process
	var svgProcessTitle = newSvgElement('text');
	svgProcessTitle.setAttribute('id', this.svgTitle_domId);
	svgProcessTitle.setAttribute('transform', 'translate(0, ' + (this.localId+0.5) * migrationGraph.processGroupHeight +')');
	svgProcessTitle.setAttribute('onmouseover', 'return overlib(procGrabInfos(' + this.pid + '), CAPTION, "Informations about process #' + this.pid + ' ");');
	svgProcessTitle.appendChild(document.createTextNode('Process #' + this.pid));
	migrationGraph.svgProcessTitlesContainer().appendChild(svgProcessTitle);

	// Hack to get a background on the text
	var svgProcessTitleBBox = svgProcessTitle.getBBox();
	var svgProcessTitleRect = newSvgElement('rect');
	svgProcessTitleRect.setAttribute('id', this.svgTitleBG_domId);
	svgProcessTitleRect.setAttribute('x', svgProcessTitleBBox.x - 4);
	svgProcessTitleRect.setAttribute('y', svgProcessTitleBBox.y - 4);
	svgProcessTitleRect.setAttribute('height', svgProcessTitleBBox.height + 8);
	svgProcessTitleRect.setAttribute('width', svgProcessTitleBBox.width + 8);
	svgProcessTitleRect.setAttribute('fill', '#ffffff');
	svgProcessTitleRect.setAttribute('opacity', '0.9');
	svgProcessTitleRect.setAttribute('transform', 'translate(0, ' + (this.localId+0.5) * migrationGraph.processGroupHeight +')');
	migrationGraph.svgProcessTitlesContainer().insertBefore(svgProcessTitleRect, svgProcessTitle);

	// A line !
	var svgLine = newSvgElement('line');
	svgLine.setAttribute('id', this.svgLine_domId);
	svgLine.setAttribute('x1', '0px');
	svgLine.setAttribute('y1', (this.localId+1)*migrationGraph.processGroupHeight+'px');
	svgLine.setAttribute('x2', migrationGraph.width+'px');
	svgLine.setAttribute('y2', (this.localId+1)*migrationGraph.processGroupHeight+'px');
	svgLine.setAttribute('stroke-width', '1');
	svgLine.setAttribute('stroke', '#000000');
	svgLine.setAttribute('shape-rendering', 'crispEdges');
	migrationGraph.svgProcessTitlesContainer().appendChild(svgLine);

	// Group in which we will draw each migrations
	this.svgGroup_domId = migrationGraph.name + '_process' + this.pid + 'Group';
	var svgProcessGroup = newSvgElement('g');
	svgProcessGroup.setAttribute('id', this.svgGroup_domId);
	svgProcessGroup.setAttribute('transform', 'translate(0, ' + this.localId * migrationGraph.processGroupHeight +')');
	migrationGraph.svgProcessContainer().appendChild(svgProcessGroup);
}

// Accessors to the group and the current run
Process.prototype.svgGroup = function() { return $(this.svgGroup_domId); }
Process.prototype.svgCurrentRun = function() { return $(this.svgCurrentRun_domId); }

// Method to put a process on a node
// Set the current node, draw the current run
Process.prototype.runOn = function(node, fromDate_rel, migrationGraph)
{
	this.currentNode = node;
	this.currentRun_start = fromDate_rel;

	this.svgCurrentRun_domId = migrationGraph.name + '_migration' + (migrationGraph.migrationCounter++);

	var procStatus;
	var currentSvgRun = newSvgElement('rect');
	currentSvgRun.setAttribute('id', this.svgCurrentRun_domId);
	currentSvgRun.setAttribute('x', this.currentRun_start + 'px');
	currentSvgRun.setAttribute('y', migrationGraph.processGroupOffset + 'px');
	currentSvgRun.setAttribute('width', '0px');
	currentSvgRun.setAttribute('height', migrationGraph.processHeight + 'px');
	currentSvgRun.setAttribute('stroke-width', '1px');
	currentSvgRun.setAttribute('stroke', '#000000');
	currentSvgRun.setAttribute('fill', this.currentNode.color);
	if (node.nodeId > 0) {
		procStatus = "Status: running.<br />Node: " + node.nodeId;
	} else {
		procStatus = "Status: dead";
	}
	currentSvgRun.setAttribute('onmouseover', 'return overlib("' + procStatus + '", CAPTION, "Process #' + this.pid + '");');
	currentSvgRun.setAttribute('onmouseout', 'return nd();');
	currentSvgRun.setAttribute('shape-rendering', 'crispEdges');

	this.svgGroup().appendChild(currentSvgRun);
}

// Method to remove a process from a node
// Unset the current node and the current run
Process.prototype.runOff = function(atDate_rel)
{
	this.updateStillRunning(atDate_rel);
	this.currentNode = undefined;
	this.currentRun_start = undefined;
	this.svgCurrentRun_domId = undefined;
}

// Method to update the process if it's still running on the same node
Process.prototype.updateStillRunning = function(atDate_rel)
{
	if(this.currentNode != undefined) {
		if (this.currentNode.nodeId == 0) {
			if (this.migraph.showdead == false) {
				document.getElementById(this.svgGroup_domId).setAttribute('visibility', 'hidden');
				document.getElementById(this.svgLine_domId).setAttribute('visibility', 'hidden');
				document.getElementById(this.svgTitleBG_domId).setAttribute('visibility', 'hidden');
				document.getElementById(this.svgTitle_domId).setAttribute('visibility', 'hidden');
			} else {
				document.getElementById(this.svgGroup_domId).setAttribute('visibility', 'visible');
				document.getElementById(this.svgLine_domId).setAttribute('visibility', 'visible');
				document.getElementById(this.svgTitleBG_domId).setAttribute('visibility', 'visible');
				document.getElementById(this.svgTitle_domId).setAttribute('visibility', 'visible');
			}
		}

		this.svgCurrentRun().setAttribute('width', atDate_rel - this.currentRun_start);
	}
}

//Process.prototype.grabInfos = function()
function procGrabInfos(procpid)
{
	var url = "ajax_getProcInfos";
	var result = "";
	var treat = function(transport) {
			var obj = transport.responseText.evalJSON(true);
			for (var i = 0; i < obj.procinfos.length; i++) {
				var elem = obj.procinfos[i];
				result += elem.name + ": '" + elem.value + "' <br />";
			}
		};

	new Ajax.Request(url, {
		method: 'get',
		asynchronous: false,
		parameters: {
			pid: procpid,
		},
		onSuccess: treat
	});

	url = "ajax_getProcLoad";
	new Ajax.Request(url, {
		method: 'get',
		asynchronous: false,
		parameters: {
			pid: procpid,
		},
		onSuccess: treat
	});

	return result;
}

/**
 * RingBuffer in JavaScript :)
 */

/* Main object */
function RingBuffer(name, retention, sizeFactor)
{
	this.init();
	
	retention	= typeof(retention) != 'undefined' ? retention : 60;
	sizeFactor	= typeof(sizeFactor) != 'undefined' ? sizeFactor : 1;

	this.setName(name);
	this.setRetention(retention);
	this.setSizeFactor(sizeFactor);
}

RingBuffer.prototype.init = function()
{
	this.dataseries = new Array();
	this.ring = new Array();
	this.ringposW = new Array();
	this.ringposR = new Array();
}

/* Manipulating the RingBuffer */

RingBuffer.prototype.createDataSerie = function(dataSerieName)
{
	if (!this.dataSerieExists(dataSerieName)) {
		this.ring[dataSerieName]	= new Array(this.getRetention());
		this.ringposW[dataSerieName]	= 0;
		this.ringposR[dataSerieName]	= 0;

		for (var i = 0; i < this.getRetention(); i++) {
			this.ring[dataSerieName][i] = 0;
		}
	}
}

RingBuffer.prototype.deleteDataSerie = function(dataSerieName)
{
	if (this.dataSerieExists(dataSerieName)) {
		delete this.ring[dataSerieName];
		delete this.ringposW[dataSerieName];
		delete this.ringposR[dataSerieName];
		delete this.dataseries[dataSerieName];
	}
}

RingBuffer.prototype.dataSerieExists = function(dataSerieName)
{
	if (this.dataseries[dataSerieName] == dataSerieName) {
		return true;
	} else {
		return false;
	}
}

RingBuffer.prototype.pushDataSerieValue = function(dataSerieName, dataSerieValue)
{
	if (!this.dataSerieExists(dataSerieName)) {
		this.createDataSerie(dataSerieName);
	}

	this.ring[dataSerieName][this.ringposW[dataSerieName]] = dataSerieValue;
	this.dataseries[dataSerieName] = dataSerieName;

	this.ringposW[dataSerieName] = (this.ringposW[dataSerieName] + 1) % this.getRetention();
	return true;
}

RingBuffer.prototype.popDataSerieValue = function(dataSerieName)
{
	var retval = undefined;

	if (this.dataSerieExists(dataSerieName)) {
		retval = (this.ring[dataSerieName][this.ringposR[dataSerieName]]) / this.getSizeFactor();
	}

	this.ringposR[dataSerieName] = (this.ringposR[dataSerieName] + 1) % this.getRetention();
	return retval;
}

RingBuffer.prototype.toIndexedArray = function(dataSerieName, factorConvert)
{
	var dataSet = [];

	if (factorConvert == undefined) {
		factorConvert = false;
	}

	if (this.dataSerieExists(dataSerieName)) {
		for (var i = 0; i < this.getRetention(); i++)
		{
			var el = this.popDataSerieValue(dataSerieName);
			if (factorConvert) {
				dataSet.push([i, el * this.getSizeFactor()]);
			} else {
				dataSet.push([i, el]);
		}
		}
	}

	return dataSet;
}

/* Accessors */

RingBuffer.prototype.setName = function(n)
{
	this.name = n;
}

RingBuffer.prototype.getName = function()
{
	return this.name;
}

RingBuffer.prototype.setRetention = function(n)
{
	this.retention = n;
}

RingBuffer.prototype.getRetention = function()
{
	return this.retention;
}

RingBuffer.prototype.setSizeFactor = function(n)
{
	this.sizeFactor = n;
}

RingBuffer.prototype.getSizeFactor = function()
{
	return this.sizeFactor;
}

RingBuffer.prototype.push = function(n, v)
{
	return this.pushDataSerieValue(n, v);
}

RingBuffer.prototype.pop = function(n)
{
	return this.popDataSerieValue(n);
}

/****** Tests ******/
function RingBufferLog(target, message)
{
	if (target.innerHTML.length > 0) {
		target.innerHTML += '<br />' + message;
	} else {
		target.innerHTML = message;
	}
}

function RingBufferTests(elem)
{
	var nbElemsS1 = 5; // Serie1 should match the size of the buffer
	var nbElemsS2 = 24;// Serie2 should exceed the size of the buffer
	var log = document.getElementById(elem);
	var RB = new RingBuffer("CPU", 5, 2);

	if (!log) {
		alert("Can't find logging target!");
	} else {
		RingBufferLog(log, "Ready to process.");
	}

	if (RB.getName() == "CPU") {
		RingBufferLog(log, "Name OK.");
	} else {
		RingBufferLog(log, "Name not OK.");
		return false;
	}

	if (RB.getRetention() == 5) {
		RingBufferLog(log, "Retention OK.");
	} else {
		RingBufferLog(log, "Retention not OK.");
		return false;
	}

	if (RB.getSizeFactor() == 2) {
		RingBufferLog(log, "SizeFactor OK.");
	} else {
		RingBufferLog(log, "SizeFactor not OK.");
		return false;
	}

	var valuesS1 = [];
	var valuesS1chk = [];
	var valuesS1good = [];
	for (var i = 0; i < nbElemsS1; i++)
	{
		valuesS1[i] = Math.floor(Math.random()*20)-10;
	}
	valuesS1good = valuesS1;
	RingBufferLog(log, "Serie1 = " + valuesS1);
	RingBufferLog(log, "Serie1 Good = " + valuesS1good);

	var valuesS2 = [];
	var valuesS2chk = [];
	var valuesS2good = [];
	for (var i = 0; i < nbElemsS2; i++)
	{
		valuesS2[i] = Math.floor(Math.random()*20)-10;
	}
	var j = nbElemsS2 - RB.getRetention() + 1;
	for (var i = 0; i < RB.getRetention(); i++)
	{
		valuesS2good[i] = valuesS2[j];
		j++;
		if (j >= nbElemsS2) {
			j = nbElemsS2 - RB.getRetention();
		}
	}
	RingBufferLog(log, "Serie2 = " + valuesS2);
	RingBufferLog(log, "Serie2 Good = " + valuesS2good);

	RingBufferLog(log, "Starting real tests ...");

	RB.createDataSerie("CPU1");
	RB.createDataSerie("CPU2");
	for (var i = 0; i < nbElemsS1; i++)
	{
		RB.push("CPU1", valuesS1[i]);
	}
	for (var i = 0; i < RB.getRetention(); i++)
	{
		valuesS1chk[i] = RB.pop("CPU1") * RB.getSizeFactor();
	}
	RingBufferLog(log, "Serie1 Check = " + valuesS1chk);
	for (var i = 0; i < nbElemsS1; i++) 
	{
		if (valuesS1chk[i] != valuesS1good[i % RB.getRetention()]) {
			RingBufferLog(log, "Serie1 Check not OK.");
			return false;
		}
	}
	RingBufferLog(log, "Serie1 Check OK.");

	for (var i = 0; i < nbElemsS2; i++)
	{
		RB.push("CPU2", valuesS2[i]);
	}
	for (var i = 0; i < RB.getRetention(); i++)
	{
		valuesS2chk[i] = RB.pop("CPU2") * RB.getSizeFactor();
	}
	RingBufferLog(log, "Serie2 Check = " + valuesS2chk);
	for (var i = 0; i < RB.getRetention(); i++) 
	{
		if (valuesS2chk[i] != valuesS2good[i % RB.getRetention()]) {
			RingBufferLog(log, "Serie2 Check not OK at " + i + ".");
			RingBufferLog(log, "Check[" + i + "] != Good[" + i % RB.getRetention() + "]");
			return false;
		}
	}
	RingBufferLog(log, "Serie2 Check OK.");

	RingBufferLog(log, "Dumping Serie1:");
	var s1 = RB.toIndexedArray("CPU1");
	var s1f = RB.toIndexedArray("CPU1", true);
	RingBufferLog(log, s1);
	RingBufferLog(log, s1f);

	RingBufferLog(log, "Dumping Serie2:");
	var s2 = RB.toIndexedArray("CPU2");
	var s2f = RB.toIndexedArray("CPU2", true);
	RingBufferLog(log, s2);
	RingBufferLog(log, s2f);

	RingBufferLog(log, "Deleting Serie1 ...");
	RB.deleteDataSerie("CPU1");
	if (!RB.dataSerieExists("CPU1") && RB.dataSerieExists("CPU2")) {
		RingBufferLog(log, "Deleting Serie1 ... OK.");
	} else {
		RingBufferLog(log, "Deleting Serie1 ... not OK.");
	}

	RingBufferLog(log, "Deleting Serie2 ...");
	RB.deleteDataSerie("CPU2");
	if (!RB.dataSerieExists("CPU1") && !RB.dataSerieExists("CPU2")) {
		RingBufferLog(log, "Deleting Serie2 ... OK.");
	} else {
		RingBufferLog(log, "Deleting Serie2 ... not OK.");
	}

	return true;
}

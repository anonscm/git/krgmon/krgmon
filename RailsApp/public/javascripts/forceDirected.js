function forceDirectedMigrationGraph(name, target) {
	this.name = name
	this.cont = document.getElementById(this.name);
	this.logtarget = document.getElementById(target);
	this.maxNodes = 10;
	this.baseNode = 10;
	this.maxLog = 10;
	this.Nodes = new Array();
	this.lastMigId = 0;
	this.totalmigs = 0;
	this.minmigs = 0;

	this.init();
//	this.generateMigrations();
} 

forceDirectedMigrationGraph.prototype.resize = function() {
	this.layout.view.skewBase = this.layout.container.scrollWidth / 6;
	this.layout.setSize();
}

forceDirectedMigrationGraph.prototype.init = function() {
	this.layout = mkGraph(this.cont);
	this.layout.migrations = new Array();
}

forceDirectedMigrationGraph.prototype.addIfNone = function(nodeid) {
	if (this.Nodes[nodeid] == undefined) {
		var node = new DataGraphNode();
		node.label = 'N' + nodeid;
		this.layout.addParticle(node);
		this.layout.newDataGraphNode(node);
		this.Nodes[nodeid] = node;
	}
}

forceDirectedMigrationGraph.prototype.appendLog = function(id, s, e) {
	var log = this.logtarget;
	var curData = log.innerHTML.split("\n");
	var newData = new Array();
	for (var i = 0; i < curData.length; i++) {
		if (i < this.maxLog) {
			newData[i] = curData[i];
		} else {
			break;
		}
	}
	log.innerHTML = "Migration #" + id + " (" + s + " -> " + e + ")<br />\n" + newData.join("\n");
}

forceDirectedMigrationGraph.prototype.createOrUpdateMigration = function(id, startnode, endnode) {
	var nmigs = 1;
	this.lastMigId = id;
	this.addIfNone(startnode);
	this.addIfNone(endnode);

	if (nmigs <= this.minmigs)
		this.minmigs = minmigs;

	this.totalmigs += nmigs;

	this.layout.migrations[this.Nodes[startnode].particle.id + "," + this.Nodes[endnode].particle.id] = {
		nbmigs: nmigs,
		totalmigs: this.totalmigs,
		minmigs: this.minmigs
	};

	this.layout.newDataGraphEdge(this.Nodes[startnode], this.Nodes[endnode]);
	this.layout.model.update();

	this.appendLog(id, startnode, endnode);
}

forceDirectedMigrationGraph.prototype.generateMigrations = function() {
	window.setInterval(function (obj) {
		var nodeA = obj.baseNode + Math.floor(Math.random()*obj.maxNodes);
		var nodeB = obj.baseNode + Math.floor(Math.random()*obj.maxNodes);

		if (nodeA != nodeB) {
			obj.createOrUpdateMigration(obj.lastMigId+1, nodeA, nodeB);
		}
	}, 10000, this)
}

function mkGraph(container) {
	var renderNodes = "canvas";
	var renderEdges = "canvas";
	var layout;

	layout = new ForceDirectedLayout( container,
		{	skew: true,
			useCanvas: true,
			useVector: true,
			edgeRenderer: renderEdges,
			nodeSize: 24,
			dampingIncValue: 1.05,
			springIncValue: 1.05,
			massIncValue: 1.05,
			oriented: true,

			dampingIncrement: function(curValue, migs) {
				var nbMigs = migs ? migs['nbmigs'] : 1;
				var totalMigs = migs ? migs['totalmigs'] : 1;
				var minMigs = migs ? migs['minmigs'] : 1;
				var v1 = (curValue * nbMigs * this.dampingIncValue) / (totalMigs);
				var v2 = minMigs / totalMigs;

				if (v1 > 0 && v2 > 0) {
					var lv1 = Math.log(v1);
					var lv2 = Math.log(v2);
					return 1 + (lv1 - lv2)/Math.log(10);
				} else {
					return 1;
				}
			},
			
			springIncrement: function(curValue, migs) {
				var nbMigs = migs ? migs['nbmigs'] : 1;
				var totalMigs = migs ? migs['totalmigs'] : 1;
				var minMigs = migs ? migs['minmigs'] : 1;

				var v1 = (curValue * nbMigs * this.springIncValue) / (totalMigs);
				var v2 = minMigs / totalMigs;

				if (v1 > 0 && v2 > 0) {
					var lv1 = Math.log(v1);
					var lv2 = Math.log(v2);
					return 1 + (lv1 - lv2)/Math.log(10);
				} else {
					return 1;
				}
			},
			
			massIncrement: function(curValue, migs) {
				var nbMigs = migs ? migs['nbmigs'] : 1;
				var totalMigs = migs ? migs['totalmigs'] : 1;
				var minMigs = migs ? migs['minmigs'] : 1;

				var v1 = (curValue * nbMigs * this.massIncValue) / (totalMigs);
				var v2 = minMigs / totalMigs;

				if (v1 > 0 && v2 > 0) {
					var lv1 = Math.log(v1);
					var lv2 = Math.log(v2);
					return 1 + (lv1 - lv2)/Math.log(10);
				} else {
					return 1;
				}
			},
			
			strokeIncrement: function(curValue, migs) {
				var nbMigs = migs ? migs['nbmigs'] : 1;
				var totalMigs = migs ? migs['totalmigs'] : 1;
				var minMigs = migs ? migs['minmigs'] : 1;
				var res = "2px";

				var result = curValue.match(/(\d+)(\w+)/i);
				if (result) {
					var v1 = (parseInt(result[1])*nbMigs) / (totalMigs);
					var v2 = minMigs / totalMigs;

					if (v1 > 0 && v2 > 0) {
						var lv1 = Math.log(v1);
						var lv2 = Math.log(v2);
						res = (1 + (lv1 - lv2)) + result[2];
					}
				} 
				return res;
			},
		});
	
	layout.view.skewBase = layout.container.scrollWidth / 6;
	layout.setSize();

	layout.config._default = {
		model: function( dataNode ) {
			return {
				mass: 1,
				color: '#0000B0',
			}
		},
		view: function( dataNode, modelNode ) {
			if(renderNodes=="canvas"  && layout.view.supportCanvas) {
				return( function(particle) {
					this.node_twod.strokeStyle = dataNode.color;
					this.node_twod.fillStyle = dataNode.color;
					this.node_twod.beginPath();
					this.node_twod.arc(
						(particle.positionX*this.skewX) + this.centerX,
						(particle.positionY*this.skewY) + this.centerY,
						this.properties.nodeSize, 0,
						Math.PI*2,true); // Outer circle

					this.node_twod.stroke();
					this.node_twod.fill();

					this.node_twod.font = '1em solid';
					this.node_twod.fillStyle = 'white';
					this.node_twod.textAlign = 'center';
					this.node_twod.textBaseLine = 'bottom';
					this.node_twod.fillText(
						dataNode.label,
						(particle.positionX*this.skewX) + this.centerX,
						(particle.positionY*this.skewY) + this.centerY
					);
				} );
			} else if(renderNodes=="vector" && layout.view.supportVector) {
				var nodeElement = document.createElementNS("http://www.w3.org/2000/svg", "circle");
				nodeElement.setAttribute('stroke', '#888888');
				nodeElement.setAttribute('stroke-width', '.25px');
				nodeElement.setAttribute('fill', dataNode.color);
				nodeElement.setAttribute('r', 6 + 'px');
				nodeElement.onmousedown =  new EventHandler( layout, layout.handleMouseDownEvent, modelNode.id )
				return nodeElement;						
			} else {
				var nodeElement = document.createElement( 'div' );
				nodeElement.style.position = "absolute";
				nodeElement.style.width = "12px";
				nodeElement.style.height = "12px";
				
				var color = dataNode.color.replace( "#", "" );
				nodeElement.style.backgroundImage = "url(http://kylescholz.com/cgi-bin/bubble.pl?title=&r=12&pt=8&b=" + color + "&c=" + color + ")";
				nodeElement.innerHTML = '<img width="1" height="1">';
				nodeElement.onmousedown =  new EventHandler( layout, layout.handleMouseDownEvent, modelNode.id )
				return nodeElement;
			}
		}
	}

	layout.forces.spring._default = function( nodeA, nodeB, isParentChild ) {
		if (isParentChild) {
			return {
				springConstant: 0.2,
				dampingConstant: 0.2,
				restLength: 32
			}
		} else {
			return {
				springConstant: 0.2,
				dampingConstant: 0.2,
				restLength: 32
			}
		}
	}
	
	layout.forces.spring['A'] = {};
	layout.forces.spring['A']['B'] = function( nodeA, nodeB, isParentChild ) {
		return {
			springConstant: 0.1,
			dampingConstant: 0.1,
			restLength: 32
		}
	}

	layout.forces.magnet = function() {
		return {
			magnetConstant: -3000,
			minimumDistance: 32 
		}
	}
	
	// 
	layout.viewEdgeBuilder = function( dataNodeSrc, dataNodeDest ) {
		return {
			'html_pixels': 5,
			'stroke': calcColor( dataNodeSrc, dataNodeDest ),
			'stroke-width': '2px',
			'stroke-dasharray': '2,4',
		}
	}

	function calcColor (src, dest) {
		var newCol = src.label + dest.label;
		var ar = newCol.split("");
		var chrs = [];
		var color = 'black';

		for (var i = 0; i < ar.length; i++) {
			chrs.push(ar[i].charCodeAt(0));
		}

		var md5 = hex_md5(chrs.join("")).split("");
		color = '#' + md5[0] + md5[3] + md5[6] + md5[9] + md5[12] + md5[15];

		return color;
	}

	var buildTimer = new Timer( 0 );
	buildTimer.subscribe( layout );
	buildTimer.start();

	return layout;
}

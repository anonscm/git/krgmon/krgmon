function MemoryGraph(name, retention)
{
	this.name = name;
	this.rb = new RingBuffer("MemoryGraph", retention, 1024*1024);
	this.init();
}

MemoryGraph.prototype.init = function()
{
	this.pos = 0;
}

MemoryGraph.prototype.push_new_value = function(value)
{
	var Datas = new Array();

	for (var key in value) {
		this.rb.push(key, value[key]);
		
		var o = {
			data: this.rb.toIndexedArray(key),
			label: "Chart " + key,
			lines: {show: true, fill: false}
		};
		Datas.push(o);
	}

	this.chart = new Proto.Chart($(this.name), Datas,
	{
		//since line chart is the default charting view
		//we do not need to pass any specific options for it.
		xaxis: {
			min: 0,
			max: this.rb.getRetention(),
			tickSize: 5
		},
		lines: {
			show: true,
			fill: false
		},
		legend: {
			show: true
		},
		grid: {
			coloredAreas: [{
				x1: (this.pos % this.rb.getRetention()) - 0.1,
				x2: (this.pos % this.rb.getRetention()) + 0.1
			}],
			coloredAreasColor: "#ff0000"
		},
	});

	this.pos++;
}

function Node(nodeId, migrationGraph)
{
	this.nodeId = nodeId;
	this.localId = migrationGraph.nodeCounter++;
	if (this.nodeId == 0) {
		this.color = this.pitfallColor;
	} else {
		this.color = migrationGraph.nodeColors[this.localId % migrationGraph.nodeColors.length];
	}
}

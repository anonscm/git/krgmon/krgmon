var svgNS = 'http://www.w3.org/2000/svg'

function newSvgElement(type) {return document.createElementNS(svgNS, type); }

function MigrationGraph(name, width, height, startDate_abs, lastMigId)
{
	// The id of the container of the graph
	this.name = name;
	// Width of the graph
	this.width = width;
	// Height of the graph
	this.height = height;
	// The date at wich we start to track migrations
	this.startDate_abs = startDate_abs;
	this.currentDate_rel = 0;
	this.showdead = true;

	this.processGroupHeight = 20;
	this.processGroupOffset = 5;
	this.processHeight = this.processGroupHeight - this.processGroupOffset;

	this.processCounter = 0;
	this.processList = new Hash();

	this.nodeCounter = 0;
	this.nodeList = new Hash();

	this.migrationCounter = 0;

	this.lastMigId = lastMigId;

	this.vPos = 0;
	this.hPos = 0;

	this.pitfallColor = '#000000';
	this.nodeColors = [
		'#ff0000', '#00ff00', '#0000ff', '#ffff00', '#ff00ff', '#00ffff', 
		'#ff4040', '#40ff40', '#4040ff', '#ffff40', '#ff40ff', '#40ffff', 
		'#ff7373', '#73ff73', '#7373ff', '#ffff73', '#ff73ff', '#73ffff', 
		'#a60000', '#00a600', '#0000a6', '#a6a600', '#a600a6', '#00a6a6'
		];

	this.init();
}

MigrationGraph.prototype.dateAbsToRel = function(date_abs)
{
	return date_abs - this.startDate_abs;
}

MigrationGraph.prototype.init = function()
{
	// Identifiant du graphique SVg
	this.svgGraph_domId = this.name + '_svgGraph';

	// Graphique SVG
	var svgContainer = newSvgElement('svg');
	svgContainer.setAttribute('id', this.svgGraph_domId);
	svgContainer.setAttribute('xmlns', svgNS);
	svgContainer.setAttribute('version', '1.1');
	svgContainer.setAttribute('baseProfile', 'full');
	svgContainer.setAttribute('preserveAspectRatio', 'xMinYMin meet');
	svgContainer.setAttribute('width', this.width + 'px');
	svgContainer.setAttribute('height', this.height + 'px');
	svgContainer.setAttribute('style', 'border:1px solid black; display:block;');
	svgContainer.setAttribute('viewBox', -this.width + ' 0 ' + this.width + ' ' + this.height);
	this.container().appendChild(svgContainer);

	// Timeline
	this.svgTimelineGroup_domId = this.name + '_Timeline';
	var svgTimelineGroup = document.createElementNS(svgNS,'g');
	svgTimelineGroup.setAttribute('id', this.svgTimelineGroup_domId);
	svgTimelineGroup.setAttribute('transform', 'translate('+ (-this.width) + ' 0)');
	for(var i = this.width; i > 0; i -= 60)
	{
		var line = newSvgElement('line');
		line.setAttribute('id', this.name + '_timeline' + i);
		line.setAttribute('x1', i);
		line.setAttribute('y1', '0');
		line.setAttribute('x2', i);
		line.setAttribute('y2', this.height);
		line.setAttribute('stroke', '#000000');
		line.setAttribute('stroke-width', '1');
		line.setAttribute('shape-rendering', 'crispEdges');
		svgTimelineGroup.appendChild(line);
	}
	this.svgGraph().appendChild(svgTimelineGroup);

	// Zone de dessin
	this.svgProcessDrawingArea_domId = this.name + '_processDrawingArea';
	var svgProcessDrawingArea = newSvgElement('g');
	svgProcessDrawingArea.setAttribute('id', this.svgProcessDrawingArea_domId);
	this.svgGraph().appendChild(svgProcessDrawingArea);

	// Conteneur de processus
	this.svgProcessContainer_domId = this.name + '_processContainer';
	var processContainer = document.createElementNS(svgNS,'g');
	processContainer.setAttribute('id', this.svgProcessContainer_domId);
	this.svgProcessDrawingArea().appendChild(processContainer);
	
	// Conteneur de titre
	this.svgProcessTitlesContainer_domId = this.name + '_processTitlesContainer';
	var processTitlesContainer = newSvgElement('g');
	processTitlesContainer.setAttribute('id', this.svgProcessTitlesContainer_domId);
	processTitlesContainer.setAttribute('transform', 'translate('+ (-this.width) + ' 0)');
	this.svgProcessDrawingArea().appendChild(processTitlesContainer);

	// Boutons d'action
	var buttonDown = document.createElement('button');
	buttonDown.setAttribute('type', 'button');
	buttonDown.setAttribute('onclick', 'javascript: ' + this.name + '.moveViewDown();');
	buttonDown.innerHTML = 'Move down';
	this.container().appendChild(buttonDown);

	var buttonReset = document.createElement('button');
	buttonReset.setAttribute('type', 'button');
	buttonReset.setAttribute('onclick', 'javascript: ' + this.name + '.resetViewVertically();');
	buttonReset.innerHTML = 'Go to first process';
	this.container().appendChild(buttonReset);

	var buttonUp = document.createElement('button');
	buttonUp.setAttribute('type', 'button');
	buttonUp.setAttribute('onclick', 'javascript: ' + this.name + '.moveViewUp();');
	buttonUp.innerHTML = 'Move up';
	this.container().appendChild(buttonUp);

	this.container().appendChild(document.createElement('br'));

	var buttonLeft = document.createElement('button');
	buttonLeft.setAttribute('type', 'button');
	buttonLeft.setAttribute('onclick', 'javascript: ' + this.name + '.moveViewLeft();');
	buttonLeft.innerHTML = '&lt; Move left';
	this.container().appendChild(buttonLeft);

	var buttonAuto = document.createElement('button');
	buttonAuto.setAttribute('type', 'button');
	buttonAuto.setAttribute('onclick', 'javascript: ' + this.name + '.autoMoveView();');
	buttonAuto.innerHTML = 'Auto-update';
	this.container().appendChild(buttonAuto);

	var buttonRight = document.createElement('button');
	buttonRight.setAttribute('type', 'button');
	buttonRight.setAttribute('onclick', 'javascript: ' + this.name + '.moveViewRight();');
	buttonRight.innerHTML = 'Move right &gt;';
	this.container().appendChild(buttonRight);

	this.container().appendChild(document.createElement('br'));

	var buttonSwitchDead = document.createElement('button');
	buttonSwitchDead.setAttribute('type', 'button');
	buttonSwitchDead.setAttribute('onclick', 'javascript: ' + this.name + '.switchDead(this);');
	buttonSwitchDead.innerHTML = 'Hide dead processes';
	this.container().appendChild(buttonSwitchDead);

	var buttonReset = document.createElement('button');
	buttonReset.setAttribute('type', 'button');
	buttonReset.setAttribute('onclick', 'javascript: ' + this.name + '.resetViewFirstNonDead();');
	buttonReset.innerHTML = 'Go to first alive process';
	this.container().appendChild(buttonReset);

	/*
	var buttonReset = document.createElement('button');
	buttonReset.setAttribute('type', 'button');
	buttonReset.setAttribute('onclick', 'javascript: ' + this.name + '.showVerticalPosition();');
	buttonReset.innerHTML = 'Show vertical position';
	this.container().appendChild(buttonReset);
	*/
}

// Accesseurs aux objets du DOM
MigrationGraph.prototype.container = function() { return $(this.name); }
MigrationGraph.prototype.svgGraph = function() { return $(this.svgGraph_domId); }
MigrationGraph.prototype.svgProcessDrawingArea = function() { return $(this.svgProcessDrawingArea_domId); }
MigrationGraph.prototype.svgProcessContainer = function() { return $(this.svgProcessContainer_domId); }
MigrationGraph.prototype.svgProcessTitlesContainer = function() { return $(this.svgProcessTitlesContainer_domId); }
MigrationGraph.prototype.svgTimelineGroup = function() { return $(this.svgTimelineGroup_domId); }

MigrationGraph.prototype.addProcess = function(pid)
{
	var p = new Process(pid, this);
	this.processList.set(pid, p)
	return p;
}

MigrationGraph.prototype.getOrCreateNode = function(nodeId)
{
	var n = this.nodeList.get(nodeId);
	if(n == undefined) {
		n = this.addNode(nodeId);
	}
	return n;
}

// Method used by the server tu notify the graph of a new migration
MigrationGraph.prototype.migrate = function(migrationId, pid, nodeIdFrom, nodeIdTo, migStart_abs, migEnd_abs)
{
	this.lastMigId = migrationId;
	var migStart_rel = this.dateAbsToRel(migStart_abs);
	var migEnd_rel = this.dateAbsToRel(migEnd_abs);

	var p = this.processList.get(pid);

	var nodeFrom = undefined;
	var nodeTo = undefined;

	if(nodeIdFrom >= 0)
	{
		nodeFrom = this.getOrCreateNode(nodeIdFrom);
	}
	if(nodeIdTo >= 0)
	{
		nodeTo = this.getOrCreateNode(nodeIdTo);
	}

	if(p == undefined) {
		// New process
		p = this.addProcess(pid);
		p.runOn(nodeTo, migEnd_rel, this);
	} else {
		// Known process
		// We stop it on the current node
		p.runOff(migStart_rel);
		// Then, we put it on the new one
		if(nodeTo != undefined)
		{
			p.runOn(nodeTo, migEnd_rel, this);
		}
	}

}

MigrationGraph.prototype.findFirstNonDead = function()
{
	/**
	  * Processes are added by migrations: the first non dead 
	  * visible will be the last added, that is the one which
	  * was the last to be started/migrated.
	  **/
	var firstNonDead = undefined;
	var current = undefined;
	var firstNonDeadDate = 0;
	var i = 0;
	var keys = this.processList.keys().sort();

	/* First, find the min value of firstNonDeadDate */
	for (var key in keys) {
		if (this.processList.get(keys[key]) != undefined && this.processList.get(keys[key]).currentNode.nodeId == 0) {
			current = this.processList.get(keys[key]);
			if (current.currentRun_start < firstNonDeadDate) {
				firstNonDeadDate = current.currentRun_start;
			}
			i++;
		}
	}

	/*  Then find the according process. */
	for (var key in keys) {
		if (this.processList.get(keys[key]) != undefined && this.processList.get(keys[key]).currentNode.nodeId > 0) {
			current = this.processList.get(keys[key]);
			if (current.currentRun_start > firstNonDeadDate) {
				firstNonDead = current;
				firstNonDead.position = i - 1;
				firstNonDeadDate = current.currentRun_start;
			}
			i++;
		}
	}

	return firstNonDead;
}

MigrationGraph.prototype.resetViewFirstNonDead = function()
{
	var fnd = undefined;

	fnd = this.findFirstNonDead();
	if (fnd != undefined) {
		this.vPos -= fnd.position*this.processGroupHeight;
	}

	this.updateViewVertically();
}

MigrationGraph.prototype.showVerticalPosition = function()
{
	alert("Currently, this.vPos = " + this.vPos);
}

MigrationGraph.prototype.updateViewVertically = function()
{
	this.svgProcessDrawingArea().setAttribute('transform', 'translate(0, ' + this.vPos + ')');
}

MigrationGraph.prototype.moveViewUp = function()
{
	this.vPos -= this.processGroupHeight;
	this.updateViewVertically();
}

MigrationGraph.prototype.moveViewDown = function()
{
	this.vPos += this.processGroupHeight;
	this.updateViewVertically();
}

MigrationGraph.prototype.resetViewVertically = function()
{
	this.vPos = 0;
	this.updateViewVertically();
}

MigrationGraph.prototype.moveViewHorizontally = function()
{
	this.svgProcessContainer().setAttribute('transform', 'translate(' + -this.hPos + ', 0)');
	this.moveTimelineGroup();
}

MigrationGraph.prototype.moveTimelineGroup = function()
{
  this.svgTimelineGroup().setAttribute('transform', 'translate(' + (-this.width - (this.hPos % 60)) + ', 0)');
}

MigrationGraph.prototype.moveViewLeft = function()
{
	this.hPos += 50;
	this.moveViewHorizontally();
}

MigrationGraph.prototype.moveViewRight = function()
{
	this.hPos -= 50;
	this.moveViewHorizontally();
}

MigrationGraph.prototype.switchDead = function(button)
{
	var nextValue;
	var nextButton;

	if (this.showdead == false) {
		nextValue = true;
		nextButton = "Hide dead processes";
		this.resetViewVertically();
	} else {
		nextValue = false;
		nextButton = "Show dead processes";
		this.resetViewFirstNonDead();
	}

	this.showdead = nextValue;
	button.innerHTML = nextButton;
}

MigrationGraph.prototype.autoMoveView = function()
{
	this.hPos = this.currentDate_rel;
	this.moveViewHorizontally();
}

MigrationGraph.prototype.autoUpdateView = function(toDate_abs)
{
	var currentDate_rel = this.dateAbsToRel(toDate_abs);

	if(this.hPos == this.currentDate_rel)
	{
		this.hPos = currentDate_rel;
	}

	this.currentDate_rel = currentDate_rel;
	this.processList.each(function(pair) { pair.value.updateStillRunning(currentDate_rel); } );
	this.moveViewHorizontally();
}

MigrationGraph.prototype.updateEachProcess = function(toDate)
{
	this.processList.each(function(pair) { pair.value.update(atDate); });
}

MigrationGraph.prototype.addNode = function(nodeId)
{
	var n = new Node(nodeId, this);
	this.nodeList.set(nodeId, n);
	return n;
}

function LoadingDiv(name, divname, target, minvalue)
{
	this.counter = 0;
	this.name = name;
	this.divname = divname;
	this.minvalue = minvalue;
	this.divtarget = target;
	
	this.ajaxCallback = {
		onCreate: function() {
			Ajax.activeRequestCount++;
		},
		onComplete: function() {
			if (Ajax.activeRequestCount >= minvalue) {
				$(divname).hide();
				$(target).show();
			}
		}
	};

	this.init();
}

LoadingDiv.prototype.init = function()
{
	this.waiterShow();
	Ajax.Responders.register(this.ajaxCallback);
}

LoadingDiv.prototype.waiterShow = function()
{
	$(this.divtarget).hide();
	$(this.divname).show();
}

require 'optparse'

options = { :process => 5, :nodes => 3 }

puts ARGV.join(', ')

OptionParser.new do |opts|
	opts.separator ""
	opts.on("-p", "--process=<integer>", Integer,
			"The number of process we want to simulate.",
			"Default: #{options[:process]}") { |n| options[:process] = n }

	opts.on("-n", "--nodes=<integer>", Integer,
			"The number of nodes we want to simulate",
			"Default: #{options[:nodes]}") { |n| puts "Got #{n}"
			options[:nodes] = n }

	# Damned script/runner
	opts.on("-e", "--environment=env", String, "The environment") { }

	opts.on_tail('--script-help', "Show this message") do
    $stderr.puts opts
    exit -1
  end

end.parse!

puts "Going to simulate #{options[:process]} process on #{options[:nodes]} nodes"

Migration.delete_all()

srand Time.now.to_i

currentNode = Hash.new(0)
migrations = Hash.new
nextMig = Hash.new(Time.now)

Signal.trap('INT') do
	puts 'Stopping kerrighed migration simulator'
	(1..options[:process]).each do |p|
		puts "Killing process #{p}"
		m = Migration.new
		m.process_id = p
		m.startDate = Time.now
		m.endDate = Time.now + rand(4)
		m.startNode = currentNode[p]
		m.endNode = 0
		m.save
		puts "... done"
	end
	exit 0
end

while(true) do

	(1..options[:process]).each do |p|
		if(nextMig[p] < Time.now) then
			n = rand(options[:nodes]) + 1

			if(currentNode[p] != n) then
				m = Migration.new
				m.process_id = p
				m.startDate = Time.now
				m.endDate = Time.now + rand(5)
				m.startNode = currentNode[p]
				m.endNode = n

				migrations[p] = m
				nextMig[p] = m.endDate + rand(10)*2 + 5
			end
		end
	end
	
	(1..options[:process]).each do |p|
 		unless(migrations[p].nil?) then
			if(migrations[p].endDate < Time.now) then
				migrations[p].save
				currentNode[p] = migrations[p].endNode
				puts "Process #{p} migrated from node #{migrations[p].startNode} to node #{migrations[p].endNode}"

				migrations[p] = nil
			end
		end
	end

	sleep(1)

end


# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_monitoring-kerrighed_session',
  :secret      => '5e67e4687ac98e5fc6e4542e1b7e446f761b7e1e4e68029b11b23f24a3130b766709652f3c7d22806acd49da5e580aef20dc713ddd6b3bbc8a4a430a40d9e9ae'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
ActionController::Base.session_store = :active_record_store

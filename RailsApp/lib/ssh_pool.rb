require 'net/ssh'

class SSHPool
	@@Connections	= {}
	@@PoolSize	= 4
	@@Timeout	= 30

	def self.computeKey(host, user, key, port)
		return user.to_s + "@" + host.to_s + ":" + port.to_s
	end

	def self.getConnection(host, user, key, port)
		retvalue = nil
		add = false
		h = self.computeKey(host, user, key, port)

		if @@Connections[h].nil?
			retvalue = self.establishConnection(host, user, key, port)
			@@Connections[h] = [ retvalue ]
		else
			@@Connections[h].each do |conn|
				if (conn[:connection].busy?)
					add = true
					retvalue = self.establishConnection(host, user, key, port)
				else
					retvalue = conn
				end
			end
		end

		if (add)
			@@Connections[h] << retvalue
		end

		return retvalue
	end

	def self.execCommand(host, user, key, port, data)
		result = nil
		status = false
		begin
			cnx = self.getConnection(host, user, key, port)
			if not cnx.nil?
				status = true
				result = cnx[:connection].exec!(data)
				cnx[:execd] += 1
				cnx[:date] = Time.now.to_i
			end
		rescue	Exception => e
			result = e.inspect
		end

		return { :status => status, :result => result }
	end
	
	def self.establishConnection(host, user, key, port)
		cnx = Net::SSH.start(host, user, :keys => [ key ], :port => port.to_i)
		return { :connection => cnx, :date => Time.now.to_i, :execd => 0 }
	end

	def self.closeUnused
		i = 0
		now = Time.now.to_i
		@@Connections.each_key do |key|
			@@Connections[key].each_with_index do |conn, idx|
				if (conn[:date] + @@Timeout) < now
					@@Connections[key][idx][:connection].close()
					@@Connections[key][idx] = nil
					i += 1
				end
			end
		end
	end

	def self.usableConnections
		i = 0
		now = Time.now.to_i
		@@Connections.each_key do |key|
			@@Connections[key].each do |conn|
				if (conn[:date] + @@Timeout) >= now
					i += 1
				end
			end
		end

		return i
	end

	def self.availableConnections
		return @@Connections.length
	end
end

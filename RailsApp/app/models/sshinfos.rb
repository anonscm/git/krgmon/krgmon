require 'ssh_pool'

class SSHInfos

	def exec_cmd(command)
		host = AppConfig.ssh_host
		user = AppConfig.ssh_user
		key  = AppConfig.ssh_key
		port = AppConfig.ssh_port
		command = "sudo " + command

		return SSHPool.execCommand(host, user, key, port, command)
	end

	def get_memory_stats
		meminfo = self.exec_cmd("free -b");

		m = meminfo.split("\n")
		(title, memtotal, memused, memfree) = m[1].split(" ")
		(plusorminus, buffer, bufused, buffree) = m[2].split(" ")
		(swap, swaptotal, swapused, swapfree) = m[3].split(" ")
		
		self.mem_total	= memtotal
		self.mem_used	= memused
		self.mem_free	= memfree
		self.buf_used	= bufused
		self.buf_free	= buffree
		self.swap_total	= swaptotal
		self.swap_used	= swapused
		self.swap_free	= swapfree
	end

	def get_raw_cpu_stats
		hash = {} # Hash needed for multi-cpu systems

		lines = self.exec_cmd("cat /proc/stat")
		lines.each_with_index{ |line, i|
			array = line.split
			break unless array[0] =~ /cpu/   # 'cpu' entries always on top

			# Some machines list a 'cpu' and a 'cpu0'.  In this case, only
			# return values for the numbered cpu entry.
			if lines[i].to_s.split[0] == "cpu" && lines[i+1].to_s.split[0] =~ /cpu\d+/
				next
			end 

			vals = array[1..-1].map{ |e| e = e.to_f }
			hash[array[0]] = vals
		}

		return hash
	end

	def get_cpu_stats(intv)
		stats0 = self.get_raw_cpu_stats()
		sleep(intv)
		stats1 = self.get_raw_cpu_stats()

		stats = []
		if stats0.keys.sort == stats1.keys.sort
			interval = stats1["cpu"].values_at(0, 1, 2).sum - stats0["cpu"].values_at(0, 1, 2).sum
			stats0.delete("cpu")
			stats1.delete("cpu")

			stats0.each_key do |key|
				s0 = stats0[key].values_at(0, 1, 2).sum
				s1 = stats1[key].values_at(0, 1, 2).sum
				
				stats << { :cpuid => key, :prcent => (s1.to_f - s0.to_f) }
			end
		end

		self.cpu_stats = stats.sort_by { |x| x[:cpuid].delete("cpu").to_i }
	end

	def get_top_stats
		self.top = self.exec_cmd("top -b -n1")
	end

	def get_proc_infos(pid)
		raise ArgumentError, 'PID is not Integer' unless pid.is_a? Integer

		res = [ ]
		root = "/proc/" + pid.to_s + "/"

		path = root + "cmdline"
		value = self.exec_cmd("cat " + path)
		if not value.include?(path)
			cmdline = value.split("\0")
			infos = { :name => "Command line", :value => cmdline.join(" ") }
			res << infos
		end

		self.proc_infos = res
	end

	def get_proc_stats(pid)
		raise ArgumentError, 'PID is not Integer' unless pid.is_a? Integer

		res = [ ]
		self.get_top_stats()
		topval = self.top
		topval = topval.split("\n")
		
		topval.each do |line|
			if line =~ /^#{pid.to_i}/
				l = line.split(" ")
				info = { :name => "CPU Usage", :value => l[8] + "%" }
				res << info
			end
		end

		self.top = nil
		self.proc_infos = res
	end

	def top
		@top_output
	end

	def top=(value)
		@top_output = value
	end

	def mem_total
		@memtotal
	end

	def mem_total=(value)
		@memtotal = value
	end

	def mem_used
		@memused
	end

	def mem_used=(value)
		@memused = value
	end

	def mem_free
		@memfree
	end

	def mem_free=(value)
		@memfree = value
	end

	def buf_used
		@bufused
	end

	def buf_used=(value)
		@bufused = value
	end

	def buf_free
		@buffree
	end

	def buf_free=(value)
		@buffree = value
	end

	def swap_total
		@swaptotal
	end

	def swap_total=(value)
		@swaptotal = value
	end

	def swap_used
		@swapused
	end

	def swap_used=(value)
		@swapused = value
	end

	def swap_free
		@swapfree
	end

	def swap_free=(value)
		@swapfree = value
	end

	def cpu_stats
		@cpustats
	end

	def cpu_stats=(value)
		@cpustats = value
	end

	def proc_infos
		@procinfos
	end

	def proc_infos=(value)
		@procinfos = value
	end
end

class TrackerController < ApplicationController
  layout 'base_layout'

	after_filter :set_content_type, :only => [:index, :gantt, :graphe]
	
	def index
	end

	def gantt
		@date = Time.now
		m = Migration.find(:first, :conditions => ["created_at < ?", @date], :order => "id asc")
		@last_mig_id = (m.nil? ? -1 : m.id)
	end

	def ajax_update_migrationGraph
		render :update do |page|
			if request.get?
				lastMigId = params[:lastMigId]
				graph = params[:graph]

				migrations = Migration.find(:all, :conditions => ["id > ?", lastMigId], :order => 'id asc')

				migrations.each do |m|
					page << "#{graph}.migrate(#{m.id}, #{m.process_id}, #{m.startNode}, #{m.endNode}, #{m.startDate.to_i}, #{m.endDate.to_i});" 
				end
				page << "#{graph}.autoUpdateView(#{Time.now.to_i});"
			end
		end
	end

	def ajax_getProcInfos
		render :update do |page|
			if request.get?
				pid = params[:pid].to_i
				infos = SSHInfos.new()
				infos.get_proc_infos(pid)
				page << infos.to_json
			end
		end
	end
	
	def ajax_getProcLoad
		render :update do |page|
			if request.get?
				pid = params[:pid].to_i
				infos = SSHInfos.new()
				infos.get_proc_stats(pid)
				page << infos.to_json
			end
		end
	end

	def graphe
		migs = Migration.find(:all, :limit => 5, :order => 'id desc')
		@migrations = migs.sort_by { |t| t.id }
	end

	def ajax_update_forceDirectedMigrationGraph
		render :update do |page|
			if request.get?
				lastMigId = params[:lastMigId]
				graph = params[:graph]

				if (lastMigId == 0)
					migrations = Migration.find(:all, :limit => 5, :order => 'id desc')
				else
					migrations = Migration.find(:all, :conditions => ["id > ?", lastMigId], :order => 'id asc')
				end

				migrations.each do |m|
					page << "#{graph}.createOrUpdateMigration(#{m.id}, #{m.startNode}, #{m.endNode});" 
				end
			end
		end
	end

	def molecular
		@debug = params[:debug] ? true : false
	end

	def ajax_update_molecularGraph
		render :update do |page|
			if request.get?
				lastMigId = params[:lastMigId]
				
				migration = Migration.last(:conditions => ["id > ?", lastMigId], :order => 'id desc')
					
				unless migration.nil?
					page << "ClusterManager.getSingleton().migrate(#{migration.id}, #{migration.process_id}, #{migration.startNode}, #{migration.endNode});" 
				end
			end
		end
	end
end

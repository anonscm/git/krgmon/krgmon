class StatusController < ApplicationController
  layout 'base_layout'

	after_filter :set_content_type, :only => [:index, :mem, :cpu, :exec]
	
	def index
	end

	def exec
		@host = AppConfig.ssh_host
		@user = AppConfig.ssh_user
		if request.post?
			cmd = params[:sshinfos]
			infos = SSHInfos.new()
			r = infos.exec_cmd(cmd[:command])
			if r[:status]
				@result_status = "Command successfully executed!"
				@result_class = "notice"
				@result_command = r[:result]
			else
				@result_status = "Error while executing command."
				@result_class = "error"
				@result_command = r[:result]
			end
		end
	end

	def ajax_update_cpuGraph
		render :update do |page|
			if request.get?
				infos = SSHInfos.new()
				infos.get_cpu_stats(0.5)
				
				graph = params[:graph]

				page << "#{graph}.push_new_value(" << infos.to_json << ");"
			end
		end
	end

	def ajax_update_memoryGraph
		render :update do |page|
			if request.get?
				infos = SSHInfos.new()
				infos.get_memory_stats
				
				graph = params[:graph]

				page << "#{graph}.push_new_value(" << infos.to_json << ");"
			end
		end
	end

	def ajax_update_topStatus
		render :update do |page|
			if request.get?
				infos = SSHInfos.new()
				infos.get_top_stats
				
				area = params[:area]

				page << "#{area}.set_value(" << infos.to_json << ");"
			end
		end
	end

end

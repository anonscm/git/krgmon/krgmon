class IndexController < ApplicationController
  layout 'base_layout'

	after_filter :set_content_type, :only => [:index]
	
  def index
  end

end
